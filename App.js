import React, { Component } from 'react';
import { StatusBar, Platform } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';


//SCREENS
import LandingPage from './src/screens/landingPage'
import { HomePage, HomeItem } from './src/screens/homePage'
import LoginPage from './src/screens/loginPage'
import RentSignupPage from './src/screens/rentSignupPage'
import PropertiesPage from './src/screens/propertiesPage'
import RoomDetailsPage from './src/screens/roomDetailsPage'
import OTPPage from './src/screens/otpPage'

//HOME SCREENS
import RentsPage from './src/screens/home/rentsPage'
import PaymentsPage from './src/screens/home/paymentsPage'
import PayRentPage from './src/screens/home/payRentPage'
import CreateReportPage from './src/screens/home/createReportPage'
import ViewReportsPage from './src/screens/home/viewReportsPage'
import AddRoomPage from './src/screens/home/addRoomPage'
import ApproveRentsPage from './src/screens/home/approveRentsPage'
import TenantsPage from './src/screens/home/tenantsPage'
import RentDetails from './src/screens/home/detailPages/rentDetails'
import PaymentDetails from './src/screens/home/detailPages/paymentDetails'
import TenantDetails from './src/screens/home/detailPages/tenantDetails'
import ApprovalDetails from './src/screens/home/detailPages/approvalDetails'
import RevokePage from './src/screens/home/revokePage'
import RentProperty from './src/screens/rentProperty'
import PaymentPage from './src/screens/paymentPage'
import InspectPropertyPage from './src/screens/inspectPropertyPage';
import UpdatesPage from "./src/screens/updates.page";
import ResetPassword from "./src/screens/resetpassword";


//STATE
import AuthState, { AuthConsumer } from "./src/store/auth.state";


import colors from './src/constants/colors';
import uploadImage from './src/screens/uploadImage';
import chooseduration from './src/screens/home/detailPages/chooseduration';
import RevokeRentPage from './src/screens/home/revokrent.page';
import InspectionsPage from './src/screens/inspectionspage';
import Rentlaws from './src/screens/rentlaws.page';
import AgreementPage from './src/screens/agreement.page';
import Terms from './src/screens/nb';
import loginLandlord from './src/screens/landlordLogin.page';
import AdminNewRent from './src/screens/adminNewRent';
import RenewChooseRent from './src/screens/renewChooseRent';
import RenewalDetailsPage from './src/screens/RenewalDetailsPage';
import payChooseRent from './src/screens/payChooseRent';


StatusBar.setBarStyle("dark-content",true);


const Stack = createStackNavigator();
class App extends Component {

  render() {
    return (
      <AuthState>
        <AuthConsumer>
          {
            ({state})=>{
              if(state.user && state.user._id){
                return(
                  <NavigationContainer>
                <Stack.Navigator>
                  <Stack.Screen
                    name="homePage"
                    component={HomePage}
                    options={{ headerShown: false }}
                  />
                  <Stack.Screen
                    name="propertiesPage"
                    component={PropertiesPage}
                    options={{ headerShown: false }}
                  />
                  <Stack.Screen
                    name="roomDetailsPage"
                    component={RoomDetailsPage}
                    options={{ headerShown: false }}
                  />
                  <Stack.Screen
                    name="Agreement"
                    component={AgreementPage}
                    options={{ headerShown: false }}
                  />
                  <Stack.Screen
                    name="Terms"
                    component={Terms}
                    options={{ headerShown: false }}
                  />
                  <Stack.Screen
                    name="rentlaws"
                    component={Rentlaws}
                    options={{ headerShown: false }}
                  />
                  <Stack.Screen
                    name="UpdatesPage"
                    component={UpdatesPage}
                    options={{ headerShown: false }}
                  />
                  <Stack.Screen
                    name="OTP"
                    component={OTPPage}
                    options={{ headerShown: false }}
                  />
                  <Stack.Screen
                    name="paymentPage"
                    component={PaymentPage}
                    options={{ headerShown: false }}
                  />
                  <Stack.Screen
                    name="rentsPage"
                    component={RentsPage}
                    options={{ headerShown: false }}
                  />
                  <Stack.Screen
                    name="paymentsPage"
                    component={PaymentsPage}
                    options={{ headerShown: false }}
                  />
                  <Stack.Screen
                    name="payChooseRent"
                    component={payChooseRent}
                    options={{ headerShown: false }}
                  />
                  <Stack.Screen
                    name="payRentPage"
                    component={PayRentPage}
                    options={{ headerShown: false }}
                  />
                  <Stack.Screen
                    name="RenewRent"
                    component={RenewChooseRent}
                    options={{ headerShown: false }}
                  />
                  <Stack.Screen
                    name="RenewalDetails"
                    component={RenewalDetailsPage}
                    options={{ headerShown: false }}
                  />
                  <Stack.Screen
                    name="InspectionsPage"
                    component={InspectionsPage}
                    options={{ headerShown:false }}
                  />
                  <Stack.Screen
                    name="rentPropertyPage"
                    component={RentProperty}
                    options={{ headerShown:false }}
                  />
                  <Stack.Screen
                    name="adminNewRent"
                    component={AdminNewRent}
                    options={{ headerShown:false }}
                  />
                  <Stack.Screen
                    name="inspectPropertyPage"
                    component={InspectPropertyPage}
                    options={{ headerShown:false }}
                  />
                  <Stack.Screen
                    name="createReportPage"
                    component={CreateReportPage}
                    options={{ headerShown:false }}
                  />
                  <Stack.Screen
                    name="viewReportsPage"
                    component={ViewReportsPage}
                    options={{ headerShown:false }}
                  />
                  <Stack.Screen
                    name="addRoomPage"
                    component={AddRoomPage}
                    options={{ headerShown:false }}
                  />
                  <Stack.Screen
                    name="UploadImages"
                    component={uploadImage}
                    options={{ headerShown:false }}
                  />
                  <Stack.Screen
                    name="ChooseDuration"
                    component={chooseduration}
                    options={{ headerShown:false }}
                  />
                  <Stack.Screen
                    name="approveRentsPage"
                    component={ApproveRentsPage}
                    options={{ headerShown:false }}
                  />
                  <Stack.Screen
                    name="tenantsPage"
                    component={TenantsPage}
                    options={{ headerShown:false }}
                  />
                  <Stack.Screen
                    name="RemoveTenant"
                    component={RevokeRentPage}
                    options={{ headerShown:false }}
                  />
                  <Stack.Screen
                    name="rentDetails"
                    component={RentDetails}
                    options={{ headerShown:false }}
                  />
                  <Stack.Screen
                    name="paymentDetails"
                    component={PaymentDetails}
                    options={{ headerShown:false }}
                  />
                  <Stack.Screen
                    name="tenantDetails"
                    component={TenantDetails}
                    options={{ headerShown:false }}
                  />
                  <Stack.Screen
                    name="approvalDetails"
                    component={ApprovalDetails}
                    options={{ headerShown:false }}
                  />
                  <Stack.Screen
                    name="revokePage"
                    component={RevokePage}
                    options={{ headerShown:false }}
                  />
                </Stack.Navigator>
              </NavigationContainer>
                )
              }else{
                return(
                  <NavigationContainer>
                <Stack.Navigator>
                  <Stack.Screen
                      name="landingPage"
                      component={LandingPage}
                      options={{ headerShown: false }}
                    />
                  <Stack.Screen
                    name="loginPage"
                    component={LoginPage}
                    options={{ headerShown: false }}
                  />
                  <Stack.Screen
                    name="ResetPassword"
                    component={ResetPassword}
                    options={{ headerShown: false }}
                  />
                  <Stack.Screen
                    name="loginLandlord"
                    component={loginLandlord}
                    options={{ headerShown: false }}
                  />
                  <Stack.Screen
                    name="signupPage"
                    component={RentSignupPage}
                    options={{ headerShown: false }}
                  />
                  <Stack.Screen
                    name="OTP"
                    component={OTPPage}
                    options={{ headerShown: false }}
                  />
                  <Stack.Screen
                    name="propertiesPage"
                    component={PropertiesPage}
                    options={{ headerShown: false }}
                  />
                  <Stack.Screen
                    name="roomDetailsPage"
                    component={RoomDetailsPage}
                    options={{ headerShown: false }}
                  />
                  <Stack.Screen
                    name="inspectPropertyPage"
                    component={InspectPropertyPage}
                    options={{ headerShown:false }}
                  />
                  <Stack.Screen
                    name="paymentPage"
                    component={PaymentPage}
                    options={{ headerShown: false }}
                  />
                </Stack.Navigator>
              </NavigationContainer>
                )
              }
            }
          }
        </AuthConsumer>
      </AuthState>
    )
  }
};
export default App;
