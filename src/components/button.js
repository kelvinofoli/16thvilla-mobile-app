import React from 'react'
import {TouchableOpacity, StyleSheet, ActivityIndicator,Text } from 'react-native'
// import NText from '../text'

const NButton=(props)=>{
    return (
        <TouchableOpacity 
            disabled={props.loading}
            onPress={props.onPress}
            style={[styles.container,{
                width:props.width
            },props.style]}
        >
            {
                props.loading ? (
                    <ActivityIndicator 
                        size={20}
                        color="#fff"
                    />
            ): <Text
                style={{
                    fontSize: 18,
                    color:'white',
                    fontFamily: 'Gotham-Medium'
                }}>{props.title}
            </Text>
            }
        </TouchableOpacity>
    )
}


const styles = StyleSheet.create({
    container: {
        backgroundColor: '#93478f',
        borderRadius: 20,
        alignItems:'center',
        justifyContent:'center',
        padding:8,
        margin:15,
        height:60,
        width:'60%'
    }
})
export default NButton
