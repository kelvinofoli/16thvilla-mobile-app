import React from 'react';
import DatePicker from 'react-native-date-picker';
import { View,TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';


const DatePickerComponent=(props)=>(
    <View style={{
        marginVertical:15,
        borderColor:'#6d6d6d',
        borderWidth:1,
        borderRadius:5
    }}>
        <TouchableOpacity 
            hitSlop={{top:10,bottom:10,left:10,right:10}}
            onPress={props.close}
        style={{
            margin:10,
            width:50
        }}>
            <Icon name="close" size={20}/>
        </TouchableOpacity>
        <DatePicker
            mode={props.type ?? 'date'}
            date={ props.value || new Date()}
            onDateChange={props.onDateChange}
        />
    </View>
);

export default DatePickerComponent;
