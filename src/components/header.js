import React from 'react'
import { SafeAreaView } from 'react-native';
import { View, TouchableOpacity, StyleSheet,Dimensions } from 'react-native'
import { Text } from 'react-native-elements';
import IconIo from 'react-native-vector-icons/Ionicons';
import colors from '../constants/colors';
const { height, width } = Dimensions.get('window');

const Header=(props)=>{
    return (
        <SafeAreaView>
            <View style={[styles.horizontal, { paddingVertical: 10, paddingLeft: 10, width: width},props.styles]} {...props}>
                {props.back && <TouchableOpacity
                    onPress={()=>props.back()}
                >
                    <IconIo 
                        name="arrow-back-circle" 
                        size={50} 
                        color={colors.black}
                        style={{marginHorizontal:10}}
                    />
                </TouchableOpacity>}
                <View style={{ marginLeft: 10,marginVertical:15 }}>
                    <Text h3 style={{ color:colors.black,fontWeight:'bold'}}>16th August 85, Villa</Text>
                    <View style={styles.horizontal}>
                        <Text style={{ fontSize: 20, color:colors.black}}>{props.name}</Text>
                    </View>
                </View>
            </View>
        </SafeAreaView>
    )
}
const styles = StyleSheet.create({
    horizontal: {
        flexDirection: 'row',
        alignItems: 'center',
        width:'100%'
    }
})
export default Header
