import React from 'react';
import { TouchableOpacity} from 'react-native';
import IconFA from 'react-native-vector-icons/FontAwesome';
import colors from '../constants/colors';


const IconButton=(props)=>(
    <TouchableOpacity
        onPress={props.onPress}
        style={{
            width:50,
            height:50,
            marginRight:20
        }}
    >
        <IconFA 
            name={props.icon || 'pencil'}
            size={30}
            color={colors.theme}
        />
    </TouchableOpacity>
);


export default IconButton;
