import React from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
import colors from '../constants/colors';
import { Text } from "react-native-elements";


const RentListItem=(props)=>{
    const {el}=props;
    return(
        <TouchableOpacity 
    style={styles.rentItem}
    onPress={props.onPress}
>
    <View style={{
        flexDirection:'row',
        alignItems:'center'                               
    }}>
        <Icon 
            name="user" 
            color={colors.black} 
            size={20}
            style={{
                marginRight:15
            }}
        />
        <Text h4 style={{color:'#6d6d6d'}}>{`${el.tenant?.firstname} ${el.tenant?.lastname}`}</Text>
    </View>
    <View style={[styles.horizontalLabel,{ marginTop:10 }]}>
        <View style={styles.horizontalLabel}>
            <Icon 
                name="home" 
                color={colors.black} 
                style={{
                    marginRight:10,
                }}
            />
            <Text h5>{el.room?.type}</Text>
        </View>
        <View style={[styles.horizontalLabel,{marginLeft:10}]}>
            <Icon 
                name="calendar" 
                color={colors.black} 
                style={{
                    marginRight:10
                }}
            />
            <Text h5>{el.duration} MONTHS</Text>
        </View>
    </View>
    <View style={{
        flexDirection:'row',
        alignItems:'center'
    }}>
        <Icon
            name="home" 
            color={colors.black} 
            style={{
                marginRight:5,
            }}
        />
        <Text style={{
            marginVertical:5,
            borderRadius:5,
            padding:5
        }}>accomodation type: <Text style={{
            marginLeft:10,
            fontWeight:'500'
        }}>{el.accomodation}</Text></Text>
    </View>
    <View style={{ 
        paddingHorizontal: 6,
        paddingVertical:10,
        marginTop:5,
        flexDirection:'row',
        justifyContent:'space-between',
        width:'100%',
        alignSelf: 'flex-start', 
        backgroundColor: 'gold', 
        borderRadius:5
    }}
    >
        <Text style={{ color: '#000' }}>STATUS: {el.status}</Text>
        <Text style={{ color: '#000' }}>RENT: {el.rentType}</Text>
    </View>
</TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    horizontalLabel:{
        justifyContent:'flex-start',
        flexDirection:'row',
        alignItems:'center',
    },
    horizontal: {
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft:25,
        marginBottom: 23
    },
    rentItem: {
        width: '100%',
        borderBottomWidth: 2,
        borderColor: '#e5e5e5',
        paddingHorizontal:15,
        paddingVertical:10
    }
});

export default RentListItem;
