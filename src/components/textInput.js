import React from 'react'
import {TextInput,Platform } from 'react-native'
import { DROP_HEIGHT, DROP_WIDTH } from '../constants/styles'

const NTextInput=(props)=>{
    return (
        <TextInput 
        style={[{
            borderRadius: 5,
            borderWidth: 2,
            borderColor: '#959595',
            paddingLeft:10,
            width:DROP_WIDTH,
            height:DROP_HEIGHT/1.15,
            ...Platform.select(
                {
                    'android':{
                        fontFamily: 'Gotham-Medium'
                    }
                }
            ),
            marginVertical:5,
            backgroundColor:'#fff',
            fontSize:18
        },{...props._style}]} 
        enablesReturnKeyAutomatically={true} 
        placeholderTextColor="#bbb" 
        placeholder={props.placeholder}
        defaultValue={props.defaultValue}
        {...props} 
        />
    )
}

export default NTextInput
