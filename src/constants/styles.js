import {StyleSheet} from 'react-native'
import colors from './colors';
import { Dimensions } from "react-native";

const width=Dimensions.get('screen').width;
export const DROP_WIDTH=width * 0.94;
export const DROP_HEIGHT=70;


export  default StyleSheet.create({
    button: {
        backgroundColor:colors.black,
        height:DROP_HEIGHT/1.1,
        width:DROP_WIDTH,
        marginTop:15,
        marginRight:'auto',
        marginLeft:'auto',
        marginBottom:20,
        borderRadius:5,
        zIndex:1
    },
    dateInput: {
        borderRadius: 5,
        borderWidth: 2,
        borderColor: '#959595',
        padding: 10,
        width:DROP_WIDTH,
        height:DROP_HEIGHT/1.1,
        justifyContent: 'center',
        marginVertical: 5,
        backgroundColor:'#fff'
    }
})