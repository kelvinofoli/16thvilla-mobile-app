import { Countries } from "./countries";

const values={
    payments:[
        {
            label:'Purpose',
            value:'purpose',
            type:'PICKER',
            list:[
                'RENT',
                'INSPECTION',
                'UTILITY',
                'SD'
            ]
        },
        {
            label:'Description',
            value:'description',
            type:'INPUT-TEXT'
        },
        {
            label:'Account Number',
            value:'accountNumber',
            type:'INPUT-TEXT'
        },
        {
            label:'Account Issuer',
            value:'accountIssuer',
            type:'PICKER',
            list:[
                'vodafone',
                'mtn',
                'airteltigo'
            ]
        },
        {
            label:'Amount',
            value:'amount',
            type:'INPUT-TEXT'
        },
        {
            label:'Status',
            value:'status',
            type:'PICKER',
            list:[
                'PENDING',
                'FAILED',
                'PAID',
                'REVERSED'
            ]
        },
        {
            label:'Payment Type',
            value:'paymentType',
            type:'PICKER',
            list:[
                'CASH',
                'MOMO',
                'BANK DEPOSIT'
            ]
        }
    ],
    rooms:[
        {
            label:'Property Type',
            value:'type',
            type:'PICKER',
            list:[
                'CHAMBER AND HALL',
                '2 BEDROOMS',
                '1 BEDROOMS'
            ]
        },
        {
            label:'Location',
            value:'location',
            type:'INPUT-TEXT'
        },
        {
            label:'Address',
            value:'address',
            type:'INPUT-TEXT'
        },
        {
            label:'Size',
            value:'size',
            type:'INPUT-TEXT'
        },
        {
            label:'Condition',
            value:'condition',
            type:'PICKER',
            list:[
                'NEW',
                'PRE-OCCUPIED'
            ]
        },
        {
            label:'Furnishing',
            value:'furnishing',
            type:'PICKER',
            list:[
                'SEMI FURNISHED',
                'FULLY FURNISHED'
            ]
        },
        {
            label:'Room Number',
            value:'roomNumber',
            type:'INPUT-TEXT'
        },
        {
            label:'Price',
            value:'price',
            type:'INPUT-TEXT'
        },
        {
            label:'Details',
            value:'details',
            type:'INPUT-TEXT'
        },
        {
            label:'Accomodation',
            value:'accomodation',
            type:'PICKER',
            list:[
                'SHORT STAY',
                'LONG STAY'
            ]
        },
        {
            label:'Inspection Amount',
            value:'inspectionAmount',
            type:'INPUT-TEXT'
        },
        {
            label:'Currency',
            value:'currency',
            type:'PICKER',
            list:[
                'GHS',
                'USD'
            ]
        }
    ],
    tenants:[
        {
            label:'Title',
            value:'title',
            type:'INPUT-TEXT'
        },
        {
            label:'Date Of Birth',
            value:'dateOfBirth',
            type:'INPUT-DATE',
            show:'showdateOfBirth'
        },
        {
            label:'Nationality',
            value:'nationality',
            type:'PICKER',
            list:Countries.map(c=>{ return c['en_short_name'] })
        },
        {
            label:'Number Of Children',
            value:'numberOfChildren',
            type:'INPUT-TEXT'
        },
        {
            label:'Office Number',
            value:'officeNumber',
            type:'INPUT-TEXT'
        },
        {
            label:'Current Address',
            value:'currentAddress',
            type:'INPUT-TEXT'
        },
        {
            label:'postalAddress',
            value:'postalAddress',
            type:'INPUT-TEXT'
        },
        {
            label:'postalAddress',
            value:'postalAddress',
            type:'INPUT-TEXT'
        },
        {
            label:'Do you smoke',
            value:'smoke',
            type:'PICKER',
            list:[
                'YES',
                'NO'
            ]
        },
        {
            label:'Do you have pets',
            value:'havePets',
            type:'PICKER',
            list:[
                'YES',
                'NO'
            ]
        },
        {
            label:'Email',
            value:'email',
            type:'INPUT-TEXT'
        },
        {
            label:'Business ContactName',
            value:'businessContactName',
            type:'INPUT-TEXT'
        },
        {
            label:'Business Location',
            value:'businessLocation',
            type:'INPUT-TEXT'
        },
        {
            label:'Business ContactNumber',
            value:'businessContactNumber',
            type:'INPUT-TEXT'
        },
        {
            label:'Business Email',
            value:'businessEmail',
            type:'INPUT-TEXT'
        },
        {
            label:'Years Employed',
            value:'yearsEmployed',
            type:'INPUT-TEXT'
        },
        {
            label:'Firstname',
            value:'firstname',
            type:'INPUT-TEXT'
        },
        {
            label:'Lastname',
            value:'lastname',
            type:'INPUT-TEXT'
        },
        {
            label:'Contact PersonName',
            value:'contactPersonName',
            type:'INPUT-TEXT'
        },
        {
            label:'Contact PersonEmail',
            value:'contactPersonEmail',
            type:'INPUT-TEXT'
        },
        {
            label:'Contact Person Address',
            value:'contactPersonAddress',
            type:'INPUT-TEXT'
        },
        {
            label:'Contact Person Phone',
            value:'contactPersonPhone',
            type:'INPUT-TEXT'
        },
        {
            label:'contact Person Relationship',
            value:'contactPersonRelationship',
            type:'INPUT-TEXT'
        },
        {
            label:'Phone',
            value:'phone',
            type:'INPUT-TEXT'
        },
        {
            label:'Gender',
            value:'gender',
            type:'PICKER',
            list:[
                'Male',
                'Female'
            ]
        },
        {
            label:'Age',
            value:'age',
            type:'INPUT-TEXT'
        },
        {
            label:'Married',
            value:'married',
            type:'PICKER',
            list:[
                'YES',
                'NO'
            ]
        },
        {
            label:'Occupation',
            value:'occupation',
            type:'INPUT-TEXT'
        },
        {
            label:'Id Number',
            value:'idNumber',
            type:'INPUT-TEXT'
        },
        {
            label:'Id Type',
            value:'idType',
            type:'PICKER',
            list:[
                'voter',
                'passport',
                'driver'
            ]
        }
    ],
    rents:[
        {
            label:'Duration',
            value:'duration',
            type:'INPUT-TEXT'
        },
        {
            label:'Amount',
            value:'amount',
            type:'INPUT-TEXT'
        },
        {
            label:'Payment Type',
            value:'paymentType',
            type:'PICKER',
            list:[
                'CASH',
                'MOMO',
                'BANK DEPOSIT'
            ]
        },
        {
            label:'Cost PerMonth',
            value:'costPerMonth',
            type:'INPUT-TEXT'
        },
        {
            label:'Accomodation Type',
            value:'accomodation',
            type:'PICKER',
            list:[
                'LONG STAY',
                'SHORT STAY'
            ]
        },
        // {
        //     label:'Room Occupied',
        //     value:'room',
        //     type:'PICKER-DYNAMIC',
        //     url:'rooms/get',
        //     listName:'rooms',
        //     id:'room'
        // },
        {
            label:'Status',
            value:'status',
            type:'PICKER',
            list:[
                'DUE',
                'PENDING APPROVAL',
                'EXPIRED',
                'ACTIVE',
                'REVOKED',
                'PENDING PAYMENT'
            ]
        },
        {
            label:'Start Date',
            value:'startDate',
            type:'INPUT-DATE',
            show:'showStartDate'
        },
        {
            label:'End Date',
            value:'endDate',
            type:'INPUT-DATE',
            show:'showEndDate'
        },
        {
            label:'Choose Rent Type',
            value:'rentType',
            type:'PICKER',
            list:[
                'NEW',
                'RENEWAL'
            ]
        }
    ]
}

export default values;