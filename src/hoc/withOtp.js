import React, {Component } from 'react';
import apiService from "../services/api.service";


const WithOtp=(WrappedComponent)=>class extends Component {

    viewLoaded=false;

    state={
        loading:false
    }

    componentDidMount(){
        this.viewLoaded=true;
    }

    componentWillUnmount(){
        this.viewLoaded=false;
    }

    async sendOtp(number,resend=false) {
        try {
            if (resend && this.viewLoaded) {
                this.setState(
                    {
                        loading:true
                    }
                )
            }
            const response=await apiService.sendOtp(
                { 
                    phoneNumber:number 
                }
            );
            if (!response.success) {
                throw Error(
                    response.message
                );
            }
            if (resend && this.viewLoaded) {
                Alert.alert(
                    'OTP',
                    'Verification code has been sent to your phone'
                )
                this.setState(
                    {
                        loading:false
                    }
                )
            }
        } catch (err) {
            if (resend && this.viewLoaded) {
                Alert.alert('Oops!',err.message);
                this.setState(
                    {
                        loading:false
                    }
                )
            }
        }
    }
    

    render(){
        return (
            <WrappedComponent loading={this.state.loading} sendOtp={this.sendOtp}>
                {this.props.children}
            </WrappedComponent>
        )
    }

}


export default WithOtp;