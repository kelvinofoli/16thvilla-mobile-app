import React, { Component } from 'react';
import apiService from '../services/api.service';
import { AuthConsumer } from '../store/auth.state';


const WithRentsComponent=(WrappedComponent)=>class extends Component {

    state={
        rents:[],
        loading:false
    }

    componentDidMount(){
        this.getRents();
    }
    
    getRents=async()=>{
        try {
            let service=null;
            switch (this.role) {
                case 'tenant':
                    service=apiService.getTenantRents;
                    break;
                default:
                    service=apiService.getAllRents;
                    break;
            }
            this.setState({loading:true});
            const response=await service(this.user._id);
            if(!response.success){
                throw Error(
                    response.message
                )
            }
            this.setState(
                {
                    loading:false,
                    rents:response.data
                }
            )
        } catch (err) {
            this.getRents();
            this.setState(
                {
                    loading:false
                }
            )
        }
    }

    render(){
        return(
            <AuthConsumer>
                {
                    ({state})=>{
                        const{user,role}=state;
                        this.user=user;
                        this.role=role;
                        return <WrappedComponent loading={this.state.loading} rents={this.state.rents} {...this.props}/>
                    }
                }
            </AuthConsumer>
        )
    }

}


export default WithRentsComponent;
