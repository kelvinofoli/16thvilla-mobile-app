import React, { Component } from 'react';
import { Text, View,TouchableOpacity,StyleSheet,Dimensions, Alert,ScrollView } from 'react-native';
import Header from '../components/header';
import DropDownPicker from 'react-native-dropdown-picker';
import generateMonths from "../utils/generateMonths";
import globalStyles, { DROP_HEIGHT, DROP_WIDTH } from '../constants/styles';
const {width}=Dimensions.get('screen');
import service from "../services/api.service";
import { Button } from 'react-native-elements'





export default class RenewalDetailsPage extends Component {

    state={
        startDate:new Date,
        loading:false
    };

    componentDidMount(){
        const d=this.props.route.params.data;
        this.setState(
            {
                ...d,amount:0,duration:0
            }
        )
    }

    renew=async(data)=>{
        try {
            this.setState(
                {
                    loading:true
                }
            )
            const response=await service.renewRent(data);
            if(!response.success){
                throw Error(
                    response.message
                )
            };
            this.setState(
                {
                    loading:false
                },()=>{
                    this.props.navigation.navigate('homePage');
                    Alert.alert(
                        'Congratulations!',
                        'Your request is received and processed your landlord will approve your request.Thank you for using our App.'
                    )
                }
            ) 
        } catch (err) {
            this.setState(
                {
                    loading:false
                },()=>Alert.alert(
                    'Oops,something went wrong',
                    err.message
                )
            ) 
        }
    }

    sendRequest=async()=>{
        try {
            const {
                _id,
                duration,
                paymentType,
                endDate,
                amount,
                tenant,
                room
            }=this.state;
            if(amount<=0 || duration===0){
                throw Error(
                    'Please choose duration for renewal'
                )
            }
            const data={
                rent:_id,
                duration:duration,
                paymentType:paymentType,
                startDate:endDate,
                amount:amount
            };
            Object.values(data).forEach(v=>{
                if(!v || v===''){
                    throw Error(
                        'Please enter all required fields'
                    )
                }
            });
            switch (paymentType) {
                case 'MOMO':
                    return this.props.navigation.navigate(
                        'paymentPage',
                        {
                            data:data,
                            page:'RENEW',
                            currency:room.currency
                        }
                    )
                default:
                    return Alert.alert(
                        'Confirm Transaction',
                        `Hello ${tenant?.firstname},you have requested to pay an amount of ${room?.currency}${amount} to renew your rent for ${duration} months.Do you want proceed?`,
                        [
                            {
                                text:'Yes,proceed',
                                onPress:()=>this.renew(data)
                            },
                            {
                                text:'No,cancel',
                                onPress:null
                            }
                        ]
                    )
            }
        } catch (err) {
            Alert.alert(
                'Renewal Error',
                err.message
            )
        }
    }


    render() {
        return (
            <>
                <Header 
                    name="Confirm Renewal" 
                    back={this.props.navigation.goBack} 
                />
                <ScrollView contentContainerStyle={styles.container} keyboardDismissMode="on-drag" keyboardShouldPersistTaps="handled">
                    <Text style={styles.label}
                            >Choose number of months you want to renew and select your payment method to proceed.</Text>

                    <Text style={{ 
                        alignSelf:'flex-start',
                        marginVertical:10,
                        marginHorizontal:'5%' 
                        }}
                    >Renewal Start Date</Text>
                    <TouchableOpacity
                        placeholder="Start Date"
                        keyboardType='number-pad'
                        style={styles.dateInput}
                    >
                        <Text style={{ fontSize: 18, color: '#000' }}>
                            {
                                new Date(this.state.endDate).toDateString()
                            }
                        </Text>
                    </TouchableOpacity>
                    <DropDownPicker
                        placeholder="Choose duration"
                        defaultValue={this.state.duration}
                        zIndex={4}
                        items={
                            generateMonths(
                                0,24
                            )
                        }
                        itemStyle={{ justifyContent: 'flex-start' }}
                        style={{ 
                            borderColor: '#959595', 
                            borderWidth: 2, 
                            margin: 5, 
                            borderRadius: 5, 
                            width:DROP_WIDTH
                        }}
                        containerStyle={{ height:DROP_HEIGHT }}
                        onChangeItem={item=>{
                            try {
                                const _dur=item.value;
                                this.setState((state)=>(
                                    {
                                        duration:_dur,
                                        amount:parseFloat(state.room?.price)*parseInt(_dur)
                                    }
                                ))
                            } catch (err) {}
                        }}
                    />
                    <DropDownPicker
                        zIndex={3}
                        placeholder="Payment method"
                        defaultValue={this.state.paymentType}
                        items={[
                            { label: 'Cash', value: 'CASH' },
                            { label: 'Mobile Money', value: 'MOMO' },
                            { label: 'Bank Deposit', value: 'BANK DEPOSIT' },
                        ]}
                        itemStyle={{ justifyContent: 'flex-start' }}
                        style={{ 
                            borderColor:'#959595', 
                            borderWidth:2, 
                            margin:5,
                            borderRadius:5,
                            width:DROP_WIDTH
                        }}
                        containerStyle={{ height:DROP_HEIGHT }}
                        onChangeItem={item=>this.setState(
                            { 
                                paymentType:item.value
                            }
                        )}
                    />
                    <Button
                            loading={this.state.loading}
                            onPress={this.sendRequest.bind(this)}
                            buttonStyle={globalStyles.button} 
                            title="RENEW" 
                        />
                </ScrollView>
            </>
        )
    }
}



const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        width: "100%",
        alignItems: 'center',
        marginTop: 25

    },
    horizontal: {
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 30,
        marginBottom: 23
    },
    dateInput: {
        borderRadius: 5,
        borderWidth: 2,
        borderColor: '#959595',
        paddingLeft:10,
        width:DROP_WIDTH,
        height:65,
        justifyContent: 'center',
        marginVertical:5
    },
    label:{
        marginHorizontal:'5%',
        alignSelf:'flex-start',
        fontSize:20,
        marginVertical:15,
        color:'#6d6d6d'
    }
});