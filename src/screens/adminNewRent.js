import React, { Component } from 'react'
import { View, StyleSheet, TouchableOpacity, Alert, Dimensions,ScrollView } from 'react-native'
import { Button, Text } from 'react-native-elements';
import NTextInput from '../components/textInput'
import DropDownPicker from 'react-native-dropdown-picker';
import globalStyles, { DROP_HEIGHT, DROP_WIDTH } from '../constants/styles';
import Header from '../components/header';
import { AuthConsumer } from '../store/auth.state';
import generateMonths from "../utils/generateMonths";
import apiService from '../services/api.service';
const { height, width } = Dimensions.get('window');


class AdminNewRent extends Component {

    viewLoaded=false;

    state={
        loading:false,
        duration:0,
        durations:[],
        paymentType: '',
        rentType:'',
        amount:'0',
        room:{},
        tenant:{},
        tenants:[],
        properties:[]
    }

    componentDidMount() {
        try {
            this.viewLoaded=true;
            this.loadTenants();
            this.loadProperties();
            this.loadDuration();
        } catch (err) {

        }
    }

    reset=(callback)=>{
        this.setState(
            {
                duration:0,
                paymentType: '',
                rentType:'',
                amount:'0',
                room:{},
                tenant:{}
            },callback
        )
    }


    loadDuration=()=>{
        try {
            const durations=generateMonths(
                1,
                36
            )
            this.setState(
                {
                    durations:durations
                }
            )
        } catch (err) {
        }
    }

    loadTenantProperties=async()=>{
        try {
            this.setState(
                {
                    loading:true
                }
            );
            const {tenant}=this.state;
            let properties=[];
            const response=await apiService.getTenantRents(tenant._id);
            if(!response.success){
                throw Error(
                    response[
                        'message'
                    ]
                )
            };
            response.data.forEach(rent=>{
                if(rent?.room){
                    properties.push(
                        {
                            label:`${rent?.room?.type}~${rent?.room?.location}`,
                            value:JSON.stringify(rent?.room || {})
                        }
                    )
                }
            })
            this.setState(
                {
                    loading:false,
                    properties:properties
                }
            )
        } catch (err) {
            this.setState(
                {
                    loading:false
                }
            )
        }
    }


    loadProperties=async()=>{
        try {
            this.setState(
                {
                    loading:true
                }
            )
            const response=await apiService.getRooms();
            if(!response.success){
                throw Error(
                    response[
                        'message'
                    ]
                )
            }
            const rooms=response.data.map(room=>{ return { label:room.type,value:JSON.stringify(room) }});
            this.viewLoaded && this.setState(
                {
                    loading:false,
                    properties:rooms
                }
            )
        } catch (err) {
            this.setState(
                {
                    loading:false
                },this.loadProperties
            )
        }
    }


    loadTenants=async()=>{
        try {
            this.setState(
                {
                    loading:true
                }
            )
            const response=await apiService.getTenants();
            if(!response.success){
                throw Error(
                    response[
                        'message'
                    ]
                )
            }
            const tenants=response.data.map(tenant=>{ return { label:`${tenant.firstname}${tenant.lastname}`,value:JSON.stringify(tenant) }});
            this.viewLoaded && this.setState(
                {
                    loading:false,
                    tenants:tenants
                }
            )
        } catch (err) {
            this.setState(
                {
                    loading:false
                },this.loadTenants
            )
        }
    }



    calculateRoomPrice=(duration)=>{
        const{
            room,
        }=this.state;
        const amount=room.price * duration;
        return this.setState(
            {
                duration:duration,
                amount:amount
            }
        )
    }



    saveData=async(data)=>{
        try {
            this.setState(
                {
                    loading:true
                }
            );
            const response=await apiService.newAdminRent(data);
            if(!response.success){
                throw Error(
                    response.message
                )
            };
            this.setState(
                {
                    loading:false
                },()=>{
                    Alert.alert(
                        'yay!',
                        'Request processed successfull and pending approval'
                    )
                }
            )
        } catch (err) {
            this.setState(
                {
                    loading:false
                },()=>{
                    Alert.alert(
                        'Oops!',
                        err.message
                    )
                }
            )
        }
    }

    sendRequest=()=>{
        const {
            paymentType,
            tenant,
            duration,
            amount,
            room,
            rentType
        }=this.state;
        const data={
            "room":room['_id'],
            "tenant":tenant['_id'],
            "duration":duration,
            "costPerMonth":room['price'],
            "paymentType":paymentType,
            "amount":amount,
            'rentType':rentType
        };
        if(data.amount<=0 || data.paymentType==='' || data['duration']===0){
            return;
        }
        return Alert.alert(
            'Hello! Tenant',
            'You have requested to make reservation for this property to tenant selected, confirm your request.',
            [
                {
                    text:'Yes,continue',
                    onPress:()=>this.saveData(data)
                },
                {
                    text:'No,cancel',
                    onPress:()=>this.props.navigation.goBack()
                }
            ]
        )
    }


    componentWillUnmount(){
        this.viewLoaded=false;
    }

    render() {
        return (
            <ScrollView>
                <Header name="Rent property" back={this.props.navigation.goBack} />
                <View style={styles.container}>
                    <View style={{ 
                            width: '80%', 
                            alignItems: 'center' 
                        }}>
                        <NTextInput 
                            editable={false} 
                            value={`GHS${this.state.amount}`} 
                        />
                        <NTextInput
                            editable={false}
                            value={`GHS${this.state.room?.price || 0} / Month`}
                        />
                        <DropDownPicker
                            zIndex={5}
                            placeholder="Rent type"
                            items={[
                                { label: 'New', value: 'NEW' },
                                { label: 'Renewal', value: 'RENEWAL' }
                            ]}
                            itemStyle={{ justifyContent: 'flex-start' }}
                            style={{ 
                                borderColor:'#959595', 
                                borderWidth:2, 
                                margin:5,
                                borderRadius:5, 
                                width:DROP_WIDTH 
                            }}
                            containerStyle={{ height:DROP_HEIGHT }}
                            onChangeItem={item=>{
                                this.reset(()=>{
                                    this.state.rentType=item.value;
                                    if(this.state.rentType==='NEW'){
                                        this.loadProperties();
                                    }
                                })
                            }}
                        />
                        <DropDownPicker
                            zIndex={4}
                            placeholder="Choose Tenant"
                            items={this.state.tenants}
                            itemStyle={{ justifyContent: 'flex-start' }}
                            style={{
                                borderColor:'#959595', 
                                borderWidth:2,
                                margin:5,
                                borderRadius:5,
                                width:width * 0.8
                            }}
                            containerStyle={{ height: 60 }}
                            onChangeItem={item=>{
                                const tenant=JSON.parse(item.value);
                                this.setState(
                                    {
                                        tenant:tenant
                                    },()=>{
                                        if(this.state.rentType==='RENEWAL'){
                                            this.loadTenantProperties();
                                        }
                                    }
                                )
                            }}
                        />
                        <DropDownPicker
                            zIndex={3}
                            placeholder="Choose Property"
                            items={this.state.properties}
                            itemStyle={{ justifyContent: 'flex-start' }}
                            style={{
                                borderColor:'#959595', 
                                borderWidth:2,
                                margin:5,
                                borderRadius:5,
                                width:width * 0.8
                            }}
                            containerStyle={{ height: 60 }}
                            onChangeItem={item=>this.setState({room:JSON.parse(item.value)},this.loadDuration.bind(this))}
                        />
                        <DropDownPicker
                            zIndex={2}
                            placeholder="Select Duration"
                            items={this.state.durations}
                            itemStyle={{ justifyContent: 'flex-start' }}
                            style={{
                                borderColor:'#959595', 
                                borderWidth:2,
                                margin:5,
                                borderRadius:5,
                                width:DROP_WIDTH
                            }}
                            containerStyle={{ height:DROP_HEIGHT }}
                            onChangeItem={item=>this.calculateRoomPrice(item.value)}
                        />
                        <DropDownPicker
                            zIndex={1}
                            placeholder="Payment method"
                            items={[
                                { label: 'Cash', value: 'CASH' },
                                { label: 'Mobile Money', value: 'MOMO' },
                                { label: 'Bank Deposit', value: 'BANK DEPOSIT' },
                            ]}
                            itemStyle={{ justifyContent: 'flex-start' }}
                            style={{ 
                                borderColor:'#959595', 
                                borderWidth:2, 
                                margin:5,
                                borderRadius:5, 
                                width:DROP_WIDTH
                            }}
                            containerStyle={{ height:DROP_HEIGHT }}
                            onChangeItem={item=>this.setState(
                                { 
                                    paymentType:item.value
                                }
                            )}
                        />
                    </View>
                </View>
                <Button
                    onPress={this.sendRequest.bind(this)}
                    containerStyle={{
                        bottom: 0,
                        alignSelf:'center',
                        marginTop:100
                    }}
                    loading={this.state.loading}
                    buttonStyle={globalStyles.button}
                    title="CONTINUE"
                />
            </ScrollView>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        width: "100%",
        alignItems: 'center',
        marginTop: 25

    },
    horizontal: {
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 30,
        marginBottom: 23
    }
});

export default AdminNewRent;
