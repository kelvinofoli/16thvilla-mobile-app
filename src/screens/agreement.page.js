import React, { Component } from 'react'
import { Text, View, ScrollView, StyleSheet, Image } from 'react-native'
import Header from '../components/header';
const logo = require("../assets/images/logo.png");
import moment from "moment";



export default class AgreementPage extends Component {

    state={

    }

    componentDidMount(){
        this.setState(
            {
                ...this.props.route.params
            }
        )
    }
    
    render() {
        return (
            <ScrollView style={{
                padding:15,
                backgroundColor:'#fff',
                marginVertical:20
            }}>
                <Header 
                    name="Tenancy Agreement" 
                    back={this.props.navigation.goBack} 
                />

                <View style={{
                    alignItems:'center',
                    marginVertical:50                  
                }}>
                    <Image 
                        style={{height:200,width:250,top:50}} 
                        source={logo}
                    />

                </View>

                <View style={styles.line}></View>
                <Text style={{textAlign:'center',fontSize:20}}>TENANCY AGREEMENT</Text>
                <View style={styles.line}></View>
                <Text style={{textAlign:'center',fontSize:20}}>BETWEEN</Text>
                <Text style={styles.text}>16TH AUGUST,85 VILLA</Text>
                <Text style={styles.text}>AND</Text>
                <Text style={styles.text}>{this.state.tenant?.firstname} {this.state.tenant?.lastname}</Text>
                <Text style={styles.text1}>DATED THIS  <Text></Text> DAY OF <Text>{moment(this.state.startDate).format('DD')}</Text>, {moment(this.state.startDate).format('YYYY')}</Text>
                <View style={styles.line}></View>
                <Text >This TENANCY AGREEMENT (the “Agreement) is made this {moment(this.state.startDate).format('DD')} day of {moment(this.state.startDate).format('YYYY')}, 
2020.
</Text>
                <Text style={styles.text1}>BETWEEN</Text>
                <Text>16TH AUGUST,85 VILLA represented by Mr. Prince Charles Kofi Akoto of P.O. Box GP 1591, 
Accra, Ghana with Ghana Post Digital Address GE-306-0321 (hereinafter called the LANDLORD
which expression shall where the context so admits include its assigns, administrators, 
executors, personal successors and agents) of the first part; 
</Text>
                    <Text style={styles.text1}>AND</Text>
                    <Text><Text style={{fontWeight:'bold'}}>{`${this.state.tenant?.firstname} ${this.state.tenant?.lastname}`}</Text> of <Text style={{fontWeight:'bold'}}>{this.state.tenant?.occupation},{this.state.tenant?.address}</Text> (hereinafter called the TENANT which expression shall 
where the context admits include its assigns and agents) of the second part.</Text>

                <Text style={styles.header}>RECITALS</Text>

                <Text style={styles.headerText}>A. WHEREAS the LANDLORD is the legal and beneficial owner of a <Text style={{fontWeight:'bold'}}>{this.state.room?.type}</Text> 
 property located at <Text style={{fontWeight:'bold'}}>{this.state.room?.location}</Text>, 
Accra, Ghana, with Digital Address <Text style={{fontWeight:'bold'}}>{this.state.room?.address}</Text> and offer to let the same for a 
period of <Text style={{fontWeight:'bold'}}>{this.state.duration}</Text> months with an option of rent reviewed at the end of 60 
months.</Text>

                        <Text style={styles.headerText}>B. WHEREAS the LANDLORD has offered his aforementioned property to the TENANT for 
residential use.</Text>

<Text style={styles.headerText}>C. WHEREAS the LANDLORD and TENANT agree that the foregoing recitals bind them to 
conclude negotiations and serve as an understanding on the basis of which they shall 
co-operate.</Text>

                        <Text style={styles.header}>NOW THIS AGREEMENT WITNESSETH AS FOLLOWS:</Text>

        <Text style={styles.headerText}>1. The LANDLORD lets and TENANT takes ALL THAT dwelling house situated at <Text style={{fontWeight:'bold'}}>{this.state.room?.location}</Text>, Ghana, with Digital Address <Text style={{fontWeight:'bold'}}>{this.state.room?.address}</Text> TO HOLD the same as TENANT for a period of  <Text style={{fontWeight:'bold'}}>{this.state.duration}</Text> months from  
        <Text style={{fontWeight:'bold'}}>{new Date(this.state.startDate).toDateString()}</Text> YIELDING and PAYING therefore the monthly rent of 
        <Text style={{fontWeight:'bold'}}> {`${this.state.currency || 'USD'}${this.state.costPerMonth}`}</Text> and payable for  <Text style={{fontWeight:'bold'}}> {this.state.duration}</Text> months in advance.</Text>

<Text style={styles.headerText}>2. The TENANT hereby covenants with the LANDLORD as follows:</Text>

<Text style={styles.headerText1}>(a) To pay the said rent in the manner prescribed in this Agreement.</Text>

<Text style={styles.headerText1}>(b) To pay a further advance Rent for 3 months, which shall be held by the Landlord as 
security for any damage which may be caused to the property, and which the 
TENANT shall fail to perfectly repair. Provided that such money will be refunded to 
the TENANT if the TENANT does not cause any damage to the LANDLORD’S 
property or if the Tenant repairs all damages caused to the property before leaving 
the rented premises.</Text>

<Text style={styles.headerText1}>(b) Not without the previous consent in writing of the LANDLORD to sublet or part 
possession of any part of the premises.</Text>

    <Text style={styles.headerText1}>(c) To replace and repair forthwith all such properties belonging to LANDLORD and used 
by the TENANT as shall be destroyed, damaged or rendered useless by them or their 
servants, agents, invitees, licensees, with others of equal value, similar dimension 
and of similar pattern.</Text>

<Text style={styles.headerText1}>(d) Not to carry or permit or suffer to be carried on the rented premises or any part 
thereof anything which may be or become a nuisance or annoyance or cause damage 
to occupiers of adjoining premises and to use the said premises for the purpose of 
private dwelling house.</Text>

<Text style={styles.headerText1}>(e) To pay all charges for gas, electricity and water supplied to and or consumed on the 
demised premises by the TENANT and all charges for conservancy and telephone 
services.</Text>


<Text style={styles.headerText1}>(f) To keep the structure of the said premises and drains in good and substantial state 
of repair.</Text>

<Text style={styles.headerText1}>(g) To peaceably yield up the rented or demised premises together with all fixtures 
and fittings at the expiration of the Tenancy or sooner determination of the 
Tenancy.</Text>

<Text style={styles.header}>3. THE LANDLORD AGREES WITH THE TENANT AS FOLLOWS:</Text>

<Text style={styles.headerText}>(a) That the TENANT paying the rent hereby reserved and performing and observing all 
the covenants and stipulations herein on their part contained shall quietly and enjoy 
the said premises without any interruption from or by the LANDLORD or any person 
rightfully claiming. Accordingly, the LANDLORD reserves the exclusive right to evict 
the TENANT forthwith if any Rent remains unpaid by the TENANT one week after 
same is due.</Text>


<Text style={styles.headerText}>(b) The tenant of the premises shall be responsible for the safety and security of his 
belongings and the TENANT do not hold the LANDLORD responsible for any lapses 
on the part of other tenant.</Text>


<Text style={styles.headerText}>(c) The LANDLORD consents that the TENANT may use the premises for dwelling purpose 
only but shall not otherwise part with the possession of the said premises or any 
part thereof or remove or attempt to remove the fixtures, fittings, articles, effect 
and things hereby to any other place whatsoever.</Text>


<Text style={styles.headerText}>(d) To keep the main roof, main timbers, external wall of the said premises in good 
tenable repair and condition.</Text>


<Text style={styles.headerText}>(b) To pay all rates, ground rent and assessment imposed upon the demised premises.</Text>

<Text style={styles.headerText}>(e) To keep the exterior of the premises in good and tenantable state of repairs.</Text>


<Text style={styles.header}>4. REPAIRS & MAINTENANCE.</Text>

<Text style={styles.headerText}>(a) The Tenant shall be responsible at his or her expense for keeping the rented 
premises and its fixtures in good and tenantable condition by undertaking 
routine cleaning & repairs of the premises and fittings/fixtures. Provided that 
all repairs of the premises shall be done by the Landlord’s authorized 
personnel or professionals, a list of which is annexed to this Agreement as 
Annex A.</Text>


<Text style={styles.headerText}>(b) The premises shall be painted by the Landlord prior to handing over to the 
Tenant, and the Tenant shall keep the inside walls clean at all times. The 
Tenant shall paint the inside walls of the premises prior to final handing over 
of premises to the Landlord before vacating the premises.</Text>


<Text style={styles.headerText}>(c) The Tenant shall restore the rented premises to a state of tenantable repair 
before quitting or vacating it, at the end of the tenancy period (i.e. keep the 
property and its surroundings in good substantial state of repair and 
condition).</Text>


<Text style={styles.headerText}>(d) The Landlord shall be responsible for the exterior painting, and shall paint 
the walls at least once every two years.</Text>


<Text style={styles.header}>5. UTILITIES</Text>


<Text style={styles.headerText}>(a) The Landlord shall provide separate meters for electricity for the rented 
premises.</Text>


<Text style={styles.headerText}>(b) The Tenant shall pay all charges for water, sewer, electricity, telephone and 
other services and utilities used by the Tenant on the rented Premises during 
the term of this Tenancy unless otherwise expressly agreed in writing by the 
Landlord.</Text>

<Text style={styles.headerText}>(c) The Tenant shall also be responsible for fueling a standby generator set (if 
any).</Text>


<Text style={styles.header}>6. IMPROVEMENTS AND FIXTURES</Text>

<Text style={styles.headerText}>The Landlord agrees to rent the Property together with all improvements, 
buildings and fixtures presently on the Property including but not limited to 
electricity, heating, air conditioning, plumbing equipment, built-in appliances, 
screens, storm windows, doors, attached carpeting, fitted kitchen, wash hands 
basins, trees, shrubs, flowers and fences.</Text>


<Text style={styles.header}>7. SECURITY, CLEANING AND MAINTENANCE FEE</Text>

<Text style={styles.headerText}>The Tenant agrees that at any time while in occupation of the rented Property 
he shall pay an amount of <Text style={{fontWeight:'bold'}}>GHS50</Text>. The maintenance of the water pumping machine every 
month and where the need by be used to maintain or repair water pumping machine.</Text>


<Text style={styles.header}>8. ENTRY</Text>

<Text style={styles.headerText}>In case of emergency or suspected damage to property, and with a minimum of 
24 hours’ notice to the Tenant, unless the Tenant grants permission otherwise, 
the Landlord shall have the right to enter upon the rented premises at 
reasonable hours to inspect the same, provided that the Landlord shall not 
thereby unreasonably interfere with the Tenant’s quite enjoyment of the rented 
premises. Accordingly, routine repairs and general maintenance must be 
scheduled and agreed by both parties.</Text>


<Text style={styles.header}>9. DEFAULT</Text>

<Text style={styles.headerText}>(a) If default shall at any time be made by the Tenant in the payment of 
Rent when due to the Landlord as herein provided, and if the said default 
shall continue for seven (7) days after notice thereof shall have been 
given to the Tenant by the Landlord, or if default shall be made in any of 
the other covenants or conditions to be kept, observed and performed by 
the Tenant, and such default shall continue for fourteen (14) days after 
notice thereof in writing to the Tenant by the Landlord without 
correction thereof, the Landlord may declare the term of this Lease 
ended and terminated by giving the Tenant written notice of such 
intention, and if possession of the Rented premises is not surrendered, 
the Landlord may re-enter the said premises.</Text>


<Text style={styles.headerText}>(b) The Landlord shall have, in addition to the remedy above provided, any 
other right or remedy available to the Landlord on account of any Tenant 
default, either in law or equity. The Landlord shall use reasonable efforts 
to mitigate his damages.</Text>


<Text style={styles.header}>10. DISPUTE RESOLUTION</Text>

<Text style={styles.headerText}>(a) In the event of any dispute arising between the Parties relating to or 
arising out of this Agreement, including the implementation, execution, 
interpretation, rectification, termination or cancellation of this 
Agreement, the dispute shall in the first instance be settled amicably 
between the Parties, following a request by a Party for the dispute to be 
settled.</Text>


<Text style={styles.headerText}>(b) In the event of the dispute not having been resolved within 10 Business 
Days of the date of such request or such longer period as the Parties may 
agree to in writing, then either Party may elect to bring an action at an 
appropriate Court or forum for the resolution of disputes in Ghana to 
resolve the dispute.</Text>


<Text style={styles.header}>11. RENEWAL OF THE TENANCY</Text>

<Text style={styles.headerText}>The Term hereby granted may be renewed at the discretion of the Landlord for a 
further term of <Text style={{fontWeight:"bold"}}>{this.state.duration}</Text> months upon terms and conditions to be agreed upon by 
both parties, by the Tenant giving to the Landlord One (1) Month written notice 
before the expiration of the present term.</Text>

<Text style={styles.header}>12. NOTICE</Text>

<Text style={styles.headerText}>Any notice required to be served on either party may be served by delivering it to 
such personally or through the post in prepaid enveloped addressed to such at his 
last known address and in such case, the notice shall be deemed to have been 
served at the expiration of two (2) clear days after it is posted in Ghana and in 
providing such service, it shall be sufficient to prove that it was so posted.</Text>


<Text style={styles.header}>13. NON - ASSIGNMENT</Text>
<Text style={styles.headerText}>The Agreement or any obligation thereunder is not assignable or otherwise transferred in 
whole or in part by either Party without the prior written consent of the other Party 
which consent shall not be unreasonably withheld or delayed.</Text>


<Text style={styles.header}>14. SEVERABILITY</Text>
<Text style={styles.headerText}>If any provision of this Agreement is found by any Court of competent jurisdiction to be 
invalid or unenforceable, the remainder of such provision or part of this Agreement shall 
be interpreted so as best to effect the intent of the Parties.</Text>

<Text style={styles.header}>15. AMENDMENT</Text>
<Text style={styles.headerText}>This Agreement should not be altered, modified, or changed except with the written 
consent of the Parties.</Text>


<Text style={styles.header}>16. WAIVER</Text>
<Text style={styles.headerText}>The failure by either Party to exercise any right provided in this Agreement shall not be a 
waiver of prior or subsequent rights.</Text>


<Text style={styles.header}>17. ENTIRE AGREEMENT</Text>
<Text style={styles.headerText}>To the extent permitted by law, in relation to the subject matter of this Agreement, this 
Agreement (a) embodies the entire understanding of the Parties and constitutes the 
entire terms agreed upon between the Parties; and (b) supersedes any prior agreement 
(whether or not in writing) between the Parties. To be effective, any modification of this 
Agreement must be in writing and signed by both parties hereto.</Text>


<Text style={styles.header}>18. GOVERNING LAW AND JURISDICTION</Text>
<Text style={styles.headerText}>This Agreement shall be construed and interpreted in accordance with and 
governed by the laws of Ghana, and the Courts of Ghana shall have exclusive 
jurisdiction over matters arising out of or relating to this Agreement.</Text>


<Text style={styles.headerText1}>IN WITHNEES WHEREOF the parties hereto have hereunto set their HANDS AND
SEALS the day and year first above-written.</Text>

<Text style={styles.headerText2}>SIGNED FOR AND BEHALF OF THE
Within-named LANDLORD 
by Prince Charles Kofi Akoto. Herein the presence<Text style={{fontWeight:'bold'}}>{this.state.tenant?.fullname}</Text> of <Text style={{fontWeight:'bold'}}>{this.state.tenant?.firstname} {this.state.tenant?.lastname}</Text></Text>



            </ScrollView>
        )
    }
}



const styles = StyleSheet.create({
    line:{
        width:'95%',
        height:1,
        backgroundColor:'#000',
        marginRight:'auto',
        marginLeft:'auto',
        marginVertical:20
    },
    text:{
        textAlign:'center',
        marginVertical:20,
        fontSize:15
    },
    text1:{
        textAlign:'center',
        marginVertical:20,
        fontSize:15
    },
    header:{
        fontWeight:'bold',
        marginVertical:15,
        fontSize:20
    },
    headerText:{
        marginVertical:10
    },
    headerText1:{
        marginVertical:10,
        marginLeft:10
    },
    headerText2:{
        fontWeight:'bold',
        marginVertical:20,
        fontSize:20
    },
});