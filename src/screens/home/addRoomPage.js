import React, { Component } from 'react'
import { View, StyleSheet, ScrollView,Dimensions } from 'react-native'
import { Button, Text } from 'react-native-elements';
import Header from '../../components/header';
import NTextInput from '../../components/textInput'
import globalStyles, { DROP_HEIGHT, DROP_WIDTH } from '../../constants/styles';
import DropDownPicker from 'react-native-dropdown-picker';
import { Alert } from 'react-native';
import { StatusBar } from 'react-native';
const { height,width } = Dimensions.get('window');



export default class AddRoomPage extends Component {


    state={
        "type":"",
        "location":"",
        "address":"",
        "size":"",
        "condition":"",
        "furnishing":"",
        "roomNumber":"",
        "price":"",
        "details":"",
        accomodation:'',
        inspectionAmount:'',
        currency:''
    }


    proceed=()=>{
        try {
            Object.values(
                this.state
            ).forEach(v=>{
                if(v.length<=0){
                    throw Error(
                        'Please fill all fields'
                    )
                }
            })
            return this.props.navigation.navigate(
                'UploadImages',
                this.state
            )
        } catch (err) {
            Alert.alert(
                'Oops',
                err.message
            )
        }
    }

    render() {
        return (<>
        <StatusBar barStyle="dark-content"/>
            <Header name="Add room" back={this.props.navigation.goBack} />
            <ScrollView style={{ flex: 1 }}>
                <View style={styles.container}>
                    <View >
                        <NTextInput 
                            placeholder="Location"
                            onChangeText={
                                (v)=>this.state.location=v
                            }
                        />
                        <NTextInput  
                            placeholder="Address"
                            onChangeText={
                                (v)=>this.state.address=v
                            }
                        />
                        <NTextInput 
                            placeholder="Size"
                            onChangeText={
                                (v)=>this.state.size=v
                            }
                        />
                        <DropDownPicker
                             zIndex={5}
                            placeholder="Apartment type"
                            items={[
                                { label: 'CHAMBER AND HALL', value: 'CHAMBER AND HALL' },
                                { label: '2 BEDROOMS', value: '2 BEDROOMS' },
                                { label: '1 BEDROOM', value: '1 BEDROOMS' }
                            ]}
                            itemStyle={{ justifyContent: 'flex-start' }}
                            style={[styles.dropPicker,{backgroundColor:'none'}]}
                            containerStyle={{ height:DROP_HEIGHT }}
                            onChangeItem={item =>this.state.type=item.value}
                        />
                        <DropDownPicker
                             zIndex={4}
                            placeholder="Condition"
                            items={[
                                { label: 'New', value: 'NEW' },
                                { label: 'Pre Occupied', value: 'PRE-OCCUPIED' }
                            ]}
                            itemStyle={{ justifyContent: 'flex-start' }}
                            style={[styles.dropPicker,{backgroundColor:'none'}]}
                            containerStyle={{ height:DROP_HEIGHT }}
                            onChangeItem={item =>this.state.condition=item.value}
                        />
                        <DropDownPicker
                             zIndex={3}
                            placeholder="Furnishing"
                            items={[
                                { label: 'Semi Furnished', value: 'SEMI FURNISHED' },
                                { label: 'Fully Furnished', value: 'FULLY FURNISHED' }
                            ]}
                            itemStyle={{ justifyContent: 'flex-start' }}
                            style={[styles.dropPicker,{backgroundColor:'none'}]}
                            containerStyle={{ height:DROP_HEIGHT }}
                            onChangeItem={item =>this.state.furnishing=item.value}
                        />
                        <DropDownPicker
                             zIndex={2}
                            placeholder="Accomodation Type"
                            items={[
                                { label: 'Short Stay', value: 'SHORT STAY' },
                                { label: 'Long Stay', value: 'LONG STAY' }
                            ]}
                            itemStyle={{ justifyContent: 'flex-start' }}
                            style={[styles.dropPicker,{backgroundColor:'none'}]}
                            containerStyle={{ height:DROP_HEIGHT }}
                            onChangeItem={item =>this.state.accomodation=item.value}
                        />
                        <DropDownPicker
                             zIndex={1}
                            placeholder="Currency"
                            items={[
                                { label: 'Ghana Cedis', value: 'GHS' },
                                { label: 'US Dollars', value: 'USD' }
                            ]}
                            itemStyle={{ justifyContent: 'flex-start' }}
                            style={[styles.dropPicker,{backgroundColor:'none'}]}
                            containerStyle={{ height:DROP_HEIGHT }}
                            onChangeItem={item =>this.state.currency=item.value}
                        />
                        <NTextInput 
                            placeholder="Room Number"
                            keyboardType='number-pad'
                            onChangeText={
                                (v)=>this.state.roomNumber=v
                            }
                        />
                        <NTextInput 
                            placeholder="Price/ Month"
                            keyboardType='number-pad'
                            onChangeText={
                                (v)=>this.state.price=v
                            }
                        />
                        <NTextInput 
                            placeholder="Inspection Amount"
                            keyboardType='number-pad'
                            onChangeText={
                                (v)=>this.state.inspectionAmount=v
                            }
                        />
                        <NTextInput 
                            placeholder="Details"
                            onChangeText={
                                (v)=>this.state.details=v
                            }
                        />
                    </View>
                    <Button 
                        onPress={this.proceed.bind(this)} 
                        buttonStyle={globalStyles.button} 
                        title="DONE"
                    />
                </View>

            </ScrollView>
        </>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        justifyContent:'center',
        width: "100%",
        alignItems: 'center',
        paddingTop: 10,
    },
    horizontal: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    dropPicker:{
        borderColor: '#959595', 
        borderWidth: 2, 
        marginVertical: 2.7, 
        borderRadius: 5, 
        width:DROP_WIDTH
    }
});
