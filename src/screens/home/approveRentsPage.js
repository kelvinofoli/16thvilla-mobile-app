import React, { Component } from 'react'
import { View, StyleSheet, TouchableOpacity, ScrollView, Image,ActivityIndicator } from 'react-native'
import { Text } from 'react-native-elements'
import Header from '../../components/header'
import colors from '../../constants/colors'
import IconMA from 'react-native-vector-icons/MaterialIcons';
import apiService from '../../services/api.service';
import moment from "moment";
import RentListItem from '../../components/rentList.component'

export default class ApproveRentsPage extends Component {

    viewLoaded=false;

    state={
        rents:[],
    }

    componentDidMount(){
        this.getPending();
    }

    getPending=async()=>{
        try {
            this.viewLoaded=true;
            this.setState(
                {
                    loading:true
                }
            )
            const response=await apiService.getPendingRents();
            if(!response.success){
                throw Error(
                    response.message
                )
            }
            return this.viewLoaded && this.setState(
                {
                    loading:false,
                    rents:response.data
                }
            )
        } catch (err) {
            this.viewLoaded && this.setState(
                {
                    loading:false
                }
            )
        }
    }


    componentWillUnmount(){
        this.viewLoaded=false;
    }


    render() {
        return (<>
            <Header name="Approve Rents" back={this.props.navigation.goBack} />
            <ScrollView style={styles.container}>
            {this.state.loading && <ActivityIndicator size={20} color="#6d6d6d"/>}
            {this.state.rents.length<=0 && <Text style={{
                    textAlign:'center',
                    color:'#6d6d6d',
                    fontSize:13,
                    marginTop:'20%'
                }}>You dont have pending approvals</Text>
            }
            
            { this.state.rents.map((el,id)=>{
                return (
                    <RentListItem
                        el={el}
                        id={id}
                        {...this.props}
                        onPress={()=>{
                            this.props.navigation.navigate(
                                'approvalDetails',
                                el
                            )
                        }}
                    />
                )
            }) }
            </ScrollView>
        </>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 10
    },
    horizontal: {
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 25,
        marginBottom: 23
    },
    rentItem: {
        width: '100%',
        borderBottomWidth: 3,
        flexDirection: 'row',
        borderColor: '#ddd',
        // paddingHorizontal: "10%",
        paddingVertical: 10
    },
    profilePicture: {
        width: 70,
        height: 70,
        borderRadius:5,
        marginLeft:10,
        marginTop:5,
        borderColor:'#ddd',
        borderWidth:1
    }
})
