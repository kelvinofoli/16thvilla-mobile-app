import React, { Component } from 'react'
import { View, StyleSheet, ScrollView, TouchableOpacity,Alert,Dimensions,Platform,StatusBar } from 'react-native'
import NTextInput from '../../components/textInput'
import { Text ,Button} from 'react-native-elements'
import Header from '../../components/header'
import globalStyles, { DROP_HEIGHT, DROP_WIDTH } from '../../constants/styles';
import apiService from '../../services/api.service'
import { AuthConsumer } from '../../store/auth.state';
import DropDownPicker from 'react-native-dropdown-picker';
import { KeyboardAvoidingView } from 'react-native'
const {
    width
}=Dimensions.get('screen');


export default class CreateReportPage extends Component {

    isLoaded=false;

    state={
        loading:false,
        title:'',
        message:'',
        rentId:'',
        room:'',
        rents:[]
    }

    componentDidMount(){
        this.isLoaded=true;
        this.getRents();
    }


    componentWillUnmount(){
        this.isLoaded=false;
    }


    getRents=()=>apiService.getTenantRents(this.user._id).then((res)=>{
        if(res.success){
            return this.setState(
                {
                    loading:false,
                    rents:res.data
                }
            )
        }
    },err=>this.isLoaded && this.getRents());


    sendReport=async()=>{
        try {
            const{
                title,
                message,
                rentId,
                room
            }=this.state;
            if(title==='' || message==='' || rentId===''){
                throw Error(
                    'Please fill all inputs'
                )
            }
            this.setState(
                {
                    loading:true
                }
            );
            const data={
                tenant:this.user._id,
                sender:'TENANT',
                title:title,
                message:message,
                rentId:rentId,
                room:room
            }
            const response=await apiService.newReport(data);
            if(!response.success){
                throw Error(
                    response.message
                )
            }
            return this.setState(
                {
                    loading:false
                },()=>{
                    this.props.navigation.navigate(
                        'viewReportsPage'
                    )
                    Alert.alert(
                        'Yay!',
                        response.message
                    )
                }
            )
        } catch (err) {
            this.setState(
                {
                    loading:false
                },()=>Alert.alert(
                    'Oops!',
                    err.message
                )
            )
        }
    }


    render() {
        return (<>
                {Platform.OS==='ios' && <StatusBar barStyle="dark-content"/>}
            <AuthConsumer>
                {
                    ({state})=>{
                        this.user=state[
                            'user'
                        ]
                    }
                }
            </AuthConsumer>
            <Header name="Create report" back={this.props.navigation.goBack} />
            <ScrollView contentContainerStyle={styles.container} keyboardShouldPersistTaps="always">
                <DropDownPicker
                    placeholder="Choose property"
                    items={this.state.rents.map(
                            rent=>{
                                return { 
                                    label:`${rent.room?.type} ~ ${rent.duration} months`, 
                                    value:`${rent._id}|${rent.room?._id}`
                                }
                            }
                        )
                    }
                    style={{ 
                        borderColor: '#959595', 
                        borderWidth: 2, 
                        margin: 5, 
                        borderRadius: 5, 
                        width:DROP_WIDTH 
                    }}
                    containerStyle={{ height:DROP_HEIGHT }}
                    onChangeItem={item=>{
                        const[rentId,room]=item.value.split('|');
                        this.setState(
                            { 
                                rentId:rentId,
                                room:room
                            }
                        )
                    }}
                />
                <NTextInput 
                    placeholder="Title"
                    onChangeText={
                        v=>this.state.title=v
                    }
                />
                <NTextInput 
                    placeholder="Details" 
                    _style={{minHeight: 150, justifyContent: "flex-start"}} 
                    multiline={true}
                    numberOfLines={4}
                    onChangeText={
                        v=>this.state.message=v
                    }
                />
                <Button
                    loading={this.state.loading}
                    buttonStyle={[globalStyles.button,{alignSelf:'center'}]} 
                    title="SEND"
                    onPress={this.sendReport.bind(this)}
                />
            </ScrollView>
            </>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 10,
        paddingHorizontal:'10%',
        marginTop:10,
        alignItems:'center'
    },
    horizontal: {
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 25,
        marginBottom: 23
    },
    rentItem: {
        width: '100%',
        borderBottomWidth: 3,
        borderColor: '#ddd',
        paddingHorizontal: "10%",
        paddingVertical: 10
    }
})
