import React, { Component } from 'react'
import { View, StyleSheet, TouchableOpacity, ScrollView, Image } from 'react-native'
import { Text, Badge, Button } from 'react-native-elements'
import IconFA from 'react-native-vector-icons/FontAwesome';
import globalStyles from '../../../constants/styles';
import Header from '../../../components/header'
import { Alert } from 'react-native';

export default class ApprovalDetails extends Component {


    state={};


    componentDidMount(){this.setState({ ...this.props.route.params })};


    sendRequest=()=>{
        try {
            this.props.navigation.navigate(
                'ChooseDuration',
                this.state
            )
        } catch (err) {
            Alert.alert(
                'Oops',
                err.message
            )
        }
    }


    render() {
        return (<>
            <Header name="Approval details" back={this.props.navigation.goBack} />

            <View style={styles.container}>
                <View style={[styles.rentItem, { borderBottomWidth: 3 }]}>
                    <Text h4>{this.state.room?.type}</Text>
                    <Text style={{marginTop:5}}>DURATION: {this.state.duration} MONTHS</Text>
                    <Text style={{marginTop:5}}>COST / MONTH: GHS{this.state.costPerMonth}</Text>
                    <Text style={{marginTop:5}}>RENT TYPE: {this.state.rentType}</Text>
                    <View style={{ 
                        paddingHorizontal:7, 
                        display: 'flex', 
                        alignSelf: 'flex-start', 
                        backgroundColor: 'gold', 
                        borderRadius:3,
                        paddingVertical:10,
                        marginTop:10
                    }}>
                        <Text style={{ color: 'white', fontWeight: "bold" }}>STATUS:  <Text style={{ fontWeight: "bold" }}>
                            {this.state.status}</Text></Text>
                    </View>
                </View>
                <View style={[styles.rentItem, { alignItems: 'flex-start' }]}>
                    {/* <Image 
                        style={styles.profilePicture} 
                        source={{uri:this.state.tenant?.photo}} 
                    /> */}
                    <Text style={{ fontSize: 20,marginVertical:10 }}>
                        {this.state.tenant?.firstname} {this.state.tenant?.lastname}
                    </Text>
                    <Text>AGE: {this.state.tenant?.age}</Text>
                    <Text>GENDER: {this.state.tenant?.gender}</Text>
                    <Text>MARRIED: {this.state.tenant?.married?'YES':'NO'}</Text>
                    <Text>OCCUPATION: {this.state.tenant?.occupation}</Text>
                    <Text>PHONE NUMBER : {this.state.tenant?.phone}</Text>
                </View>

                <Button 
                    buttonStyle={globalStyles.button} 
                    title="APPROVE"
                    onPress={this.sendRequest.bind(this)}
                />
            </View>
        </>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center'
    },
    horizontal: {
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 25,
        marginBottom: 23
    },
    rentItem: {
        width: '100%',
        borderColor: '#ddd',
        paddingHorizontal: "10%",
        paddingVertical: 20

    },
    profilePicture: {
        width: 80,
        height: 80,
        borderRadius:10,
        margin: 10
    }
})
