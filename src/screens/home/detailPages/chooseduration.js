import React, { Component } from 'react'
import { View,StyleSheet,Dimensions ,TouchableOpacity} from 'react-native';
import { Button, Text } from 'react-native-elements';
import globalStyles from '../../../constants/styles';
import Header from '../../../components/header'
import { Alert } from 'react-native';
import apiService from '../../../services/api.service';
import moment from "moment";
import DatePickerComponent from '../../../components/datePickerComponent';



export default class chooseduration extends Component {

    state={
        loading:false,
        startDate:new Date,
        endDate:new Date,
        paymentType:'',
        show:false,
        amount:'',
        dateType:"",
        data:{},
        showStart:false,
        showEnd:false,
        mode:'date'
    }

    componentDidMount(){
        const _data=this.props.route.params;
        this.setState(
            { 
                data:_data,
                endDate:moment(new Date()).add(_data['duration'],'months')
            }
        )
    }

    sendRequest=()=>{
        try {
            const{
                startDate,
                endDate,
                data,
                paymentType,
            }=this.state;
            if(startDate==='' || endDate===''){
                throw Error(
                    'Please fill all inputs'
                )
            }
            return Alert.alert(
                'APPROVE RENT',
                `you have requested to let out this property to ${data['tenant']['firstname']} for a period of ${data['duration']}, kindly confirm your request`,
                [
                    {
                        text:'Yes,continue',
                        onPress:async()=>{
                            try {
                                this.setState(
                                    {
                                        loading:true
                                    }
                                )
                                const response=await apiService.approveRent(
                                    {
                                        rentId:data['_id'],
                                        tenantId:data['tenant']['_id'],
                                        startDate:startDate,
                                        endDate:endDate,
                                        paymentType:paymentType,
                                        status:data['status'],
                                        amount:data['duration']*data['costPerMonth']
                                    }
                                );
                                if(!response.success){
                                    throw Error(
                                        response.message
                                    )
                                }
                                return this.setState(
                                    {
                                        loading:false
                                    },()=>{
                                        Alert.alert(
                                            'Yay!',
                                            response.message
                                        )
                                        this.props.navigation.navigate(
                                            'approveRentsPage'
                                        )
                                    }
                                )
                            } catch (err) {
                                this.setState(
                                    {
                                        loading:false
                                    },()=>Alert.alert(
                                        'Oops',
                                        err.message
                                    )
                                )
                            }
                        }
                    },
                    {
                        text:'No,cancel',
                        onPress:null
                    }
                ]
            )
        } catch (err) {
            this.setState(
                {
                    loading:false
                },()=>Alert.alert(
                    'Oops',
                    err.message
                )
            )
        }
    }


    onChange=(v)=>{
        try {
            this.setState(
                {
                    date:v
                }
            )
        } catch (err) {}
    };



    render() {
        return (
            <>
                <Header name="Choose Duration" back={this.props.navigation.goBack} />
                <View style={styles.container}>
                    <Text style={styles.label}
                        >Choose Start Date</Text>
                        <TouchableOpacity
                            placeholder="Start Date"
                            keyboardType='number-pad'
                            onPress={()=>this.setState(prev=>(
                                {
                                    showStart:!prev.showStart
                                }
                            ))}
                            style={styles.dateInput}
                        >
                            <Text style={{ fontSize: 18, color: '#000' }}>
                                {
                                    new Date(this.state.startDate).toDateString()
                                }
                            </Text>
                        </TouchableOpacity>
                        {this.state.showStart && <DatePickerComponent 
                                value={this.state.startDate}
                                onDateChange={(v)=>this.setState({startDate:v})} 
                                close={()=>this.setState({showStart:false})}
                            />
                        }
                        <Text style={styles.label}>Choose End Date</Text>
                        <TouchableOpacity
                            placeholder="End Date"
                            keyboardType='number-pad'
                            onPress={()=>this.setState(prev=>(
                                {
                                    showEnd:!prev.showEnd
                                }
                            ))}
                            style={styles.dateInput}
                        >
                            <Text
                                style={{ fontSize: 18, color: '#000' }}
                            >
                                {
                                    new Date(this.state.endDate).toDateString()
                                }
                            </Text>
                        </TouchableOpacity>
                        {this.state.showEnd && (
                            <DatePickerComponent
                                value={this.state.endDate}
                                close={()=>this.setState({showEnd:false})}
                                onDateChange={(v)=>this.setState({endDate:v})}
                            />
                        )}
                    <Button
                        onPress={this.sendRequest.bind(this)}
                        loading={this.state.loading}
                        buttonStyle={globalStyles.button}
                        title="CONTINUE"
                    />
                </View>
            </>
        )
    };


}



const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        width: "100%",
        alignItems: 'center',
        marginTop: 25

    },
    horizontal: {
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 30,
        marginBottom: 23
    },
    dateInput: {
        borderRadius: 5,
        borderWidth: 2,
        borderColor: '#959595',
        padding: 10,
        width: '80%',
        height: 60,
        justifyContent: 'center',
        marginVertical: 5
    },
    label:{
        marginHorizontal:'10%',
        alignSelf:'flex-start',
        fontSize:20,
        marginVertical:15,
        color:'#6d6d6d'
    }
});