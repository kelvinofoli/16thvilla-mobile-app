import React, { Component } from 'react'
import { View, StyleSheet, ScrollView } from 'react-native'
import { Text } from 'react-native-elements'
import colors from '../../../constants/colors';
import Header from '../../../components/header';
import moment from "moment";
import IconButton from "../../../components/iconButton";
import { AuthConsumer } from '../../../store/auth.state';

export default class PaymentDetails extends Component {

    state={
    }

    componentDidMount(){
        this.setState(
            { 
                ...this.props.route.params 
            }
        )
    }

    getName=()=>{
        try {
            const {purpose,tenantId,inspectionId}=this.state;
            switch (purpose) {
                case 'INSPECTION':
                    return inspectionId?.name;
                default:
                    return `${tenantId?.firstname} ${tenantId?.lastname}`;
            }
        } catch (err) {
            return '';    
        }
    }

    render() {
        return (<>
            <Header name="Payment" back={()=>this.props.navigation.goBack()} />
            <View style={{flexDirection:'row-reverse'}}>
                <AuthConsumer>
                    {
                        ({state})=>state.role==='landlord' && (
                            <IconButton 
                                onPress={()=>{
                                    this.props.navigation.navigate(
                                        'UpdatesPage',
                                        {
                                            data:this.state,
                                            keys:'payments'
                                        }
                                    )
                                }}
                            />
                        )
                    }
                </AuthConsumer>
            </View>
            <View style={styles.container}>
                    <Text h3 style={{marginVertical:10}}>{this.getName()}</Text>
                <View style={styles.horizontal}>
                    <Text h5 style={styles.item}>GHS{this.state.amount}</Text>
                    <Text style={{ 
                        color: colors.theme, 
                        fontSize: 11,
                        fontWeight:'bold',
                        marginBottom:4 }}
                    >{moment(
                        this.state.createdAt
                    ).fromNow()}</Text>
                </View>
                <Text style={{color:'grey'}}>PAYMENT TYPE: {this.state.paymentType}</Text>
                <Text style={{color:'grey'}}>DATE: {new Date(this.state.createdAt).toDateString()}</Text>
                {this.state.period && <Text style={{color:'grey'}}>PERIOD: {this.state.period}</Text>}
                {this.state.paymentType==='MOMO' && <>
                    <Text style={{color:'grey',paddingVertical:5}}>NUMBER: {this.state.accountNumber}</Text>
                    <Text style={{color:'grey',paddingVertical:5}}>PROVIDER: {this.state.accountIssuer}</Text>
                </>}

                <View style={{ 
                    paddingHorizontal:6, 
                    flexDirection:'row',
                    alignItems:'center',
                    justifyContent:'space-between',
                    backgroundColor: 'gold', 
                    borderRadius:5,
                    paddingVertical:5,
                    marginTop:20
                }}
                >
                    <Text style={{ color: 'white', fontWeight: "bold" }}>STATUS:  <Text style={{ fontWeight: "bold" }}>
                        {this.state.status}</Text></Text>
                <Text h5 style={styles.item}>{this.state.purpose}</Text>

                </View>
            </View>

            <Text style={{
                borderBottomWidth:1, 
                fontSize: 8, 
                marginHorizontal: '10%',
                marginTop: 20, 
                color: colors.theme, 
                borderColor:'#ddd',
                paddingVertical:10
            }}>REASON</Text>
            <Text style={{
                marginHorizontal: '12%',
                marginTop: 20, fontSize: 16, color:'#000'
            }}>{
                this.state.description
            }</Text>
        </>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        marginHorizontal: '10%',
        // borderTopWidth: 2,
        paddingTop: 30,
        justifyContent: 'space-around'
    },
    horizontal: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    rentItem: {
        width: '100%',
        borderBottomWidth: 3,
        borderColor: '#ddd',
        paddingHorizontal: "10%",
        paddingVertical: 10
    },
    item:{
        paddingVertical:5
    }
})
