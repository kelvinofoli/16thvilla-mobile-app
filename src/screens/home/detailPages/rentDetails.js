import React, { Component } from 'react'
import { View, StyleSheet,ScrollView,TouchableOpacity } from 'react-native'
import { Text, Button } from 'react-native-elements'
import globalStyles from '../../../constants/styles';
import colors from '../../../constants/colors';
import Header from '../../../components/header'
import IconFA from 'react-native-vector-icons/FontAwesome';
import moment from "moment";
import apiService from '../../../services/api.service';
import { AuthConsumer } from '../../../store/auth.state';


export default class RentDetails extends Component {

    viewLoaded=false;

    state={
        loading:false,
        lastPayment:null
    }


    componentDidMount(){
        this.viewLoaded=true;
        const data=this.props.route.params[
            'data'
        ];
        this.setState(
            {
                ...data
            },this.getLastPaid.bind(this)
        )
    }

    componentWillUnmount(){
        this.viewLoaded=false;
    }


    getLastPaid=async()=>{
        try {
            const {_id}=this.state;
            const response=await apiService.rentLastPaid(
                _id
            );
            if(!response.success){
                throw Error(
                    response.message
                )
            }
            this.viewLoaded && this.setState(
                {
                    lastPayment:response[
                        'data'
                    ]
                }
            )
        } catch (err) {
            this.getLastPaid().catch(err=>null);
        }
    }

    render() {

        const fomateDate=(date)=>{
            return moment(
                new Date(date)
            ).format(
                'DD-MM-YYYY'
            )
        }

        return (<>
            <Header name="Rent details" back={this.props.navigation.goBack} />
            <View style={{flexDirection:'row-reverse'}}>
                <AuthConsumer>
                    {
                        ({state})=>state.role==='landlord' && (
                            <TouchableOpacity
                                onPress={()=>{
                                    this.props.navigation.navigate(
                                        'UpdatesPage',
                                        {
                                            data:this.state,
                                            keys:'rents'
                                        }
                                    )
                                }}
                                style={{
                                    width:50,
                                    height:50,
                                    marginRight:20
                                }}
                            >
                                <IconFA 
                                    name="pencil"
                                    size={30}
                                    color={colors.theme}
                                />
                            </TouchableOpacity>
                        )
                    }
                </AuthConsumer>
            </View>
            <ScrollView contentContainerStyle={styles.container}>
                <View style={styles.rentItem}>
                    <Text h4 style={{color:colors.theme}}>
                        {
                            this.state.room?.type
                        }
                    </Text>
                    <Text style={{
                        marginTop:5,
                        marginBottom:5,
                    }}>DURATION: {this.state.duration} MONTH</Text>
                    <Text style={{
                        marginTop:2,
                        marginBottom:5,
                    }}>PER MONTH: {this.state.currency || 'GHS'}{this.state.costPerMonth}</Text>
                    <View style={{ 
                        paddingHorizontal:6,
                        paddingVertical:5, 
                        flexDirection:'row', 
                        justifyContent:'space-between', 
                        alignSelf: 'flex-start', 
                        backgroundColor: 'gold', 
                        borderRadius:5,
                        marginTop:5 ,
                        width:'100%',
                    }}
                    >
                        <Text style={{ color: '#000' }}>
                             STATUS: {this.state.status}
                        </Text>
                        <Text style={{ color: '#000'}}>
                             TYPE: {this.state.rentType}
                        </Text>
                    </View>
                </View>
                {this.state.status !== 'PENDING PAYMENT' && <>    
                    <View style={styles.timeline}>
                        <View style={[styles.time,{marginLeft:30}]}>
                            <Text style={styles.date}>
                                {
                                    fomateDate(
                                        this.state.startDate
                                    )
                                }
                            </Text>
                            <View style={styles.circle}></View>
                            <Text style={{fontSize:12}}>Start date</Text>
                        </View>
                        <View style={{ width: '100%', height: 2, backgroundColor: colors.theme, position: 'absolute',zIndex:-1,top:29,alignSelf:'center',paddingLeft:5}}>
                            <View></View>
                        </View>
                        <View style={[styles.time,{marginRight:30}]}>
                            <Text style={styles.date}>
                            {
                                    fomateDate(
                                        this.state.endDate
                                    )
                                }
                            </Text>
                            <View style={styles.circle}></View>
                            <Text style={{fontSize:12}}>End date</Text>
                        </View>
                    </View>

                    {this.state.lastPayment && <View style={styles.rentItem}>
                        <Text style={{fontSize:19,marginVertical:10}}>AMOUNT PAID: GHS{ this.state.lastPayment?.amount }</Text>
                        <Text style={{color:colors.theme}}>Last date : {new Date(this.state.lastPayment?.createdAt).toDateString()}</Text>
                        <Text style={{color:colors.theme}}>Status: {this.state.lastPayment?.status}</Text>
                        <Text style={{color:colors.theme}}>cost/month: GHS{this.state.lastPayment?.rentId?.costPerMonth}</Text>
                        <Text style={{color:colors.theme}}>Payment Type: {this.state.lastPayment?.paymentType}</Text>
                    </View>}


                    {this.state.status==='REVOKED' && <View style={styles.rentItem}>
                        <Text style={{fontSize:19,fontWeight:"bold"}}>BALANCE REMAINING: GHS{this.state.status.balanceRemaing || 0}</Text>
                        <Text style={{color:colors.theme}}>DURATION LEFT: {this.state.durationRemaining} MONTHS</Text>
                        <Text style={{color:colors.theme}}>REASON:  {this.state.revokedReason}</Text>
                        <Text style={{color:colors.theme}}>DATE MOVED: {new Date(this.state.dateMoved).toDateString()}</Text>
                    </View>}
                    <Button 
                        buttonStyle={globalStyles.button} 
                        title="VIEW AGREEMENT"
                        onPress={()=>this.props.navigation.navigate('Agreement',this.state)}
                    />
                </>}
            </ScrollView>
        </>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        alignItems:'center',
    },
    date:{fontSize:12,backgroundColor:'pink',borderRadius:12,paddingHorizontal:5,paddingVertical:2,color:"white",fontWeight:'bold',marginBottom:3},
    circle: { width: 14, height: 14, borderWidth: 2, borderColor: 'gold', backgroundColor: colors.theme, borderRadius: 10 },
    time: {
        alignItems: 'center'
    },
    timeline: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: '100%',
        marginVertical:20
    },
    horizontal: {
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 25,
        marginBottom: 23
    },
    rentItem: {
        width: '100%',
        // borderBottomWidth: 3,
        borderColor: '#ddd',
        paddingHorizontal: "10%",
        paddingVertical: 20,
    }
})
