import React, { Component } from 'react'
import { View, StyleSheet, TouchableOpacity, ScrollView, Image,ActivityIndicator } from 'react-native'
import { Text, Badge } from 'react-native-elements'
import Header from '../../../components/header'
import apiService from '../../../services/api.service';
import { Alert } from 'react-native';
import IconFA from 'react-native-vector-icons/FontAwesome';
import colors from '../../../constants/colors';



export default class tenantDetails extends Component {

    state={
        loading:false
    }

    componentDidMount(){
        this.setState(
            { 
                ...this.props.route.params 
            }
        )
    }


    block=async()=>{
        try {
            this.setState(
                {
                    loading:true
                }
            )
            const{
                _id,
                blocked
            }=this.state;
            const response=await apiService.updateTenant(
                {
                    id:_id,
                    data:{
                        blocked:!blocked
                    }
                }
            );
            if(!response.success){
                throw Error(
                    response.message
                ) 
            };
            let message=response.data.blocked?'Account blocked successfully':'Account activated successfully';
            return this.setState(
                {
                    loading:false,
                    blocked:response.data.blocked
                },()=>{
                    Alert.alert(
                        'TENANT STATUS',
                        message
                    );
                }
            )
        } catch (err) {
            Alert.alert(
                'TENANT STATUS',
                err.message
            )
            this.setState(
                {
                    loading:false
                }
            )
        }
    }



    render() {
        return (<>
            <Header 
                name="Tenant details" 
                back={this.props.navigation.goBack} 
            />
            <ScrollView contentContainerStyle={styles.container}>
                <View style={styles.actions}>
                    {this.state.loading && <ActivityIndicator color="#000"/>}
                    <TouchableOpacity 
                        onPress={this.block.bind(this)}
                        style={{
                            paddingHorizontal: 10, 
                            paddingVertical: 10, 
                            borderRadius: 6,
                            backgroundColor:'red'
                        }}
                    >
                        <Text style={{color:'#fff'}}>{this.state.blocked?'BLOCKED':'ACTIVE'}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity 
                        onPress={()=>{
                            this.props.navigation.navigate(
                                'RemoveTenant',
                                {
                                    tenant:this.state['_id']
                                }
                            )
                        }} 
                        style={{
                            paddingHorizontal: 10, 
                            paddingVertical: 10, 
                            borderRadius: 6,
                            backgroundColor:'blue',
                            marginLeft:10
                        }}
                    >
                        <Text style={{color:'#fff'}}>REVOKE</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={()=>{
                            this.props.navigation.navigate(
                                'UpdatesPage',
                                {
                                    data:this.state,
                                    keys:'tenants'
                                }
                            )
                        }}
                        style={{
                            width:50,
                            height:50,
                            margin:50,
                        }}
                    >
                        <IconFA 
                            name="pencil"
                            size={35}
                            color={colors.theme}
                            style={{marginTop:6}}
                        />
                </TouchableOpacity>
                </View>
                <View style={styles.rentItem}>
                    {/* <Image 
                        style={styles.profilePicture} 
                        source={{uri:this.state.photo}} /> */}

                    <Text h3>{this.state.fullname}</Text>
                    <Text style={styles.item}>AGE: {this.state.age}</Text>
                    <Text style={styles.item}>GENDER: {this.state.gender}</Text>
                    <Text style={styles.item}>MARRIED: {this.state.married?'YES':'NO'}</Text>
                    <Text style={styles.item}>OCCUPATION: {this.state.occupation}</Text>
                    <Text style={styles.item}>PHONE NUMBER: {this.state.phone}</Text>
                    <Text style={styles.item}>DATE OF BIRTH: {this.state.dateOfBirth}</Text>
                    <Text style={styles.item}>NUMBER OF CHILDREN: {this.state.numberOfChildren}</Text>
                    <Text style={styles.item}>OFFICE PHONE: {this.state.officeNumber}</Text>
                    <Text style={styles.item}>CURRENT ADDRESS: {this.state.currentAddress}</Text>
                    <Text style={styles.item}>POSTAL ADDRESS: {this.state.postalAddress}</Text>
                    <Text style={styles.item}>SMOKES: {this.state.smoke}</Text>
                    <Text style={styles.item}>HAVE PETS: {this.state.havePets}</Text>
                    <Text style={styles.item}>EMAIL: {this.state.email}</Text>

                    <View style={styles.divider}></View>

                    <Text style={styles.item}>NAME OF BUSINESS CONTACT: {this.state.businessContactName}</Text>
                    <Text style={styles.item}>BUSSINESS LOCATION: {this.state.businessLocation}</Text>
                    <Text style={styles.item}>BUSINESS CONTACT NUMBER: {this.state.businessContactNumber}</Text>
                    <Text style={styles.item}>BUSINESS EMAIL: {this.state.businessEmail}</Text>
                    <Text style={styles.item}>YEARS EMPLOYED: {this.state.yearsEmployed}</Text>

                    <View style={styles.divider}></View>

                    <Text style={styles.item}>CONTACT PERSON NAME: {this.state.contactPersonName}</Text>
                    <Text style={styles.item}>CONTACT PERSON EMAIL: {this.state.contactPersonEmail}</Text>
                    <Text style={styles.item}>CONTACT PERSONN ADDRESS: {this.state.contactPersonAddress}</Text>
                    <Text style={styles.item}>CONTACT PERSON PHONE: {this.state.contactPersonPhone}</Text>
                    <Text style={styles.item}>CONTACT PERSON RELATIONSHIP: {this.state.contactPersonRelationship}</Text>

                    <View style={styles.divider}></View>


                    <Text style={styles.item}>ID TYPE: {this.state.idType}</Text>
                    <Text style={styles.item}>ID NUMBER: {this.state.idNumber}</Text>
                    <View style={{ paddingHorizontal:6, marginTop:10, paddingVertical:5, display: 'flex', backgroundColor: 'gold', borderRadius:2 }}>
                        <Text style={{ color: 'white', fontWeight: "bold" }}>STATUS:  <Text style={{ fontWeight: "bold" }}><Text style={{color:'green', fontWeight: "bold"}}>{this.state.blocked?'BLOCKED':'ACTIVE'}</Text></Text></Text>
                    </View>
                </View>

            </ScrollView>
        </>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        paddingVertical:20
    },
    horizontal: {
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 25,
        marginBottom: 23
    },
    actions: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        paddingHorizontal: '10%',
        justifyContent: 'flex-start'
    },
    rentItem: {
        width: '100%',
        borderColor: '#ddd',
        paddingHorizontal: "10%",
        paddingVertical: 20,
        justifyContent: 'space-between',
        alignItems: 'flex-start'
    },
    profilePicture: {
        width: 120,
        height: 120,
        borderRadius: 60,
        alignSelf: 'center',
        margin: 10
    },
    divider:{
        marginVertical:15,
        borderBottomColor:'#ddd',
        borderBottomWidth:1,
        height:1,
        width:'100%'
    },
    item:{
        paddingVertical:5
    }
})
