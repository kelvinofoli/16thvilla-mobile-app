import React, { Component } from 'react'
import { View, StyleSheet,Dimensions,Alert,ScrollView } from 'react-native'
import { Text, Button } from 'react-native-elements';
import globalStyles from '../../constants/styles';
import Header from '../../components/header';
import DropDownPicker from 'react-native-dropdown-picker';
import { AuthConsumer } from '../../store/auth.state';
import apiService from '../../services/api.service';
import generateMonths from "../../utils/generateMonths";
const {width,height}=Dimensions.get('screen');
import moment from "moment";


const DROP_WIDTH=width * 0.94;
const DROP_HEIGHT=70;


export default class PayRentPage extends Component {

    state={
        rents:[],
        utils:[],
        rooms:[],
        durations:[],
        rent:null,
        price:0,
        duration:0,
        paymentType:'',
        rentType:'',
        purpose:'',
        amount:'0.00',
        utilityId:'',
        propertyId:'',
        period:'',
        months:[]
    }

    componentDidMount(){
        const periods=moment.months().map(m=>{
            const f=`${m} ${moment().format('YYYY')}`;
            return {
                label:f,
                value:f
            }
        })
        this.setState(
            {
                months:periods
            }
        )
    };


    getUtils=()=>apiService.getUtils().then(
        res=>{
            if(res.success){
                this.setState(
                    {
                        utils:res.data
                    }
                )
            }
        }
    ).catch(err=>this.getUtils());


    getRooms=()=>apiService.getRooms().then((res)=>{
        if(!res.success){
            throw Error(
                response[
                    'message'
                ]
            )
        }
        this.setState(
            {
                rooms:res.data
            }
        )
    }).catch(err=>this.getRooms());


    getRents=()=>apiService.getTenantRents(this.user._id).then((res)=>{
        if(!res.success){
            throw Error(
                response[
                    'message'
                ]
            )
        }
        this.setState(
            {
                rents:res.data
            }
        )
    }).catch(err=>this.getRents())


    componentWillUnmount(){
        this.getRents=null;
    }


    calculateRoomPrice=(duration)=>{
        const{
            price,
        }=this.state;
        const amount=price*duration;
        return this.setState(
            {
                duration:duration,
                amount:amount
            }
        )
    }

    getType=()=>{
        switch (this.state.rentType) {
            case 'NEW':
                return 'rent';
            default:
                return 'renew';
        }
    }

    getMonths=()=>moment.months();


    sendRequest=()=>{
        try {
            const {
                paymentType,
                rent,
                duration,
                amount,
                purpose,
                utilityId,
                propertyId,
                period,
                currency
            }=this.state;
            let data={};
            let message='';
            let page='';
            switch (purpose) {
                case 'RENT':
                    page='RENT|PAY';
                    message=`You have requested to pay an amount of ${currency}${amount} to ${this.getType()} this property, your landlord will approve your request when you successfully make payment.`;
                    data={
                        rent:rent,
                        duration:duration,
                        amount:amount,
                        paymentType:paymentType
                    }
                    break;
                case 'SD':
                    page='RENT|SD';
                    message=`You have requested to pay a security deposit of ${currency}${amount} for this property, confirm your request.`;
                    data={
                        propertyId:propertyId,
                        amount:amount,
                        paymentType:paymentType
                    }
                    break;
                default:
                    if(period===''){
                        throw Error(
                            'Please choose month you are making payment for'
                        )
                    }
                    page='RENT|UTILITY';
                    message=`You have requested to pay an amount of GHS${amount} for Utility`;
                    data={
                        utilityId:utilityId,
                        tenantId:this.user['_id'],
                        amount:amount,
                        paymentType:'MOMO',
                        period:period
                    }
                    break;
            }
            if(data.amount<=0){
                return;
            }
            return Alert.alert(
                'Dear Tenant',message,
                [
                    {
                        text:'Yes,continue',
                        onPress:async()=>{
                            try {
                                switch (data['paymentType']) {
                                    case 'MOMO':
                                        return this.props.navigation.navigate(
                                            'paymentPage',
                                            {
                                                data:data,
                                                page:page,
                                                currency
                                            }
                                        )
                                    default:
                                        let response={};
                                        this.setState(
                                            {
                                                loading:true
                                            }
                                        )
                                        switch (this.state.purpose) {
                                            case 'SD':
                                                response=await apiService.paySecurityDeposit(
                                                    data
                                                );
                                                break;
                                            default:
                                                response=await apiService.payRent(
                                                    data
                                                );
                                                break;
                                        }
                                        if(!response.success){
                                            throw Error(
                                                response.message
                                            )
                                        };
                                        return this.setState(
                                            {
                                                loading:false
                                            },()=>{
                                                return Alert.alert(
                                                    'Congratulations!',
                                                    response.message
                                                )
                                            }
                                        )
                                }
                            } catch (err) {
                                this.setState(
                                    {
                                        loading:false
                                    },()=>{
                                        Alert.alert(
                                            'Oops! something went wrong',
                                            err.message
                                            
                                        )
                                    }
                                )
                            }
                        }
                    },
                    {
                        text:'No,cancel',
                        onPress:null
                    }
                ]
            )
        } catch (err) {
            Alert.alert(
                'Dear Tenant',
                err[
                    'message'
                ]
            )
        }
    }

    render() {

        return (
            <>
                <AuthConsumer>
                    {
                        ({state})=>{
                            const{user,role}=state;
                            this.user=user;
                            this.role=role;
                        }
                    }
                </AuthConsumer>
                <Header name="Make Payment" back={this.props.navigation.goBack} />
                <ScrollView contentContainerStyle={styles.container} keyboardDismissMode="on-drag" keyboardShouldPersistTaps="handled">
                    <View style={{width:'100%',marginLeft:70,marginBottom:30}}>
                        {this.state.purpose!=='UTILITY' ? <Text style={{fontSize:70,fontWeight:'200'}}>{`${this.state.currency || 'GHS'}${this.state.amount}`}</Text>:<Text style={{fontSize:50}}>{`GHS${this.state.amount}`}</Text>}
                        <Text style={{color:'#000'}}>Total payment due</Text>
                    </View>
                    <View style={{ width: '80%', alignItems: 'center' }}>
                        <DropDownPicker
                            placeholder="What do you want to pay for"
                            zIndex={10}
                            items={[
                                {
                                    label:'Utility',
                                    value:'UTILITY'
                                },
                                {
                                    label:'Rent',
                                    value:'RENT'
                                },
                                {
                                    label:'Security Deposit',
                                    value:'SD'
                                }
                            ]}
                            itemStyle={{ justifyContent: 'flex-start' }}
                            style={{ 
                                borderColor: '#959595', 
                                borderWidth: 2, 
                                margin: 5, 
                                borderRadius: 5, 
                                width:DROP_WIDTH 
                            }}
                            containerStyle={{ height:DROP_HEIGHT }}
                            onChangeItem={item=>{
                                this.setState(
                                    {
                                        purpose:item.value
                                    },()=>{
                                        switch (this.state.purpose) {
                                            case 'RENT':
                                                this.getRents();
                                                break;
                                            case 'SD':
                                                this.getRooms();
                                                break;
                                            default:
                                                this.getUtils();
                                                break;
                                        }
                                    }
                                )
                            }}
                        />
                        {this.state.purpose==='UTILITY' && <>
                            <DropDownPicker
                                zIndex={9}
                                placeholder="Choose Period"
                                items={this.state.months}
                                itemStyle={{ justifyContent: 'flex-start' }}
                                style={{
                                    borderColor: '#959595', 
                                    borderWidth: 2, 
                                    margin: 5, 
                                    borderRadius: 5, 
                                    width:DROP_WIDTH 
                                }}
                                containerStyle={{ height:DROP_HEIGHT }}
                                onChangeItem={item=>{
                                    try {
                                        this.setState(
                                            {
                                                period:item[
                                                    'value'
                                                ]
                                            }
                                        )
                                    } catch (err) {}
                                }}
                            />
                            <DropDownPicker
                                zIndex={7}
                                placeholder="Choose payment type"
                                items={this.state.utils.map(
                                        util=>{
                                            return { 
                                                label:`${util.type} ~ GHS${util.amount}`, 
                                                value:JSON.stringify(util)
                                            }
                                        }
                                    )
                                }
                                itemStyle={{ justifyContent: 'flex-start' }}
                                style={{
                                    borderColor: '#959595', 
                                    borderWidth: 2, 
                                    margin: 5, 
                                    borderRadius: 5, 
                                    width:DROP_WIDTH
                                }}
                                containerStyle={{ height:DROP_HEIGHT }}
                                onChangeItem={item=>{
                                    try {
                                        const{_id,amount}=JSON.parse(item.value);
                                        this.setState(
                                            {
                                                utilityId:_id,
                                                amount:amount
                                            }
                                        )
                                    } catch (err) {}
                                }}
                            />
                        </>
                        }
                        {
                            this.state.purpose==='SD' && <>
                                <DropDownPicker
                                    placeholder="Choose Property"
                                    zIndex={6}
                                    items={this.state.rooms.map(
                                            room=>{
                                                return { 
                                                    label:`${room.type} ~ Room ${room.roomNumber}`, 
                                                    value:JSON.stringify(room)
                                                }
                                            }
                                        )
                                    }
                                    itemStyle={{ justifyContent: 'flex-start' }}
                                    style={{ 
                                        borderColor: '#959595', 
                                        borderWidth: 2, 
                                        margin: 5, 
                                        borderRadius: 5, 
                                        width:DROP_WIDTH 
                                    }}
                                    containerStyle={{ height:DROP_HEIGHT }}
                                    onChangeItem={item=>{
                                        try {
                                            const {
                                                _id,
                                                price,
                                                paymentType,
                                                currency
                                            }=JSON.parse(item.value);
                                            this.setState(
                                                { 
                                                    currency:currency,
                                                    propertyId:_id,
                                                    paymentType:paymentType,
                                                    amount:parseFloat(price)*3,
                                                    price:price
                                                }
                                            )
                                        } catch (err) {}
                                    }}
                                />
                            </>
                        }
                        {this.state.purpose==='RENT' && <>
                                <DropDownPicker
                                    placeholder="Choose rent"
                                    zIndex={6}
                                    items={this.state.rents.map(
                                            rent=>{
                                                return { 
                                                    label:`${rent.room?.type} ~ Room ${rent.room?.roomNumber}`, 
                                                    value:JSON.stringify(rent)
                                                }
                                            }
                                        )
                                    }
                                    itemStyle={{ justifyContent: 'flex-start' }}
                                    style={{ 
                                        borderColor: '#959595', 
                                        borderWidth: 2, 
                                        margin: 5, 
                                        borderRadius: 5, 
                                        width:DROP_WIDTH
                                    }}
                                    containerStyle={{ height:DROP_HEIGHT }}
                                    onChangeItem={item=>{
                                        try {
                                            const {
                                                _id,
                                                room,
                                                rentType,
                                                duration,
                                                paymentType,
                                                amount
                                            }=JSON.parse(item.value);
                                            this.setState(
                                                { 
                                                    currency:room.currency,
                                                    rent:_id,
                                                    price:room?.price || 0,
                                                    rentType,
                                                    duration,
                                                    paymentType,
                                                    amount
                                                }
                                            )
                                        } catch (err) {}
                                    }}
                                />
                                <DropDownPicker
                                    placeholder="Choose Type"
                                    defaultValue={this.state.rentType}
                                    disabled={true}
                                    zIndex={5}
                                    items={[
                                        {
                                            label:'New',
                                            value:'NEW'
                                        },
                                        {
                                            label:'Renewal',
                                            value:'RENEWAL'
                                        }
                                    ]}
                                    itemStyle={{ justifyContent: 'flex-start' }}
                                    style={{ 
                                        borderColor: '#959595', 
                                        borderWidth: 2, 
                                        margin: 5, 
                                        borderRadius: 5, 
                                        width:DROP_WIDTH 
                                    }}
                                    containerStyle={{ height:DROP_HEIGHT }}
                                    onChangeItem={item=>this.state.rentType=item.value}
                                />
                                <DropDownPicker
                                    placeholder="Choose duration"
                                    defaultValue={this.state.duration}
                                    zIndex={4}
                                    items={
                                        generateMonths(
                                            0,
                                            24
                                        )
                                    }
                                    itemStyle={{ justifyContent: 'flex-start' }}
                                    style={{ 
                                        borderColor: '#959595', 
                                        borderWidth: 2, 
                                        margin: 5, 
                                        borderRadius: 5, 
                                        width:DROP_WIDTH 
                                    }}
                                    containerStyle={{ height:DROP_HEIGHT }}
                                    onChangeItem={item=>this.calculateRoomPrice(item.value)}
                                />
                            </>
                        }
                        {(this.state.purpose==='SD' || this.state.purpose==='RENT') && (
                                <DropDownPicker
                                    zIndex={3}
                                    placeholder="Payment method"
                                    defaultValue={this.state.paymentType}
                                    items={[
                                        { label: 'Cash', value: 'CASH' },
                                        { label: 'Mobile Money', value: 'MOMO' },
                                        { label: 'Bank Deposit', value: 'BANK DEPOSIT' },
                                    ]}
                                    itemStyle={{ justifyContent: 'flex-start' }}
                                    style={{ 
                                        borderColor:'#959595', 
                                        borderWidth:2, 
                                        margin:5,
                                        borderRadius:5,
                                        width:DROP_WIDTH
                                    }}
                                    containerStyle={{ height:DROP_HEIGHT }}
                                    onChangeItem={item=>this.setState(
                                        { 
                                            paymentType:item.value
                                        }
                                    )}
                                />
                            )
                        }
                        <Button
                            loading={this.state.loading}
                            onPress={this.sendRequest.bind(this)}
                            buttonStyle={globalStyles.button} 
                            title="PAY" 
                        />
                    </View>
                </ScrollView>

            </>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        width: "100%",
        alignItems: 'center',
        marginVertical:50,
        height:height
    },
    horizontal: {
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 30,
        marginBottom: 23
    },
    dateInput: {
        borderRadius: 5,
        borderWidth: 2,
        borderColor: '#959595',
        padding: 10,
        width: '100%',
        height: 60,
        justifyContent: 'center',
        marginVertical: 5
    }
});
