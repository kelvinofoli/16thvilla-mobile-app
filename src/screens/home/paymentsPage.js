import React, { Component } from 'react'
import { View, StyleSheet, ScrollView, TouchableOpacity,ActivityIndicator,Platform,StatusBar } from 'react-native'
import { Text } from 'react-native-elements'
import colors from '../../constants/colors';
import Header from '../../components/header'
import { AuthConsumer } from '../../store/auth.state';
import apiService from '../../services/api.service';


export default class PaymentsPage extends Component {

    viewLoaded=false;
    state={
        loading:false,
        payments:[]
    }

    componentDidMount(){
        this.viewLoaded=true;
        this.getPayments();
    }


    componentWillUnmount(){
        this.getPayments=null;
        this.viewLoaded=false;
    }

    getPayments(){
        let service=null;
        switch (this.role) {
            case 'tenant':
                service=apiService.getTenantPayments;
                break;
            default:
                service=apiService.getAllPayments;
                break;
        }
        this.setState(
            {
                loading:true
            }
        )
        return service(
            this.user._id
        ).then((res) => {
            if(res.success){
                return this.viewLoaded && this.setState(
                    {
                        loading:false,
                        payments:res.data
                    }
                )
            }
        },err=>{
            this.getPayments();
        })
    }


    render() {
        return (<>
                {Platform.OS==='ios' && <StatusBar barStyle="dark-content"/>}
            <AuthConsumer>
                {
                    ({state})=>{
                        this.user=state['user'];
                        this.role=state['role'];
                    }
                }
            </AuthConsumer>
            <Header name="Payments" back={this.props.navigation.goBack} />
            <ScrollView style={styles.container}>
            {this.state.loading && <ActivityIndicator size={20} color="#6d6d6d"/>}
                {this.state.payments.map((el, id) =>
                    <TouchableOpacity 
                        key={id} style={styles.rentItem}
                        onPress={()=>this.props.navigation.navigate('paymentDetails',el)}
                    >
                        {(el.purpose==='RENT' || el.purpose === 'UTILITY') && <Text h3>{el.tenantId?.firstname} {el.tenantId?.lastname}</Text>}
                        {el.purpose==='INSPECTION' && <Text h3>{el.inspectionId?.name}</Text>}
                        <View style={styles.horizontal}>
                            {
                                el.purpose==='RENT'?(
                                    <Text h4>{el.property}</Text>
                                ):(
                                    <Text h4>{el.purpose}</Text>
                                )
                            }
                        </View>
                        <View style={styles.horizontal}>
                            <Text h5>AMOUNT: GHS{el.amount}</Text>
                            <Text style={{ 
                                color: colors.theme, 
                                fontWeight: 'bold', 
                                alignSelf: 'flex-end', 
                                fontSize: 11, 
                                marginBottom: 3 
                                }}
                            >{el.date}</Text>
                        </View>
                        <View style={styles.horizontal}>
                            <Text h5>DURATION: {el.rentId?.duration || el.period} {el.rentId?.duration && <Text style={{color:'#aaa'}}>Months</Text>}</Text>
                        </View>
                        <View style={{
                            marginTop:7,
                            paddingHorizontal:6,
                            paddingVertical:5, 
                            display: 'flex', 
                            alignSelf: 'flex-start', 
                            backgroundColor: 'gold', 
                            borderRadius:2 }}
                        >
                            <Text style={{ 
                                    color: 'white'
                            }}
                            >STATUS: <Text style={{ color: el.status == 'PAID' ? 'green' : 'red', fontWeight: "bold" }}>{el.status}</Text></Text>
                        </View>
                    </TouchableOpacity>
                )}
            </ScrollView>
        </>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 20
    },
    horizontal: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    rentItem: {
        width: '100%',
        borderBottomWidth: 2,
        borderColor: '#ddd',
        paddingHorizontal: "10%",
        paddingVertical: 10
    }
})
