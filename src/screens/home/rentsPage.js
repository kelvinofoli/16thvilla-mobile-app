import React, { Component } from 'react'
import { View, StyleSheet, TouchableOpacity, ScrollView,ActivityIndicator,StatusBar } from 'react-native'
import Header from '../../components/header';
import apiService from '../../services/api.service';
import { AuthConsumer } from '../../store/auth.state';
import RentListItem from "../../components/rentList.component";


export default class RentsPage extends Component {

    isLoaded=false;

    state={
        loading:false,
        rents:[]
    }

    componentDidMount(){
        this.isLoaded=true;
        this.getRents();
    }


    getRents() {
        let service=null;
        switch (this.role) {
            case 'tenant':
                service=apiService.getTenantRents;
                break;
            default:
                service=apiService.getAllRents;
                break;
        }
        this.setState(
            {
                loading:true
            }
        )
        return service(
            this.user._id
        ).then((res) => {
            if(res.success){
                return this.setState(
                    {
                        loading:false,
                        rents:res.data
                    }
                )
            }
        },err=>this.isLoaded && this.getRents());
    }


    render() {
        return (
            <>
                <StatusBar  barStyle="dark-content"/>
                <AuthConsumer>
                    {
                        ({state})=>{
                            const{user,role}=state;
                            this.user=user;
                            this.role=role;
                        }
                    }
                </AuthConsumer>
                <Header name="Rents" back={this.props.navigation.goBack} />
                <ScrollView style={styles.container}>
                    {this.state.loading && <ActivityIndicator size={20} color="#6d6d6d"/>}
                    { this.state.rents.map((el,id)=>{
                        return (
                            <RentListItem
                                key={id}
                                el={el}
                                {...this.props}
                                onPress={()=>{
                                    this.props.navigation.navigate(
                                        'rentDetails',
                                        {
                                            data:el
                                        }
                                    )
                                }}
                            />
                        )
                    }) }
                </ScrollView>
            </>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 20
    },
    horizontal: {
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 25,
        marginBottom: 23
    },
    rentItem: {
        width: '100%',
        borderBottomWidth: 2,
        borderColor: '#e5e5e5',
        paddingHorizontal: "10%",
        paddingVertical: 10
    }
})
