import React, { Component } from 'react'
import { View, StyleSheet, TouchableOpacity, ScrollView, Image } from 'react-native'
import { Text, Badge, Button } from 'react-native-elements'
import NTextInput from '../../components/textInput'
import globalStyles from '../../constants/styles';
import Header from '../../components/header'

export default class RevokePage extends Component {
    render() {
        return (
            <>
                <Header name="Revoke" back={this.props.navigation.goBack} />

                <View style={styles.container}>
                    <View style={{ width: '80%', alignItems: 'center' }}>
                        <NTextInput placeholder="CHOOSE RENT" />

                        <Text h4>Chamber and hall SC</Text>
                        <Text>DURATION: 1 MONTH</Text>
                        <View style={{ paddingHorizontal: 6, backgroundColor: 'gold', borderRadius: 5 }}>
                            <Text style={{ color: 'white', fontWeight: "bold" }}>Status: ACTIVE</Text>
                        </View>
                    </View>
                    <Button containerStyle={{ position: 'absolute', bottom: 0 }} buttonStyle={globalStyles.button} title="REVOKE" />
                </View>

            </>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        width: "100%",
        alignItems: 'center',
        marginTop: 20
    },
    horizontal: {
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 30,
        marginBottom: 23
    }
});
