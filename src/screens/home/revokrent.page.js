import React, { Component } from 'react'
import { View, StyleSheet, ScrollView, TouchableOpacity,Dimensions,Alert } from 'react-native'
import NTextInput from '../../components/textInput'
import { Text, Button } from 'react-native-elements'
import IconFA from 'react-native-vector-icons/FontAwesome';
import globalStyles from '../../constants/styles';
import Header from '../../components/header';
import DropDownPicker from 'react-native-dropdown-picker';
import { AuthConsumer } from '../../store/auth.state';
import apiService from '../../services/api.service';
import generateMonths from "../../utils/generateMonths";

const {
    width
}=Dimensions.get('screen');


export default class RevokeRentPage extends Component {

    viewLoaded=false;
    state={
        rents:[],
        reason:'',
        rentId:'',
        tenantId:null
    }

    componentDidMount(){
        try {
            this.viewLoaded=true;
            this.setState(
                {
                    tenantId:this.props.route.params[
                        'tenant'
                    ]
                },()=>this.getRents()
            )
        } catch (err) {
        }
    }


    getRents() {
        return apiService.getTenantRents(
            this.state.tenantId
        ).then((res) => {
            if(res.success){
                return this.viewLoaded && this.setState(
                    {
                        rents:res.data
                    }
                )
            };
        },err=>{
            this.getRents();
        })
    }


    componentWillUnmount(){
        this.viewLoaded=false;
        this.getRents=null;
    }



    sendRequest=()=>{
        const {
            reason,
            rentId,
            tenantId
        }=this.state;
        const data={
            tenantId:tenantId,
            rentId:rentId,
            reason:reason
        }
        if(reason==='' || !tenantId){
            return;
        }
        return Alert.alert(
            'REVOKE TENANT',
            `You have requested to remove Tenant from this property, do you confirm your request?`,
            [
                {
                    text:'Yes,continue',
                    onPress:async()=>{
                        try {
                            this.setState(
                                {
                                    loading:true
                                }
                            )
                            const response=await apiService.revokeTenant(data);
                            if(!response.success){
                                throw Error(
                                    response.message
                                )
                            };
                            this.setState(
                                {
                                    loading:false
                                },()=>{
                                    Alert.alert(
                                        'DONE',
                                        response.message
                                    )
                                    this.props.navigation.goBack();
                                }
                            )
                        } catch (err) {
                            Alert.alert(
                                'ERROR',
                                err.message
                            )
                            this.setState(
                                {
                                    loading:false
                                }
                            )
                        }
                    }
                },
                {
                    text:'No,cancel',
                    onPress:null
                }
            ]
        )
    }

    render() {
        return (
            <>
                <AuthConsumer>
                    {
                        ({state})=>{
                            const{user,role}=state;
                            this.user=user;
                            this.role=role;
                        }
                    }
                </AuthConsumer>
                <Header name="Pay Rent" back={this.props.navigation.goBack} />
                <View style={styles.container}>
                    <View style={{ width: '80%', alignItems: 'center' }}>
                        <DropDownPicker
                            placeholder="Choose rent"
                            items={this.state.rents.map(
                                    rent=>{
                                        return { 
                                            label:`${rent.room?.type} ~ ${rent.duration} months`, 
                                            value:`${rent._id}|${rent.costPerMonth}`
                                        }
                                    }
                                )
                            }
                            itemStyle={{ justifyContent: 'flex-start' }}
                            style={{ 
                                borderColor: '#959595', 
                                borderWidth: 2, 
                                margin: 5, 
                                borderRadius: 5, 
                                width: width * 0.8 
                            }}
                            containerStyle={{ height: 60 }}
                            onChangeItem={item=>{
                                const[
                                    _id
                                ]=item.value.split('|');
                                this.setState(
                                    { 
                                        rentId:_id,
                                    }
                                )
                            }}
                        />
                        <NTextInput
                            onChangeText={v=>this.state.reason=v} 
                            placeholder="Reason" 
                        />
                        <Button
                            loading={this.state.loading}
                            onPress={this.sendRequest.bind(this)}
                            buttonStyle={globalStyles.button} 
                            title="CONTINUE" 
                        />
                    </View>
                </View>

            </>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        width: "100%",
        alignItems: 'center',
        marginTop:50,
        flex: 1
    },
    horizontal: {
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 30,
        marginBottom: 23
    }
});
