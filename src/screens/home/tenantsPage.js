import React, { Component } from 'react'
import { View, StyleSheet, TouchableOpacity, ScrollView, Image,ActivityIndicator } from 'react-native'
import { Text } from 'react-native-elements'
import Header from '../../components/header'
import IconMA from 'react-native-vector-icons/MaterialIcons';
import colors from '../../constants/colors';
import apiService from '../../services/api.service';

export default class TenantsPage extends Component {

    viewloaded=false;

    state={
        loading:false,
        tenants:[]
    }

    componentDidMount(){
        this.viewloaded=true;
        this.getTenants();
    }


    componentWillUnmount(){
        this.viewloaded=false;
    }

    getTenants=async()=>{
        try {
            this.setState(
                {
                    loading:true
                }
            )
            const response=await apiService.getTenants();
            if(!response.success){
                throw Error(
                    response.message
                )
            }
            this.viewloaded && this.setState(
                {
                    tenants:response.data,
                    loading:false
                }
            )
        } catch (err) {
            this.getTenants().catch(err=>null);
        }
    }

    render() {
        return (<>
            <Header name="Tenants" back={this.props.navigation.goBack} />
            <ScrollView style={styles.container}>
                {this.state.loading && <ActivityIndicator size={20} color="#6d6d6d"/>}
                {this.state.tenants.map((el, id) =>
                    <TouchableOpacity 
                        key={id} 
                        style={[styles.rentItem, { borderTopWidth: id == 0 ? 3 : 0, }]} 
                        onPress={()=>this.props.navigation.navigate('tenantDetails',el)}>
                        {/* <View style={{ width: '27%' }}>
                            <Image style={styles.profilePicture} source={{uri:el.photo}} />
                        </View> */}
                        <View style={{ 
                            justifyContent: 'space-between', 
                            flexDirection: 'row', 
                            alignItems: 'center', 
                            width: '100%' 
                            }}
                        >
                            <View>
                                <Text h3 style={{fontSize:12}}>{el.firstname +" "+ el.lastname}</Text>
                                <Text h4 style={{color:'#6d6d6d'}}>Gender: {el.gender}</Text>
                                <Text h4 style={{color:'#6d6d6d'}}>Occupation: {el.occupation}</Text>
                                <Text h4 style={{color:'#6d6d6d'}}>Date joined: {new Date(el.createdAt).toDateString()}</Text>
                                <View style={{ paddingHorizontal: 6,paddingVertical:6,marginTop:5, display: 'flex', alignSelf: 'flex-start', backgroundColor: 'gold', borderRadius:2 }}>
                                    <Text style={{ color: 'white', fontWeight: "bold" }}>STATUS: {el.blocked?'BLOCKED':'ACTIVE'}</Text>
                                </View>
                            </View>
                            <IconMA name="arrow-right" size={25} color={'#ddd'} />
                        </View>
                    </TouchableOpacity>
                )}
            </ScrollView>
        </>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 10
    },
    horizontal: {
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 25,
        marginBottom: 23
    },
    rentItem: {
        width: '100%',
        borderBottomWidth:1,
        flexDirection: 'row',
        borderColor: '#ddd',
        paddingVertical: 10,
        padding:20
    },
    profilePicture: {
        width: 80,
        height: 80,
        borderRadius:2,
        margin: 10,
        borderColor:'#ddd',
        borderWidth:1
    }
})
