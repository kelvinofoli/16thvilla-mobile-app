import React, { Component } from 'react'
import { FlatList } from 'react-native'
import { View, StyleSheet, ScrollView, TouchableOpacity,ActivityIndicator,StatusBar,Platform } from 'react-native'
import { Text } from 'react-native-elements'
import Header from '../../components/header'
import apiService from '../../services/api.service'
import { AuthConsumer } from '../../store/auth.state'


export default class ViewReportsPage extends Component {

    isLoaded=false;

    state={
        loading:false,
        reports: []
    }
    
    componentDidMount=()=>this.getReports();

    getReports=async()=>{
        try {
            this.isLoaded=true;
            let service=null;
            switch (this.role) {
                case 'landlord':
                    service=apiService.getReports;
                    break;
                default:
                    service=apiService.getTenantReports;
                    break;
            }
            this.setState(
                {
                    loading:true
                }
            )
            const response=await service(this.user._id);
            if(!response.success){
                throw Error(
                    response.message
                )
            }
            this.setState(
                {
                    loading:false,
                    reports:response.data
                }
            )
        } catch (err) {
            this.isLoaded && this.getReports();
        }
    }

    
    componentWillUnmount(){
        this.isLoaded=false;
    }

    render() {
        return (
            <>
                {Platform.OS==='ios' && <StatusBar barStyle="dark-content"/>}
                <AuthConsumer>
                    {
                        ({state})=>{
                            this.role=state['role'];
                            this.user=state[
                                'user'
                            ]
                        }
                    }
                </AuthConsumer>
                <Header name="Reports" back={this.props.navigation.goBack} />
                {this.state.loading && <ActivityIndicator size={20} color="#6d6d6d"/>}
                <FlatList 
                    style={styles.container}
                    data={this.state.reports}
                    keyExtractor={(v,i)=>i.toString()}
                    renderItem={({item})=>{
                        return(
                            <View style={styles.rentItem}
                            >
                                <Text h4>{item.title}</Text>
                                <Text>{item.message}</Text>
                                <View style={{marginTop:10}}>
                                    <Text>{item.tenant?.fullname}</Text>
                                    <Text style={{ fontSize: 10, color: '#bf4203' }}>{item.room?.type}</Text>
                                    <Text style={{ fontSize: 10, color: '#bf4203' }}>{item.room?.location}</Text>
                                    <Text style={{ fontSize: 10, color: '#bf4203' }}>{new Date(item.createdAt).toDateString()}</Text>
                                </View>
                            </View>
                        )
                    }}
                />
            </>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 10,
        marginVertical:5
    },
    horizontal: {
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 25,
        marginBottom: 23
    },
    rentItem: {
        width: '100%',
        borderBottomWidth: 2,
        borderColor: '#ddd',
        paddingHorizontal: "10%",
        paddingVertical: 10,
        height: 'auto',
        justifyContent: 'space-between'
    }
})
