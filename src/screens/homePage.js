import React, { Component } from 'react'
import { StyleSheet, TouchableOpacity, Platform, ScrollView, View,Dimensions } from 'react-native';
import { Text } from 'react-native-elements'
import Icon from 'react-native-vector-icons/MaterialIcons';
import FIcon from "react-native-vector-icons/FontAwesome5";
import Header from '../components/header'
import { AuthConsumer } from '../store/auth.state';
import AsyncStorage from '@react-native-async-storage/async-storage';
const {height}=Dimensions.get('screen');


export const HomeItem=(props)=>{
    return (
        <TouchableOpacity
            style={styles.homeItem} 
            onPress={()=>{props.route && props.route()}
        }>
            <View style={styles.iconLabel}>
                <FIcon color='orange' style={{marginRight:10}} name={props.icon} size={20}/>
                <Text
                    style={[
                        styles.homeBtnText, { 
                        color:'#000'
                    }]}
                >
                    {props.title}
                </Text>
            </View>
            <Icon
                name="arrow-right" 
                size={20} 
                color="#000" 
            />
        </TouchableOpacity>
    )
}


let items=[
    { viewFor: 'both', title: 'ROOMS', route: "propertiesPage",icon:'home' },
    { viewFor: 'both', title: 'RENTS', route: "rentsPage",icon:'monument' },
    { viewFor: 'both', title: 'PAYMENTS', route: "paymentsPage",icon:'file-invoice' },
    { viewFor: 'landlord', title: 'INSPECTIONS', route: "InspectionsPage" },
    { viewFor: 'tenant', title: 'REPORT', route: "createReportPage",icon:'plus' },
    { viewFor: 'both', title: 'VIEW REPORTS', route: "viewReportsPage",icon:'clipboard-list' },
    { viewFor: 'landlord', title: 'ADD ROOMS', route: "addRoomPage",icon:"plus" },
    { viewFor: 'landlord', title: 'APPROVE RENTS', route: "approveRentsPage",icon:"thumbs-up" },
    { viewFor: 'landlord', title: 'TENANTS', route: "tenantsPage",icon:'people-pants-simple',icon:"user-group-crown" },
    { viewFor: 'landlord', title: 'NEW RENT', route: "adminNewRent",icon:"buildings" },
    { viewFor: 'tenant', title: 'MAKE PAYMENT', route: "payRentPage",icon:"cash-register" },
    // { viewFor: 'tenant', title: 'PAY RENT', route: "payChooseRent" },
    { viewFor: 'tenant', title: 'RENEW RENT', route: "RenewRent",icon:"recycle" },
    { viewFor: 'both', title: 'RENT LAWS', route: "rentlaws",icon:"gavel" },
    // { viewFor: 'landlord', title: 'UPDATES', route: "UpdatesPage" }
]


export class HomePage extends Component {

    state={};

    componentDidMount(){};

    render() {
        
        const getName=(user)=>{
            const{role,firstname,lastname}=user;
            switch (role) {
                case 'landlord':
                    return `Landlord`
                default:
                    return `${firstname} ${lastname}`;
            }
        }

        return (
            <ScrollView style={styles.container}>
                <AuthConsumer>
                    {
                        ({state,dispath})=>{
                            this.dispatch=dispath;
                            this.role=state['role'];
                            this.user=state[
                                'user'
                            ]
                            return (
                                <>
                                    <Header
                                        role={this.state.role && this.state.role.toUpperCase()}
                                        name={`Welcome ${getName(
                                            { ...state['user'],role:state['role']}
                                        )}`}
                                        back={null}
                                        color="#fff"
                                    />
                                    {items.map((item,i)=>{
                                        return (item.viewFor===state.role || item.viewFor==='both') && (
                                            <HomeItem
                                                icon={item.icon}
                                                title={item.title} 
                                                route={()=>this.props.navigation.navigate(item.route)} 
                                                key={i} 
                                            />
                                        )
                                    })}
                                </>
                            )
                        }
                    }
                </AuthConsumer>
                <HomeItem
                    icon="lock"
                    title={'LOGOUT'} 
                    route={()=>this.dispatch(
                        {
                            user:null,
                            token:null,
                            role:null
                        },()=>{
                            AsyncStorage.removeItem('CRT-user')
                                        .catch(err=>null);
                        }
                    )} 
                />
            </ScrollView>
        )
    }
}
const styles=StyleSheet.create({
    container: {
        height:height
    },
    horizontal: {
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 25,
        marginBottom: 23
    },
    homeItem: {
        width: '100%',
        paddingHorizontal: 40,
        paddingVertical: 20,
        borderBottomWidth: 3,
        borderColor: '#6d6d6d',
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderRadius: 30
    },
    homeBtn: {
        width: 100,
        height: 100,
        borderRadius: 15,
        backgroundColor: '#e894e3',
        justifyContent: 'center',
        alignItems: 'center'
    },
    homeBtnText: {
        fontSize: 18,
        fontWeight: "bold",
        color:'#000'
    },
    iconLabel:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'flex-start',
        paddingVertical:5
    }
});