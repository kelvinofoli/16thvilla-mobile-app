import React, { Component } from 'react'
import { View, StyleSheet, TouchableOpacity, Alert,ScrollView } from 'react-native'
import { Button, Text } from 'react-native-elements';
import NTextInput from '../components/textInput'
import globalStyles, { DROP_HEIGHT, DROP_WIDTH } from '../constants/styles';
import DatePicker from 'react-native-date-picker';
import moment from "moment";
import Icon from 'react-native-vector-icons/FontAwesome';



import Header from '../components/header'
import { KeyboardAvoidingView } from 'react-native';

class InspectProperty extends Component {
    state = {
        loading: false,
        "name":"",
        "phone":"",
        show:false,
        showTime:false,
        mode:'date',
        date:new Date(),
        room:{}
    }


    componentDidMount() {
        this.setState(
            { 
                room:this.props.route.params[
                    'data'
                ] 
            }
        )

    }

    continue() {
        if (this.state.name == '' ||
            this.state.phone.length !== 10){
            Alert.alert(
                'Oops!', 
                'please fill all the required fields properly'
            )
        } else {
            const {
                name,
                phone,
                room,
                date
            }=this.state;
            const data={
                name:name,
                phone:phone,
                amount:room['inspectionAmount'],
                room:room['_id'],
                date:date.toISOString(),
                time:moment(new Date(date)).format('h:mm A')
            }
            this.props.navigation.navigate('paymentPage',{
                data:data,
                page:'INSPECT'
            })
        }
    }

    onDateChange(v){
        try {
            this.setState(
                {
                    date:v
                }
            )
        } catch (err) {
        }
    }

    showMode=(currentMode)=>{
        this.setState({ show: true, mode: currentMode })
    };

    showDatepicker=()=>{
        this.showMode('date')
    };

    showTimepicker=()=>{
        this.setState(
            {
                showTime:true
            }
        )
    };


    render() {
        return (
            <>
                <Header name="Inspect property" back={this.props.navigation.goBack} />
                <ScrollView contentContainerStyle={styles.container} keyboardShouldPersistTaps="handled" keyboardDismissMode='on-drag'>
                    <View style={{ width: '80%', alignItems: 'center' }}>
                        <NTextInput
                            onChangeText={v=>this.state.name=v.trim()}
                            placeholder="Fullname"
                        />
                        <NTextInput 
                            maxLength={10} 
                            onChangeText={v=>this.state.phone=v.trim()} 
                            placeholder="Phone number" 
                            keyboardType='number-pad' 
                        />
                        {!this.state.show && <>
                            <TouchableOpacity
                                placeholder="Date"
                                keyboardType='number-pad'
                                onPress={this.showDatepicker}
                                style={styles.dateInput}
                            >
                                <Text style={{ fontSize: 18, color: '#000' }}>
                                    {
                                        moment(this.state.date).format(
                                            'DD-MM-YYYY'
                                        )
                                    }
                                </Text>
                                <Text style={{color:'orange',marginVertical:5}}>Date</Text>
                            </TouchableOpacity>
                        </>}
                        {this.state.show && <View style={{
                            marginVertical:15,
                            borderColor:'#6d6d6d',
                            borderWidth:1,
                            borderRadius:5,
                        }}>
                            <TouchableOpacity 
                                hitSlop={{top:10,bottom:10,left:10,right:10}}
                                onPress={()=>this.setState({show:false})}
                            style={{
                                margin:10,
                                width:50
                            }}>
                                <Icon name="close" size={20}/>
                            </TouchableOpacity>
                            <DatePicker
                                mode={'date'}
                                date={ this.state.date || new Date()}
                                onDateChange={this.onDateChange.bind(this)}
                            />
                        </View>}
                        {!this.state.showTime && <>
                        <TouchableOpacity
                            placeholder="Time"
                            keyboardType='number-pad'
                            onPress={this.showTimepicker}
                            style={styles.dateInput}
                        >
                            <Text
                                style={{ fontSize: 18, color: '#000' }}
                            >
                                {
                                    new Date(this.state.date).toLocaleTimeString(undefined,{
                                        hour:   '2-digit',
                                        minute: '2-digit',
                                    }).slice(0,5)
                                }
                            </Text>
                            <Text style={{color:'orange',marginVertical:5}}>Time</Text>
                        </TouchableOpacity>
                        </>}
                        {this.state.showTime && <View style={{
                            marginVertical:15,
                            borderColor:'#6d6d6d',
                            borderWidth:1,
                            borderRadius:5
                        }}>
                            <TouchableOpacity 
                                hitSlop={{top:10,bottom:10,left:10,right:10}}
                                onPress={()=>this.setState({showTime:false})}
                            style={{
                                margin:10,
                                width:50
                            }}>
                                <Icon name="close" size={20}/>
                            </TouchableOpacity>
                            <DatePicker
                                mode={'time'}
                                date={ this.state.date || new Date()}
                                onDateChange={this.onDateChange.bind(this)}
                            />
                        </View>}

                    </View>
                    <Button
                        onPress={this.continue.bind(this)} 
                        buttonStyle={globalStyles.button} 
                        title="CONTINUE" 
                    />
                </ScrollView>

            </>
        )
    }
}
const styles=StyleSheet.create({
    container: {
        alignItems:'center',
        width:"100%",
        alignItems:'center',
        marginTop:25
    },
    horizontal: {
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 30,
        marginBottom: 23
    },
    dateInput: {
        borderRadius: 5,
        borderWidth: 2,
        borderColor: '#959595',
        padding: 10,
        width:DROP_WIDTH,
        height:DROP_HEIGHT,
        justifyContent: 'center',
        marginVertical: 5,
        backgroundColor:'#fff'
    }
});

export default InspectProperty
