import React, { Component } from 'react'
import { View, StyleSheet, ScrollView, TouchableOpacity,ActivityIndicator,Linking } from 'react-native'
import { Text } from 'react-native-elements'
import IconFA from 'react-native-vector-icons/FontAwesome';
import colors from '../constants/colors';
import Header from '../components/header'
import { AuthConsumer } from '../store/auth.state';
import apiService from '../services/api.service';
import moment from 'moment';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Alert } from 'react-native';


export default class InspectionsPage extends Component {

    viewLoaded=false;
    state={
        loading:false,
        inspections:[]
    }

    componentDidMount(){
        this.viewLoaded=true;
        this.getInspections();
    }


    componentWillUnmount(){
        this.getInspections=null;
        this.viewLoaded=false;
    }

    getInspections(){
        return apiService.getInspections().then((res)=>{
            if(res.success){
                return this.viewLoaded && this.setState(
                    {
                        loading:false,
                        inspections:res.data
                    }
                )
            }
        },err=>{
            this.getPayments();
        })
    }


    render() {
        return (<>
            <AuthConsumer>
                {
                    ({state})=>{
                        this.user=state['user'];
                    }
                }
            </AuthConsumer>
            <Header name="Inspections" back={this.props.navigation.goBack} />
            <ScrollView style={styles.container}>
            {this.state.loading && <ActivityIndicator size={20} color="#6d6d6d"/>}
                {this.state.inspections.map((el, id) =>
                    <TouchableOpacity 
                        key={id} style={styles.rentItem}
                        onPress={
                            ()=>Alert.alert(
                                'CALL TENANT',
                                'Do you want to place a phone call to this customer?',
                                [
                                    {
                                        text:'Yes,call',
                                        onPress:()=>Linking.openURL(`tel://${el.phone}`).catch(
                                            err=>null
                                        )
                                    },
                                    {
                                        text:'No,cancel',
                                        onPress:null
                                    }
                                ]
                            )
                        }>
                        <Text h3>{el.name}</Text>
                        <View style={styles.horizontal}>
                            <Text h4>{el.phone}</Text>
                            <Icon 
                                name="phone-square"
                                color="green"
                                size={20}
                            />
                        </View>
                        <View style={styles.horizontal}>
                            <Text h5>VISIT DATE: {new Date(el.date).toDateString()}</Text>
                            <Text style={{ 
                                color: colors.theme, 
                                fontWeight: 'bold', 
                                alignSelf: 'flex-end', 
                                fontSize: 11, 
                                marginBottom: 3 
                                }}
                            >{el.time}</Text>
                        </View>
                        <Text h5>PROPERTY: {el.room?.type}</Text>
                        <Text h5>LOCATION: {el.room?.location}</Text>
                        <Text h5>DATE REQUESTED: {moment(el.createdAt).fromNow()}</Text>
                        <View style={{
                            marginTop:7,
                            paddingHorizontal:6,
                            paddingVertical:5, 
                            display: 'flex', 
                            alignSelf: 'flex-start', 
                            backgroundColor: 'gold', 
                            borderRadius:2 }}
                        >
                            <Text style={{ 
                                    color: 'white'
                            }}
                            >STATUS: <Text style={{ color: el.status == 'PAID' ? 'green' : 'red', fontWeight: "bold" }}>{el.status}</Text></Text>
                        </View>
                    </TouchableOpacity>
                )}
            </ScrollView>
        </>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 20
    },
    horizontal: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    rentItem: {
        width: '100%',
        borderBottomWidth: 2,
        borderColor: '#ddd',
        paddingHorizontal: "10%",
        paddingVertical: 10
    }
})
