import React, { Component } from 'react';
import { View, Image, StyleSheet, Dimensions, StatusBar, ScrollView } from 'react-native';
import { Button, Text } from 'react-native-elements';
import colors from '../constants/colors';
const { height, width } = Dimensions.get('window');
import AsyncStorage from '@react-native-async-storage/async-storage';
import {AuthConsumer,AuthProvider} from '../store/auth.state';
import { DROP_HEIGHT } from '../constants/styles';




export default class LandingPage extends Component {

    static contextType=AuthProvider;

    state={
        loading: false
    }


    componentDidMount(){};


    getTenant=async()=>{
        this.setState(
            {
                loading: true
            }
        )
        AsyncStorage.getItem(
            'CRT-user'
        ).then(res=>{
            if (typeof res ==='string') {
                const data=JSON.parse(res);
                if (data.user && data.role) {
                    this.context.setHeaders('');
                    return this.context.dispath(
                        {
                            user:data['user'],
                            role:data['role']
                        }, () => {
                            this.props.navigation.navigate(
                                'homePage'
                            )
                        }
                    )
                }
            }
            this.props.navigation.navigate(
                'loginPage',
                {
                    role:'tenant'
                }
            )
        }).catch(err=>null);
    }


    render() {
        return (
            <ScrollView contentContainerStyle={styles.container}>
                <StatusBar backgroundColor="#000" barStyle="light-content" />
                <Image
                    style={styles.backgroundImage}
                    source={require('../assets/images/apartment-rent-concept-illustration.jpg')}
                />
                <Button
                    buttonStyle={[styles.button]}
                    title="Available Rooms"
                    titleStyle={{ color: '#fff',fontWeight:'200' }}
                    onPress={() => this.props.navigation.navigate('propertiesPage')}
                />
                <Button
                    onPress={
                        ()=>this.props.navigation.navigate(
                            'signupPage'
                        )}
                    buttonStyle={[styles.button]}
                    titleStyle={{ color: '#fff',fontWeight:'200' }}
                    title="signup"
                />
                <Button
                    onPress={this.getTenant.bind(this)}
                    buttonStyle={[styles.button]}
                    titleStyle={{ color: '#fff',fontWeight:'200' }}
                    title="login"
                />
                <Button
                    onPress={()=>this.props.navigation.navigate('ResetPassword')}
                    buttonStyle={[styles.button]}
                    titleStyle={{ color: '#fff',fontWeight:'200' }}
                    title="Reset Password"
                />
            </ScrollView>
        )
    }
}
const styles = StyleSheet.create({
    container:{
        flexDirection:'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    button: {
        backgroundColor:colors.black,
        marginBottom: 12,
        height:DROP_HEIGHT,
        width:200
    },
    btn: { position: 'absolute', bottom: 0, padding: 10, marginBottom: 10 },
    txt: {
        color: colors.theme,
        fontSize: 17,
        fontWeight: 'bold'
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    backgroundImage: {
        width: width,
        height: '100%',
        resizeMode: 'cover',
        position: 'absolute'
    }
});
