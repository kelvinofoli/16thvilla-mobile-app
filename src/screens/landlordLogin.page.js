import React, { Component } from 'react'
import { View, StyleSheet, StatusBar, Platform, Alert, TouchableOpacity ,Image} from 'react-native'
import { Button, Text } from 'react-native-elements';

import colors from '../constants/colors';
import globalStyles from '../constants/styles';
import NTextInput from '../components/textInput'
import Icon from 'react-native-vector-icons/FontAwesome';
import authService from "../services/auth.service";
import { AuthConsumer } from "../store/auth.state";
import AsyncStorage from '@react-native-async-storage/async-storage';
import { ScrollView } from 'react-native';
const logo=require("../assets/images/logo.png");

export default class loginLandlord extends Component {

    viewLoaded=false;
    state = {
        phone:'',
        password:'',
        loading:false,
        role:'tenant'
    };

    componentDidMount() {
        this.viewLoaded=true;
    }


    componentWillUnmount() {
        this.viewLoaded=false;
    }


    async login() {
        try {
            const { phone,password }=this.state;
            if (phone === '' || password === ''){
                throw Error(
                    'Please provide your phonenumber and password'
                )
            }
            this.setState({ loading:true });
            const response=await authService.landlordLogin(
                {
                    phone:phone, 
                    password:password 
                }
            );
            if (response===null) {
                throw Error(
                    `Please check your internet connection`
                )
            }
            if (!response.success) {
                throw Error(
                    response.message
                );
            }
            if (this.viewLoaded) {
                this.setHeaders(response._id);
                return this.dispath(
                    { 
                        user:response.data,
                        role:'landlord'
                    }
                )
            }
        } catch (err) {
            this.viewLoaded && this.setState(
                { 
                    loading: false 
                }
            );
            Alert.alert(
                'Oops!', 
                err.message
            )
        }
    }



    render() {
        return (
            <ScrollView contentContainerStyle={styles.container} keyboardShouldPersistTaps={'always'}>
                <AuthConsumer>
                    {
                        ({ dispath,setHeaders }) => {
                            this.dispath=dispath;
                            this.setHeaders=setHeaders;
                        }
                    }
                </AuthConsumer>
                <Image style={{height:200,width:250,top:50}} source={logo}/>
                <View style={{ width: '80%', alignItems: 'center', marginVertical:100}}>
                    <NTextInput
                    _style={{backgroundColor:'#FCFCFC'}}
                        keyboardType="phone-pad"
                        maxLength={10}
                        onChangeText={v => this.state.phone = v.trim()}
                        placeholder="Phone number" 
                    />
                    <NTextInput
                    _style={{backgroundColor:'#FCFCFC'}}
                        secureTextEntry={true}
                        onChangeText={v => this.state.password = v.trim()}
                        placeholder="Password" />
                    <Button
                        loading={this.state.loading}
                        onPress={this.login.bind(this)}
                        // onPress={() => this.props.navigation.navigate('homePage')}
                        buttonStyle={globalStyles.button}
                        title="ENTER" 
                    />
                </View>
            </ScrollView>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        height:'100%',
        flexDirection:'column',
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center'
    },
    button: {
        width: 200,
        backgroundColor: colors.theme,
        justifyContent: 'center',
        alignItems: 'center',
        height: 50,
        marginTop: 10
    },
    circle: {
        width: 60,
        height: 60,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: colors.theme,
        borderRadius: 30,
        marginBottom: 15
    }
});