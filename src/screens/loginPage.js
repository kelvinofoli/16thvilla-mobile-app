import React, { Component } from 'react'
import { View, StyleSheet, StatusBar, Platform, Alert, TouchableOpacity ,Image} from 'react-native'
import { Button, Text } from 'react-native-elements';

import colors from '../constants/colors';
import globalStyles from '../constants/styles';
import NTextInput from '../components/textInput'
import Icon from 'react-native-vector-icons/FontAwesome';
import authService from "../services/auth.service";
import { AuthConsumer } from "../store/auth.state";
import AsyncStorage from '@react-native-async-storage/async-storage';
import { ScrollView } from 'react-native';
import Header from '../components/header';
const logo=require("../assets/images/logo.png");

export default class LoginPage extends Component {

    viewLoaded=false;
    state = {
        phone:'',
        password:'',
        loading:false,
        role:'tenant'
    };

    componentDidMount() {
        this.viewLoaded=true;
    }


    componentWillUnmount() {
        this.viewLoaded=false;
    }


    async login() {
        try {

            const { phone,password,role }=this.state;
            if (phone === '' || password === ''){
                throw Error(
                    'Please provide your phonenumber and password'
                )
            }
            this.setState({ loading:true });
            const response=await authService.login(
                {
                    phone:phone, 
                    password:password 
                }
            );
            if (response === null) {
                throw Error(
                    `Please check your internet connection`
                )
            }
            if (!response.success) {
                throw Error(
                    response.message
                );
            }
            if (this.viewLoaded) {
                await AsyncStorage.setItem(
                    'CRT-user', 
                    JSON.stringify(
                        {
                            user:response['data'],
                            role:'tenant'
                        }
                    )
                );
                this.setHeaders(response._id);
                return this.dispath(
                    { 
                        user:response.data,
                        role:response['role']
                    }
                )
            }
        } catch (err) {
            this.viewLoaded && this.setState(
                { 
                    loading: false 
                }
            );
            Alert.alert(
                'Oops!', 
                err.message
            )
        }
    }



    render() {
        return (
            <View style={{backgroundColor:'#fff',flex:1}}>
            <Header name="Login" back={this.props.navigation.goBack} />
                <ScrollView contentContainerStyle={styles.container} keyboardShouldPersistTaps={'handled'} keyboardDismissMode="on-drag">
                    <AuthConsumer>
                        {
                            ({ dispath,setHeaders })=>{
                                this.dispath=dispath;
                                this.setHeaders=setHeaders;
                            }
                        }
                    </AuthConsumer>
                    <Image style={{height:200,width:250}} source={logo}/>
                    <View style={{ width: '80%', alignItems: 'center', marginVertical:20}}>
                        <NTextInput
                        _style={{backgroundColor:'#FCFCFC'}}
                            keyboardType="phone-pad"
                            maxLength={10}
                            onChangeText={v=>this.state.phone=v.trim()}
                            placeholder="Phone number" 
                        />
                        <NTextInput
                        _style={{backgroundColor:'#FCFCFC'}}
                            secureTextEntry={true}
                            onChangeText={v=>this.state.password=v.trim()}
                            placeholder="Password" />
                        <Button
                            loading={this.state.loading}
                            onPress={this.login.bind(this)}
                            buttonStyle={globalStyles.button}
                            title="ENTER" 
                        />
                        {/* <TouchableOpacity
                            onPress={()=>this.props.navigation.navigate('signupPage')}
                            style={{
                                margin:20,
                                flexDirection:'row',
                                alignItems:'center'                        
                            }}
                        >
                            <Text style={{marginHorizontal:5,fontSize:20}}>
                                New Tenant Signup
                            </Text>
                            <Icon size={15} name="chevron-right" color="#000"/>
                        </TouchableOpacity> */}
                    </View>
                </ScrollView>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flexDirection:'column',
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center'
    },
    button: {
        width: 200,
        backgroundColor: colors.theme,
        justifyContent: 'center',
        alignItems: 'center',
        height: 50,
        marginTop: 10
    },
    circle: {
        width: 60,
        height: 60,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: colors.theme,
        borderRadius: 30,
        marginBottom: 15
    }
});