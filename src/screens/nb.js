import React, { Component } from 'react'
import { Text, View,Alert } from 'react-native';
import CheckBox from '@react-native-community/checkbox';
import Header from '../components/header';
import api from "../services/api.service";
import { Button } from 'react-native-elements';
import globalStyles from '../constants/styles';



export default class Terms extends Component {

    state={
        checked:false,
        loading:false
    }

    componentDidMount(){
    }
    
    sendRequest=async()=>{
        let data=this.props.route.params;
            const penalty=data['costPerMonth']*3;
            const newAmount=data['amount']+penalty;
            data['amount']=newAmount;
            return Alert.alert(
                'Dear Tenant',
                `Per our agreement the new amount due for payment is GHS${data['amount']}, Do you still want to proceed?`,
                [
                    {
                        text:'Yes, I agree',
                        onPress:async()=>{
                            try {
                                switch (data['paymentType']) {
                                    case 'MOMO':
                                        return this.props.navigation.navigate(
                                            'paymentPage',
                                            {
                                                data:data,
                                                page:'RENT'
                                            }
                                        )
                                    default:
                                        this.setState(
                                            {
                                                loading:true
                                            }
                                        )
                                        const response=await api.newRent(
                                            data
                                        )
                                        if(!response.success){
                                            throw Error(
                                                response.message
                                            )
                                        };
                                        return this.setState(
                                            {
                                                loading:false
                                            },()=>{
                                                Alert.alert(
                                                    'yay!',
                                                    response.message
                                                )
                                                return this.props.navigation.navigate(
                                                    'propertiesPage',
                                                )
                                            }
                                        )
                                }
                            } catch (err) {
                                this.setState(
                                    {
                                        loading:false
                                    },()=>{
                                        Alert.alert(
                                            'Something went wrong',
                                            err.message
                                        )
                                    }
                                )
                            }
                        }
                    },
                    {
                        text:'No, Cancel',
                        onPress:()=>this.props.navigation.goBack()
                    }
                ]
            )
    }

    render() {
        return (
            <>
                <Header name="Terms and Conditions" back={this.props.navigation.goBack}/>
                <View style={{
                    padding:20
                }}>
                    <Text style={{fontSize:20}}>
                    The tenant shall pay a further advance rent of 3 months which will be held as security for any damage which may be caused to the property. Such money will be refunded to the tenant if he/she does not cause any damage to the property or repair all damages before leaving the rented premises.
                    </Text>
                    <View style={{margin:50,flexDirection:'row',alignItems:'center'}}>
                        <CheckBox
                            disabled={false}
                            value={this.state.checked}
                            onValueChange={(c) =>this.setState(
                                {
                                    checked:c
                                }
                            )}
                        />
                        <Text style={{color:'#6d6d6d'}}>Check the box to agree</Text>
                    </View>
                    {this.state.checked && <Button
                        onPress={this.sendRequest.bind(this)}
                        loading={this.state.loading}
                        buttonStyle={[globalStyles.button,{ marginTop:30 }]}
                        title="CONTINUE"
                    />}
                </View>
            </>
        )
    }
}
