import React, { Component } from 'react'
import { View, StyleSheet, TouchableOpacity, Alert } from 'react-native'
import { Button, Text } from 'react-native-elements';
import NTextInput from '../components/textInput'
import globalStyles from '../constants/styles';
import authService from "../services/auth.service";
import apiService from "../services/api.service";
import Header from '../components/header';


export default class OTPPage extends Component {

    viewLoaded=false;
    state={
        loading:false,
        otp:'',
        data:{},
        number:'',
        page:''
    }

    componentDidMount() {
        this.viewLoaded=true;
        const {
            data,
            page
        }=this.props.route.params;
        this.setState(
            {
                data:data,
                page:page
            },()=>{
                switch (page) {
                    case 'RENEW':
                    case 'INSPECT':
                    case 'RENT':
                    case 'RENT|PAY':
                    case 'RENT|SD':
                    case 'RENT|UTILITY':
                        this.state.number=data[
                            'accountNumber'
                        ];
                        break;
                    case 'SIGNUP':
                        this.state.number=data[
                            'phone'
                        ];
                        break;
                    default:
                        break;
                };
                this.sendOtp(true);
            }
        )
    }

    async sendOtp(resend) {
        try {
            if (resend && this.viewLoaded) {
                this.setState(
                    {
                        loading:true
                    }
                )
            }
            const response=await apiService.sendOtp(
                {
                    phoneNumber:this.state.number 
                }
            );
            if (!response.success) {
                throw Error(
                    response.message
                );
            }
            if (resend && this.viewLoaded) {
                Alert.alert(
                    'OTP',
                    'Verification code has been sent to your phone'
                )
                this.setState(
                    {
                        loading: false
                    }
                )
            }
        } catch (err) {
            if (resend && this.viewLoaded) {
                Alert.alert('Oops!', err.message);
                this.setState(
                    {
                        loading: false
                    }
                )
            }
        }
    }

    sendRequest=()=>{
        const {
            data,
            otp,
            page
        }=this.state;
        if (otp===''){
            return;
        }
        let message='';
        let service=null;
        switch (page) {
            case 'INSPECT':
                message=`You have requested to make payment to inspect our property, do you want to proceed?`;
                service=apiService.newInspection;
                break;
            case 'RENEW':
                message=`You have requested to make payment to renew this property, do you want to proceed?`;
                service=apiService.renewRent;
                break;
            case 'RENT':
                message=`You have requested to make payment for this property, do you want to proceed?`;
                service=apiService.newRent;
                break;
            case 'RENT|PAY':
                message=`You have requested to make payment to extend your rent, do you want to proceed?`;
                service=apiService.payRent;
                break;
            case 'RENT|SD':
                message=`Have you changed your mind, Do you still want to proceed?`;
                service=apiService.paySecurityDeposit;
                break;
            case 'RENT|UTILITY':
                message=`You have requested to make payment for Utility, do you want to proceed?`;
                service=apiService.payUtility;
                break;
            case 'SIGNUP':
                service=authService.signup;
                message=`You're about to be registered on the 16th August 85, Villa property app. you can send complains and make payments through this platform with ease, Tap Yes to continue.`
                break;
            default:
                break;
        };
        return Alert.alert(
            'Dear Tenant',
            message,
            [
                {
                    text:'Yes,continue',
                    onPress:async()=>{
                        try {
                            this.setState(
                                {
                                    loading:true
                                }
                            )
                            const payload={...data,otp:otp};
                            const response=await service(payload);
                            if (!response.success) {
                                throw Error(
                                    response.message
                                )
                            };
                            this.setState(
                                {
                                    loading:false
                                },()=>{
                                    Alert.alert(
                                        'Congratulations!',
                                        response.message
                                    )
                                    this.props.navigation.navigate(
                                        this.state.page !== 'SIGNUP' ? 'homePage' : 'signupPage'
                                    )
                                }
                            )
                        } catch (err) {
                            this.setState(
                                {
                                    loading:false
                                },()=>{
                                    Alert.alert(
                                        'Oops!',
                                        err.message
                                    )
                                    switch (this.state.page) {
                                        case 'SIGNUP':
                                            this.props.navigation.navigate(
                                                'signupPage'
                                            )
                                            break;
                                        default:
                                            this.props.navigation.navigate(
                                                'homePage'
                                            )
                                            break;
                                    }
                                }
                            )
                        }
                    }
                },
                {
                    text: 'No, cancel',
                    onPress:()=>{
                        this.state.loading=false;
                        this.props.navigation.goBack();
                    }
                }
            ]
        )
    }

    render() {
        return (
            <>
                <Header name="Confirm your phone number" back={this.props.navigation.goBack} />
                <View style={styles.container}>
                    <View style={{ width: '80%', alignItems: 'center' }}>
                        <TouchableOpacity
                            style={{
                                marginVertical:15
                            }}
                            onPress={()=>this.sendOtp(true)}>
                            <Text>Resend SMS</Text>
                        </TouchableOpacity>
                        <NTextInput
                            onChangeText={v => this.state.otp = v.trim()}
                            placeholder="OTP"
                            keyboardType='number-pad'
                        />
                    </View>
                    <Button
                        onPress={this.sendRequest.bind(this)}
                        loading={this.state.loading}
                        buttonStyle={globalStyles.button}
                        title="DONE"
                    />
                </View>

            </>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        width: "100%",
        alignItems: 'center',
        marginTop: 25

    },
    horizontal: {
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 30,
        marginBottom: 23
    }
});
