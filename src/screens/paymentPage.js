import React, { Component } from 'react'
import { View, StyleSheet,Alert, Dimensions,StatusBar,Platform } from 'react-native'
import { Button, Text } from 'react-native-elements';
import NTextInput from '../components/textInput'
import globalStyles, { DROP_HEIGHT, DROP_WIDTH } from '../constants/styles';
import DropDownPicker from 'react-native-dropdown-picker';

import Header from '../components/header'
import { ScrollView } from 'react-native';
const { height,width } = Dimensions.get('window');

class PaymentPage extends Component {

    state={
        loading:false,
        accountIssuer:'',
        accountNumber:'',
        amount:'',
        data:{},
        page:'',
        toggle:false
    }

    sendOTP() {
        if (this.state.accountNumber.length !== 10) {
            Alert.alert(
                'Oops!', 
                'please enter a valid phone number'
            )
        } else {
            const{
                data,
                accountIssuer,
                accountNumber,
                amount,
                page
            }=this.state;
            const _data={
                data:{
                    ...data,
                    accountIssuer:accountIssuer,
                    accountNumber:accountNumber,
                    amount:amount
                },
                page:page
            };
            this.props.navigation.navigate(
                'OTP',_data
            )
        }
    }

    componentDidMount() {
        const {
            data,
            page,
            currency
        }=this.props.route.params;
        this.setState(
            {
                data,
                page,
                currency,
                amount:data['amount']
            }
        );
    }

    render() {
        return (
            <>
                {Platform.OS==='ios' && <StatusBar barStyle="dark-content"/>}
                <Header name="Payment" back={this.props.navigation.goBack} />
                <ScrollView contentContainerStyle={styles.container} keyboardDismissMode="on-drag" keyboardShouldPersistTaps={'handled'}>
                    <View style={{ width:'80%', alignItems:'center' }}>
                        <NTextInput
                            editable={false}
                            value={`${this.state.currency || 'GHS'}${this.state.amount}`}
                        />
                        <NTextInput
                            onChangeText={v=>this.state.accountNumber=v.trim()} 
                            placeholder="Mobile money number"
                            value={this.state.momoNumber}
                            keyboardType='number-pad'
                            maxLength={10}
                        />
                        <DropDownPicker
                            onOpen={()=>this.setState({toggle:true})}
                            onClose={()=>this.setState({toggle:false})}
                            zIndex={100}
                            placeholder="Select yout network"
                            items={[
                                { label: 'Select your network', value:'=' },
                                { label: 'MTN', value:'mtn' },
                                { label: 'Vodafone Cash', value:'vodafone' },
                                { label: 'AirtelTigo', value:'airteltigo' },
                            ]}
                            itemStyle={{ justifyContent: 'flex-start' }}
                            style={{ borderColor: '#959595', borderWidth: 2, margin: 5, borderRadius: 5, width:DROP_WIDTH }}
                            containerStyle={{ height:DROP_HEIGHT }}
                            onChangeItem={item=>this.setState({ accountIssuer:item.value })}
                        />
                    </View>
                    {!this.state.toggle && <Button
                        onPress={()=>this.sendOTP()}
                        loading={this.state.loading}
                        buttonStyle={globalStyles.button} 
                        title="CONTINUE"
                    />}
                </ScrollView>

            </>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        width: "100%",
        alignItems:'center',
        marginVertical:25,
        height:height
    },
    horizontal: {
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 30,
        marginBottom: 23
    }
});

export default PaymentPage
