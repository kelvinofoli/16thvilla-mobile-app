import React, { Component } from 'react'
import { View, StyleSheet, FlatList, TouchableOpacity, Image, ActivityIndicator, Dimensions,Platform,StatusBar } from 'react-native'
import { Button, Text } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome5';
import IconIo from 'react-native-vector-icons/Ionicons';
import colors from '../constants/colors';
import Header from '../components/header'
import apiService from "../services/api.service";
const { height, width } = Dimensions.get('window');
import { AuthProvider } from "../store/auth.state";

export default class PropertiesPage extends Component {


    static contextType=AuthProvider;

    viewLoaded=false;

    state={
        loading:false,
        dataSource: []
    }


    componentDidMount() {
        this.viewLoaded=true;
        this.getRooms();
    }

    componentWillUnmount(){
        this.viewLoaded=false;
        this.getRooms=null;
    }



    async getRooms() {
        try {
            const {state}=this.context;
            this.setState(
                {
                    loading: true
                }
            );
            const response=await apiService.getAllRooms();
            if (!response.success) {
                throw Error(
                    response.message
                );
            }
            this.viewLoaded && this.setState(
                {
                    dataSource:response.data,
                    loading:false
                }
            )
        } catch (err) {this.getRooms()}
    }


    render() {
        return (
            <>
                {Platform.OS==='ios' && <StatusBar barStyle="dark-content"/>}

                <Header name="Available Rooms" back={this.props.navigation.goBack} />
                {this.state.loading ?
                    <ActivityIndicator style={{
                        alignSelf: 'center',
                        marginTop: height / 2.5
                    }} animating={true}
                    color={colors.theme}
                    size={'large'}
                    /> :
                    <FlatList
                        contentContainerStyle={styles.container}
                        data={this.state.dataSource}
                        numColumns={2}
                        keyExtractor={el=>Math.random()}
                        renderItem={(el) =>
                            <TouchableOpacity style={styles.card} onPress={()=>this.props.navigation.navigate('roomDetailsPage',{data:el.item})} >
                                <Image style={styles.item} source={{uri:el.item.images[0]}} />
                                <View style={{ 
                                    flexDirection: 'row', 
                                    alignItems:'center',
                                    justifyContent:'space-between',
                                    width:width*0.45, 
                                    padding:10
                                }}>
                                    <Text style={{ width: '80%', fontSize: 12 }}>{el.item.type}</Text>
                                    <Icon style={{ marginLeft: 'auto', marginRight: 5 }} name="bed" size={12} color={'orange'} />
                                    <Text style={{ color:'orange' }}>{el.item?.roomNumber}</Text>
                                </View>
                                <View style={styles.horizontal}>
                                    <Text style={{ color:colors.black,marginLeft:10 }}>{el.item.location}</Text>
                                    {el.item.occupied?<Icon name='times-circle' size={20} color='red'/>:<Icon name='check-circle' size={20} color='green'/>}
                                </View>
                            </TouchableOpacity>
                        }
                    />
                }


            </>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        marginTop: 10,
    },
    horizontal: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    item: {
        width:width*0.45,
        height: 150,
        borderRadius: 2,
        margin: 10,
        marginBottom: 6,
        borderColor:'#6d6d6d',
        borderWidth:1
    },
    card:{}
});
