import React, { Component } from 'react'
import { ScrollView,ActivityIndicator,StyleSheet } from 'react-native'
import WithRentsComponent from '../hoc/withRents';
import RentListItem from "../components/rentList.component";
import Header from '../components/header';



class RenewChooseRent extends Component {

    render() {
        return (
            <>
                <Header name="Choose Rent to Renew" back={this.props.navigation.goBack} />
                <ScrollView style={styles.container}>
                    {this.props.loading && <ActivityIndicator size={20} color="#6d6d6d"/>}
                    { this.props.rents && this.props.rents.map((el,id)=>{
                        return (
                            <RentListItem
                                key={id}
                                el={el}
                                {...this.props}
                                onPress={()=>{
                                    this.props.navigation.navigate(
                                        'RenewalDetails',
                                        {
                                            data:el
                                        }
                                    )
                                }}
                            />
                        )
                    }) }
                </ScrollView>
            </>
        )
    }


}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 20
    },
    horizontal: {
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 25,
        marginBottom: 23
    },
    rentItem: {
        width: '100%',
        borderBottomWidth: 2,
        borderColor: '#e5e5e5',
        paddingHorizontal: "10%",
        paddingVertical: 10
    }
})

export default WithRentsComponent(RenewChooseRent);