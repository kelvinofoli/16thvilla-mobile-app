import React, { Component } from 'react'
import { View, StyleSheet,Alert, Dimensions,ScrollView } from 'react-native'
import { Button, Text } from 'react-native-elements';
import NTextInput from '../components/textInput'
import DropDownPicker from 'react-native-dropdown-picker';
import globalStyles, { DROP_HEIGHT, DROP_WIDTH } from '../constants/styles';
import Header from '../components/header';
import { AuthConsumer } from '../store/auth.state';
import generateMonths from "../utils/generateMonths";
const { height, width } = Dimensions.get('window');


class RentProperty extends Component {

    state = {
        loading:false,
        duration:0,
        durations:[],
        paymentType: '',
        amount:'0',
        room:{},
        tenant:{},
        opened:false
    }

    componentDidMount() {
        try {
            let durations=[];
            const room=this.props.route.params.data;
            switch (room.accomodation){
                case 'SHORT STAY':
                    durations=generateMonths(
                        2,
                        12
                    )
                    break;
                default:
                    durations=generateMonths(
                        12,
                        36
                    )
                    break;
            }
            this.setState(
                {
                    durations:durations,
                    room:room
                }
            )
        } catch (err) {
            return this.props.navigation.goBack();
        }

    }



    calculateRoomPrice=(duration)=>{
        const{
            room,
        }=this.state;
        const amount=room.price * duration;
        return this.setState(
            {
                duration:duration,
                amount:amount
            }
        )
    }

    sendRequest=()=>{
        const {
            paymentType,
            tenant,
            duration,
            amount,
            room
        }=this.state;
        const data={
            "room":room['_id'],
            "tenant":tenant['_id'],
            "duration":duration,
            "costPerMonth":room['price'],
            "paymentType":paymentType,
            "amount":amount
        }
        if(data.amount<=0 || data.paymentType==='' || data['duration']===0){
            return;
        }
        return Alert.alert(
            'Hello! Tenant',
            'You have requested to make reservation for this property, your landlord will approve your request when you successfully make payment.',
            [
                {
                    text:'Yes,continue',
                    onPress:()=>this.props.navigation.navigate(
                        'Terms',
                        data
                    )
                },
                {
                    text:'No,cancel',
                    onPress:()=>this.props.navigation.goBack()
                }
            ]
        )
    }

    render() {
        return (
            <>
                <AuthConsumer>
                    {
                        ({state})=>{
                            const {user}=state;
                            this.state.tenant=user;
                        }
                    }
                </AuthConsumer>
                <Header name="Rent property" back={this.props.navigation.goBack} />
                <ScrollView contentContainerStyle={styles.container} keyboardDismissMode="on-drag" keyboardShouldPersistTaps="handled">
                    <View style={{ 
                            width: '80%', 
                            alignItems: 'center' 
                        }}>
                        <NTextInput 
                            editable={false} 
                            value={`${this.state.room.currency}${this.state.amount}`} 
                        />
                        <NTextInput
                            editable={false}
                            value={`${this.state.room.currency}${this.state.room.price} / Month`}
                        />
                        <DropDownPicker
                            zIndex={6}
                            onOpen={()=>{this.setState({opened:true})}}
                            onClose={()=>this.setState({opened:false})}
                            placeholder="Select Duration"
                            items={this.state.durations}
                            itemStyle={{ justifyContent: 'flex-start' }}
                            style={{
                                borderColor:'#959595', 
                                borderWidth:2,
                                margin:5,
                                borderRadius:5,
                                width:DROP_WIDTH
                            }}
                            containerStyle={{ height:DROP_HEIGHT }}
                            onChangeItem={item=>this.calculateRoomPrice(item.value)}
                        />
                        <DropDownPicker
                            onOpen={()=>{this.setState({opened:true})}}
                            onClose={()=>this.setState({opened:false})}
                            zIndex={5}
                            placeholder="Payment method"
                            items={[
                                { label: 'Cash', value: 'CASH' },
                                { label: 'Mobile Money', value: 'MOMO' },
                                { label: 'Bank Deposit', value: 'BANK DEPOSIT' },
                            ]}
                            itemStyle={{ justifyContent: 'flex-start' }}
                            style={{ 
                                borderColor:'#959595', 
                                borderWidth:2, 
                                margin:5,
                                borderRadius:5, 
                                width:DROP_WIDTH
                            }}
                            containerStyle={{ height:DROP_HEIGHT }}
                            onChangeItem={item=>this.setState(
                                { 
                                    paymentType:item.value
                                }
                            )}
                        />
                    </View>
                    {!this.state.opened && <Button
                        onPress={this.sendRequest.bind(this)}
                        containerStyle={{
                            alignSelf:'center' 
                        }}
                        loading={this.state.loading}
                        buttonStyle={globalStyles.button}
                        title="CONTINUE"
                    />}
                </ScrollView>
            </>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        width: "100%",
        alignItems: 'center',
        marginTop: 25

    },
    horizontal: {
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 30,
        marginBottom: 23
    }
});

export default RentProperty
