import React, { Component } from 'react'
import { View, StyleSheet, ScrollView, Alert, Dimensions,Image } from 'react-native'
import { Button, Text } from 'react-native-elements';
import NTextInput from '../components/textInput'
import Header from '../components/header'
import globalStyles, { DROP_HEIGHT, DROP_WIDTH } from '../constants/styles';
import DropDownPicker from 'react-native-dropdown-picker';
import CheckBox from '@react-native-community/checkbox';
import DatePicker from 'react-native-date-picker';
import { TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import moment from "moment";
import { Countries } from '../constants/countries';
import {launchImageLibrary} from 'react-native-image-picker';




const {width}=Dimensions.get('screen');



export default class RentSignupPage extends Component {

    state={
        confirmpassword:'',
        password:'',
        checked:false,
        photo:''
    }

    capture=()=>launchImageLibrary(
        {
            mediaType:'photo',
            includeBase64:true
        },({base64,type})=>{
            const photo=`data:${type};base64,${base64}`;
            this.setState(
                {photo}
            )
    })


    onDateChange(v){
        try {
            this.setState(
                {
                    dateOfBirth:moment(v.toISOString()).format('LL'),
                    date:v
                }
            )
        } catch (err) {
            console.log(err);
        }
    }

    send() {
        try {

            const data = this.state;
            if(data.photo===''){
                return Alert.alert(
                    'Oops',
                    'Please upload a valid photo of yourself'
                )
            }
            if (data.phone === '' || data.confirmpassword === '' || data.password === '') {
                Alert.alert(
                    'Oops',
                    'Please fill the form'
                )
                return;
            };
            if (data.confirmpassword !== data.password) {
                throw Error(
                    'Passwords does not match'
                )
            }
            return Alert.alert(
                'Confirm Account',
                'By signing up you agree to our terms and conditions',
                [
                    {
                        text:'No',
                        onPress:null
                    },
                    {
                        text: 'Yes',
                        onPress:()=>this.props.navigation.navigate('OTP', { data:this.state,page:'SIGNUP' })
                    }
                ]
            )
        } catch (err) {
            Alert.alert(
                'Oops!',
                err.message
            )
        }
    }
    render() {
        return (
            <>
                <Header name="TENANT DETAILS" back={this.props.navigation.goBack} />
                <ScrollView style={styles.container} contentContainerStyle={{ alignItems: 'center' }}>
                    <View style={{ width: '80%', alignItems: 'center' }}>
                        <View style={{marginVertical:10}}>
                            <Text style={{fontWeight:'bold',fontSize:20}}>TENANT DETAILS FORM</Text>
                            <Text>PLEASE ENSURE THAT THIS FORM IS FILLED OUT IN FULL</Text>
                        </View>
                        <View style={styles.imageContainer}>
                            <Image style={styles.image} source={{uri:this.state.photo}}/>
                            <Button 
                                onPress={this.capture.bind(this)} 
                                buttonStyle={styles.capture} 
                                title="UPLOAD PHOTO" 
                            />
                        </View>
                        <DropDownPicker
                                zIndex={2}
                                placeholder="Title"
                                items={
                                    [
                                        {
                                            label:'Mr',
                                            value:'Mr'
                                        },
                                        {
                                            label:'Mrs',
                                            value:'Mrs'
                                        }
                                    ]
                                }
                                itemStyle={{ justifyContent: 'flex-start' }}
                                style={{ 
                                    borderColor: '#959595', 
                                    borderWidth: 2, 
                                    margin: 5, 
                                    borderRadius: 5, 
                                    width:DROP_WIDTH
                                }}
                                containerStyle={{ height:DROP_HEIGHT }}
                                onChangeItem={item=>this.state.title=item.value}
                            />
                        <DropDownPicker
                            zIndex={1}
                            placeholder="Sex"
                            items={
                                [
                                    {
                                        label:'Male',
                                        value:'Male'
                                    },
                                    {
                                        label:'Female',
                                        value:'Female'
                                    }
                                ]
                            }
                            itemStyle={{ justifyContent: 'flex-start' }}
                            style={{ 
                                borderColor: '#959595', 
                                borderWidth: 2, 
                                margin: 5, 
                                borderRadius: 5, 
                                width:DROP_WIDTH
                            }}
                            containerStyle={{ height:DROP_HEIGHT }}
                            onChangeItem={item=>this.state.gender=item.value}
                        />
                        <NTextInput
                            onChangeText={v=>this.setState({firstname:v.trim()})}
                            placeholder="First Name" 
                        />
                        <NTextInput
                            onChangeText={v => this.state.lastname = v.trim()}
                            placeholder="Last Name" 
                        />
                        {this.state.show && <View style={{
                            marginVertical:15,
                            borderColor:'#6d6d6d',
                            borderWidth:1,
                            borderRadius:5
                        }}>
                            <TouchableOpacity 
                                hitSlop={{top:10,bottom:10,left:10,right:10}}
                                onPress={()=>this.setState({show:false})}
                            style={{
                                margin:10,
                                width:50
                            }}>
                                <Icon name="close" size={20}/>
                            </TouchableOpacity>
                            <DatePicker
                                mode={'date'}
                                date={ this.state.date || new Date()}
                                onDateChange={this.onDateChange.bind(this)}
                            />
                        </View>}
                        {!this.state.show && <TouchableOpacity
                            onPress={()=>this.setState({show:true})}
                             style={globalStyles.dateInput}>
                            <Text style={{color:'#6d6d6d'}}>{this.state.dateOfBirth || 'Date of Birth'}</Text>
                            </TouchableOpacity>}
                        <NTextInput
                            onChangeText={v => this.state.age=v.trim()}
                            placeholder="Age" 
                        />
                        <DropDownPicker
                            zIndex={3}
                            placeholder="Nationality"
                            items={Countries.map(c=>{
                                return {
                                    label:c.nationality,
                                    value:c.nationality
                                }
                            })}
                            itemStyle={{ justifyContent: 'flex-start' }}
                            style={{ 
                                borderColor: '#959595', 
                                borderWidth: 2, 
                                margin: 5, 
                                borderRadius: 5, 
                                width:DROP_WIDTH 
                            }}
                            containerStyle={{ height:DROP_HEIGHT }}
                            onChangeItem={item=>this.state.nationality=item.value}
                        />
                        <DropDownPicker
                            zIndex={2}
                            placeholder="ID Type"
                            items={
                                [
                                    {
                                        label:'Passport',
                                        value:'passport'
                                    },
                                    {
                                        label:'Voters ID',
                                        value:'voter'
                                    },
                                    {
                                        label:'Driver Licence',
                                        value:'driverlicence'
                                    },
                                    {
                                        label:'Ghana Card',
                                        value:'ghanacard'
                                    }
                                ]
                            }
                            itemStyle={{ justifyContent: 'flex-start' }}
                            style={{ 
                                borderColor: '#959595', 
                                borderWidth: 2, 
                                margin: 5, 
                                borderRadius: 5, 
                                width:DROP_WIDTH 
                            }}
                            containerStyle={{ height:DROP_HEIGHT }}
                            onChangeItem={item=>this.state.idType=item.value}
                        />
                        <NTextInput
                            onChangeText={v => this.state.idNumber=v.trim()}
                            placeholder="ID Number" 
                        />

                        <DropDownPicker
                            zIndex={1}
                            placeholder="Marital Status"
                            items={
                                [
                                    {
                                        label:'I am married',
                                        value:'YES'
                                    },
                                    {
                                        label:'Not Married',
                                        value:'NO'
                                    }
                                ]
                            }
                            itemStyle={{ justifyContent: 'flex-start' }}
                            style={{ 
                                borderColor: '#959595', 
                                borderWidth: 2, 
                                margin: 5, 
                                borderRadius: 5, 
                                width:DROP_WIDTH 
                            }}
                            containerStyle={{ height:DROP_HEIGHT }}
                            onChangeItem={item=>this.state.married=item.value}
                        />

                        {this.state.married==='YES' && <NTextInput
                            keyboardType="number-pad"
                            maxLength={10}
                            onChangeText={v=>this.state.numberOfChildren = v.trim()}
                            placeholder="Number of children" 
                        />}

                        <Text style={{ marginVertical:10 }}>CONTACT DETAILS</Text>

                        <NTextInput
                            keyboardType="phone-pad"
                            maxLength={10}
                            onChangeText={v=>this.state.phone=v.trim()}
                            placeholder="Mobile Number" 
                        />
                        <NTextInput
                            keyboardType="phone-pad"
                            onChangeText={v=>this.state.officeNumber=v.trim()}
                            placeholder="Work Number" 
                        />
                        <NTextInput
                            onChangeText={v=>this.state.email=v.trim()}
                            placeholder="Personal Email" 
                        />
                        <NTextInput
                            onChangeText={v=>this.state.currentAddress=v}
                            placeholder="Current Address" 
                        />
                        <NTextInput
                            onChangeText={v=>this.state.postalAddress=v}
                            placeholder="Postal Address" 
                        />
                        <DropDownPicker
                            zIndex={2}
                            placeholder="Do you smoke"
                            items={
                                [
                                    {
                                        label:'Yes, I smoke',
                                        value:'YES'
                                    },
                                    {
                                        label:'No, I dont',
                                        value:'NO'
                                    }
                                ]
                            }
                            itemStyle={{ justifyContent: 'flex-start' }}
                            style={{ 
                                borderColor: '#959595', 
                                borderWidth: 2, 
                                margin: 5, 
                                borderRadius: 5, 
                                width:DROP_WIDTH 
                            }}
                            containerStyle={{ height:DROP_HEIGHT }}
                            onChangeItem={item=>this.state.smoke=item.value}
                        />
                        <DropDownPicker
                            zIndex={1}
                            placeholder="Will you keep pets in the property? If so what kind?"
                            items={
                                [
                                    {
                                        label:'Yes',
                                        value:'YES'
                                    },
                                    {
                                        label:'No',
                                        value:'NO'
                                    }
                                ]
                            }
                            itemStyle={{ justifyContent: 'flex-start'}}
                            style={{ 
                                borderColor: '#959595', 
                                borderWidth: 2, 
                                margin: 5, 
                                borderRadius: 5, 
                                width:DROP_WIDTH 
                            }}
                            containerStyle={{ height:DROP_HEIGHT }}
                            onChangeItem={item=>this.state.havePets=item.value}
                        />

                        <Text style={{ marginVertical:20 }}>REQUIRED INFORMATION FROM TENANTS</Text>
                        <NTextInput
                            onChangeText={v=>this.state.businessLocation=v}
                            placeholder="Name of Place of Employment/Business" 
                        />
                        <NTextInput
                            onChangeText={v=>this.state.businessContactName=v}
                            placeholder="Name of Contact at Company/Business" 
                        />
                        <NTextInput
                            onChangeText={v=>this.state.businessContactNumber=v}
                            placeholder="Contact number" 
                        />
                        <NTextInput
                            onChangeText={v=>this.state.businessEmail=v}
                            placeholder="Email Address" 
                        />
                        <NTextInput
                            onChangeText={v=>this.state.occupation=v}
                            placeholder="Your Job Title" 
                        />
                        <NTextInput
                            onChangeText={v=>this.state.yearsEmployed=v}
                            placeholder="Years Employed" 
                        />

                        <Text style={{ marginVertical:10 }}>CONTACT PERSON IN CASE OF EMERGENCY</Text>

                        <NTextInput
                            onChangeText={v=>this.state.contactPersonName=v}
                            placeholder="Name" 
                        />
                        <NTextInput
                            onChangeText={v=>this.state.contactPersonPhone=v}
                            placeholder="Telephone Number" 
                        />
                        <NTextInput
                            onChangeText={v=>this.state.contactPersonRelationship=v}
                            placeholder="Relationship to the Person" 
                        />
                        <NTextInput
                            onChangeText={v=>this.state.contactPersonEmail=v}
                            placeholder="Email Address" 
                        />
                        <NTextInput
                            onChangeText={v=>this.state.contactPersonAddress=v}
                            placeholder="Residential Address" 
                        />

                        <Text style={{marginVertical:10}}>LOGIN DETAILS</Text>
                        <NTextInput
                            secureTextEntry={true}
                            onChangeText={v=>this.state.password=v.trim()}
                            placeholder="password" 
                        />
                        <NTextInput
                            secureTextEntry={true}
                            onChangeText={v => this.state.confirmpassword = v.trim()}
                            placeholder="Confirm password" 
                        />
                        <View style={{margin:50,flexDirection:'row',alignItems:'center'}}>
                        <CheckBox
                            style={{marginRight:20}}
                            disabled={false}
                            value={this.state.checked}
                            onValueChange={(c) =>this.setState(
                                {
                                    checked:c
                                }
                            )}
                        />
                        <Text style={{color:'#6d6d6d'}}>I <Text style={{fontWeight:'bold'}}>{this.state.firstname}</Text> do hereby confirm that the information provided by 
me are correct and accurate to the best of my knowledge</Text>
                    </View>
                    </View>
                    {this.state.checked && <Button onPress={() => this.send()} buttonStyle={globalStyles.button} title="CONTINUE" />}
                </ScrollView>

            </>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        width: "100%",
        flex: 1,
        marginTop: 25,
    },
    horizontal: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: '100%'
    },
    imageContainer:{
        marginRight:'auto',
        marginLeft:'auto',
        justifyContent:'center',
        alignItems:'center',
        marginVertical:10
    },
    image:{
        height:150,
        width:150,
        borderWidth:1,
        borderColor:'#ddd',
        borderRadius:5
    },
    capture:{
        marginVertical:5,
        backgroundColor:'#000'
    }
});
