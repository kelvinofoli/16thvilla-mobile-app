import React, { Component } from 'react'
import { Text, View, ScrollView,StyleSheet,Platform,StatusBar } from 'react-native'
import Header from '../components/header'

export default class Rentlaws extends Component {
    render() {
        return (
            <ScrollView contentContainerStyle={{
                padding:15
            }}>
                {Platform.OS==='ios' && <StatusBar barStyle="dark-content"/>}
                <Header
                    name={'RENT CONTROL LAW'}
                    back={()=>this.props.navigation.goBack()}
                />
                <Text style={styles.p}>Every agreement related to tenancy should be in accordance with the laws of the nation. Like any other contract, the lease agreement should include the rules and rights, as well as the regulations of both the tenants and the landlords. The agreement should be signed by both parties, and a copy of the contract should be issued to all involved.</Text>
                
                <Text style={styles.p}>The Rent Act of Ghana (Act 220) was established in 1963 and was mandated by the Rent Control Division, as the body in charge of monitoring and establishing guidelines to manage the tenant-landlord relationship. There are also rent control laws in Ghana to regulate the eviction of tenants by landlords. This Act procures the tenant a period of time to search for accommodation while he/she is being evicted; this is normally three months.</Text>

                <Text style={styles.p}>
                Although a Rent Act exists, a law court can order the immediate vacation of the tenant from a premise for reasons such as the immediate need of property for personal use by the landlord or the landlord’s need to renovate the property. Even in such situations, the tenant is given some time to search for accommodation.
                </Text>

                <Text style={styles.p}>If you have any issues with your landlord and you need help, the best option is to speak to Rent Control Ghana. This authority is responsible for handling disagreements between tenants, landlords and any other individuals interested in a particular property and will be able to provide further advice in any difficult situations.</Text>

                <Text style={[styles.p,{ fontWeight:'bold' }]}>Rent Control Law 1986 (PNDCL 138)</Text>

                <Text>Rent in Respect of Residential Premises</Text>
                <Text style={styles.p}>1. The rent payable by a tenant in respect of single or two-roomed accommodation in any residential premises shall be as specified in the First Schedule to this Law.</Text>

                <Text style={styles.p}>2. Where the residential accommodation in respect of which the rent is payable is smaller or larger in dimensions than those specified in the First Schedule the recoverable rent shall be proportionate to the dimensions of such accommodation.</Text>

                <Text style={styles.p}>3. The Secretary may by legislative instrument amend the First Schedule from time to time as he deems necessary.</Text>

                <Text style={styles.p}>4. Notwithstanding any provision in any enactment to the contrary and until the 6th day of March 1987 no landlord shall raise the rent prescribed by this Law in respect of any residential accommodation referred to in subsection (1) of this section. [As substituted by the Rent Control (Amendment) Law, 1986 (PNDCL 163) s.(a)]</Text>

                <Text style={[styles.p,{ fontWeight:'bold' }]}>Exemptions</Text>
                <Text>The provisions of Section 1 of this Law shall not apply to any lease or tenancy</Text>

                <Text style={styles.p}>1. Held by Government or other State agency;</Text>
                <Text style={styles.p}>2. Of premises let for industrial, commercial or other business purposes</Text>
                <Text style={styles.p}>3. Of premises rented by diplomatic or consular missions, international organisations, foreign companies and firms which under section 1 of the Rent (Amendment) (No. 3) Decree, 1979 (A.F.R.C.D. 51) are required to pay the foreign exchange equivalent of their rents to the Bank of Ghana in the first instance</Text>
                <Text style={styles.p}>4. aken by any incorporated body other than a body the whole proprietary interest in which is held by the Government or other State agency; and (e) where the rent payable exceeds ¢1,000.00 a month.</Text>

                <Text style={[styles.p,{ fontWeight:'bold' }]}>Houses Built by Tema Development Corporation or the State Housing Corporation etc.</Text>

                <Text style={styles.p}>1. With effect from the commencement of this Law and subject to subsections of this section, any person who derives his title to any premises from the Tema Development Corporation, the State Housing Corporation or other similar housing organisation or agency sponsored by the Government, under a subsisting hire-purchase agreement howsoever called, and who in turn sublets or has sublet the said premises shall (notwithstanding any agreement to the contrary) not charge the tenant or demand or receive from him a monthly rent in respect of the premises which exceeds his aggregate of:a) the instalment which that person pays per month to either the State Housing Corporation, the Tema Development Corporation or other similar housing organization or agency sponsored by the Government, as the case may be;b) the amount which is the equivalent of the property rate or other imposts payable by such a person in respect of the said premises; andc) twenty-five percent of the total of the amounts referred to in paragraphs (a) and (b) of this subsection.</Text>

                <Text style={styles.p}>2. Where the instalment which is paid by the person referred to in subsection (1) to any of the bodies referred to therein, is different from the instalment which he would have paid if no deposit had been paid and he had agreed to pay the purchase price of the premises over a period of twenty years from the date of commencement of the hire-purchase agreement, then the reference in subsection (1)(a) of this section to the monthly instalment paid by him shall nevertheless be deemed to be a reference to the instalment which he would have paid if no deposit had been paid and he had agreed to pay the purchase price over a period of twenty years.</Text>

                <Text style={styles.p}>3. A certificate issued by any of the said bodies certifying the instalment payable under subsection (1)(a) or subsection (2) of this section by any person referred to in subsection (1) shall be prima facie evidence of such instalment.</Text>

            </ScrollView>
        )
    }
}


const styles = StyleSheet.create({
    p:{
        marginVertical:20
    }
});