import React, { Component, useState } from 'react'
import { View, StyleSheet,Alert,Image} from 'react-native'
import { Button, Text } from 'react-native-elements';

import colors from '../constants/colors';
import globalStyles from '../constants/styles';
import NTextInput from '../components/textInput'
import { ScrollView } from 'react-native';
import Header from '../components/header';
import service from "../services/api.service";

const logo=require("../assets/images/logo.png");


const ResetPassword=(props)=>{

    const [loading,setLoading]=useState(false);
    const [state,setState]=useState({otp:'',phone:'',otpsent:false,password:''});

    const sendOtp=async()=>{
        try {
            const {phone}=state;
            if(!phone || phone.length<10){
                throw Error(
                    'Enter a valid phone number'
                )
            }
            setLoading(true);
            const response=await service.sendOtp(
                {
                    phoneNumber:phone
                }
            );
            if(!response.success){
                throw Error(
                    response.message
                )
            }
            setLoading(false);
            setState({...state,otpsent:true});
            Alert.alert(
                'OTP SUCCESS',
                'OTP has been sent to the phone number you entered. enter the OTP to proceed with your password reset request'
            )
        } catch (err) {
            setLoading(false);
            Alert.alert(
                'OTP ERROR',
                err.message
            )
        }
    };

    const reset=async()=>{
        try {
            const {phone,otp,password}=state;
            console.log(phone,otp,password);
            if(!phone || phone.length<10){
                setState({...state,otpsent:false});
                throw Error(
                    'Enter a valid phone number'
                )
            }
            if(!otp || otp.length<=0){
                throw Error(
                    'Enter a valid OTP sent to your phone'
                )
            }
            if(!password || password.length<=0){
                throw Error(
                    'Enter a valid password'
                )
            }
            setLoading(true);
            const response=await service.resetpassword(
                {
                    phone,
                    password,
                    otp
                }
            );
            if(!response || !response.success){
                throw Error(
                    response.message
                )
            };
            setLoading(false);
            Alert.alert(
                'RESET MESSAGE',
                'Password reset successful,you can login to continue.'
            )
        } catch (err) {
            setLoading(false);
            Alert.alert(
                'RESET ERROR',
                err.message
            )
        }
    }

    return (
        <View style={{backgroundColor:'#fff',flex:1}}>
        <Header name="Reset Password" back={props.navigation.goBack} />
            <ScrollView contentContainerStyle={styles.container} keyboardShouldPersistTaps={'handled'} keyboardDismissMode="on-drag">
                <Image style={{height:200,width:250}} source={logo}/>
                <View style={{ width: '80%', alignItems: 'center', marginVertical:20}}>
                    <Text style={{fontSize:20,paddingVertical:20}}>Enter the phone number you registered on the app with.</Text>
                    <NTextInput
                    _style={{backgroundColor:'#FCFCFC'}}
                        onChangeText={v=>setState({...state,phone:v.trim()})}
                        placeholder="Phone Number" 
                        maxLength={10}
                    />
                    {state.otpsent && <>
                        <NTextInput
                        _style={{backgroundColor:'#FCFCFC'}}
                            onChangeText={v=>setState({...state,otp:v.trim()})}
                            placeholder="Enter OTP" 
                            maxLength={5}
                        />
                        <NTextInput
                            _style={{backgroundColor:'#FCFCFC'}}
                            secureTextEntry={true}
                            onChangeText={v=>setState({...state,password:v.trim()})}
                            placeholder="New Password" 
                        />
                    </>}
                    <Button
                        loading={loading}
                        onPress={state.otpsent?reset:sendOtp}
                        buttonStyle={globalStyles.button}
                        title="ENTER" 
                    />
                </View>
            </ScrollView>
        </View>
    )
}



const styles = StyleSheet.create({
    container: {
        flexDirection:'column',
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center'
    },
    button: {
        width: 200,
        backgroundColor: colors.theme,
        justifyContent: 'center',
        alignItems: 'center',
        height: 50,
        marginTop: 10
    },
    circle: {
        width: 60,
        height: 60,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: colors.theme,
        borderRadius: 30,
        marginBottom: 15
    }
});


export default ResetPassword;