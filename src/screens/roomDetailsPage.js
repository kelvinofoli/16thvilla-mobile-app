import React, { Component } from 'react'
import { View, StyleSheet, Image, Dimensions, TouchableOpacity ,Linking} from 'react-native'
import { Button, Text } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import IconFA from 'react-native-vector-icons/FontAwesome5';
import IconEn from 'react-native-vector-icons/Entypo';

import Header from '../components/header'
import colors from '../constants/colors';
import globalStyles from '../constants/styles';
import { AuthConsumer } from '../store/auth.state';
const { height, width } = Dimensions.get('window');
import ImageSlider from 'react-native-image-slider';
import { ScrollView } from 'react-native';
import service from "../services/api.service";
import { Alert } from 'react-native';
import apiService from '../services/api.service';



export default class RoomDetailsPage extends Component {

    state={
        loading:false,
        images:[],
        room:{},
        exchangeRate:0
    }

    componentDidMount() {
        this.loadSettings();
        this.setState({ 
            room: this.props.route.params[
                'data'
            ] 
        },()=>{
            this.setState(
                {
                    images:this.state.room.images ||[]
                }
            )
        });
    }

    loadSettings(){
        apiService.loadSettings().then(res=>{
            if(res.success){
                try {
                    const rate=res.data.dollarRate;
                    this.setState(
                        {
                            exchangeRate:rate || 0
                        }
                    )
                } catch (err) {
                }
            }
        })
    }

    delete(){
        return Alert.alert(
            'Delete Property',
            'you have requested to delete this property, do you confirm your request?',
            [
                {
                    text:'Yes,Delete',
                    onPress:()=>{
                        this.setState({loading:true});
                        service.deleteRoom(
                            this.state.room[
                                '_id'
                            ]
                        ).then(res=>{
                            if(res===null){
                                throw Error(
                                    'Charlie check your internet connection'
                                )
                            }
                            if(!res.success){
                                throw Error(
                                    res[
                                        'message'
                                    ]
                                )
                            };
                            this.setState(
                                {
                                    loading:false
                                },()=>{
                                    this.props.navigation.goBack();
                                    Alert.alert(
                                        'yay!',
                                        'Property deleted successfully'
                                    )
                                }
                            )
                        }).catch(err=>this.setState(
                            {
                                loading:false
                            },()=>{
                                Alert.alert(
                                    'Oops',
                                    err.message
                                )
                            }
                        ))
                    }
                },
                {
                    text:'No,Cancel',
                    onPress:null
                }
            ]
        )
    }

    openDirection=()=>{
        Linking.openURL(`https://www.google.com/maps/dir/?api=1&travelmode=driving&dir_action=navigate&destination=${this.state.room.location}`)
    }

    inspect(){
        if(this.state.room.occupied){
            return Alert.alert(
                'Oops,Sorry',
                'You are late this property is already occupied!'
            )
        }

        this.props.navigation.navigate('inspectPropertyPage',{data:this.state.room})

    }

    render() {

        return (
            <>
                <Header name="Property Details"  back={this.props.navigation.goBack} />
                <ScrollView contentContainerStyle={styles.container}>
                    <ImageSlider 
                        style={styles.images}
                        images={this.state.images}
                    />

                    <View style={styles.info}>
                        <View style={styles.horizontal}>
                            <View>
                                <Text h2 style={{ color: colors.theme,fontWeight:'bold' }}>{this.state.room?.currency}{this.state.room?.price}</Text>
                                {this.state.room?.currency==='USD' && <Text style={{color:'red'}}>GHS{parseFloat(this.state.room?.price*this.state.exchangeRate).toFixed(2)}</Text>}
                            </View>
                            <Text h5 style={{ fontSize:20,width:'50%',textAlign:'right' }}>{this.state.room.type}</Text>
                        </View>

                        <View style={{...styles.horizontal,justifyContent:'space-between'}}>
                            <View style={styles.horizontal}>
                                <View style={styles.horizontal}>
                                    <Icon style={{ marginRight:10 }} name="bed" size={16} color={colors.theme} />
                                    <Text>{this.state.room.roomNumber}</Text>
                                </View>
                                <View style={{...styles.horizontal,marginLeft:15}}>
                                    <IconEn 
                                        style={{ marginRight:10 }} 
                                        name="location" 
                                        size={16} 
                                        color={colors.theme} 
                                    />
                                    <Text>{this.state.room.location}</Text>
                                </View>
                            </View>
                            <TouchableOpacity
                                onPress={this.openDirection}
                                style={styles.direction}
                            >
                                <IconFA 
                                    name={"map-marked-alt"} 
                                    size={15} 
                                    color="white"
                                />
                            </TouchableOpacity>
                        </View>

                        <View style={{marginVertical:10}}>
                            <Text style={{ marginTop: 15,marginVertical:10,fontSize:20 }}>Description</Text>
                            <Text style={{ color:'grey' }}>{this.state.room.details}</Text>
                        </View>
                    </View>

                    <View style={[styles.horizontal, {justifyContent:'space-around'}]}>
                        <AuthConsumer>
                        {
                            ({state})=>{
                                    const user=state.user;
                                    const role=state['role'];
                                    if(role!=='landlord'){
                                        if(user && user._id){
                                            return (
                                                <>
                                                    <Button
                                                        onPress={
                                                            ()=>this.props.navigation.navigate('rentPropertyPage',{
                                                                data:this.state.room
                                                            })
                                                        } 
                                                        buttonStyle={[globalStyles.button, { 
                                                            width: width * 0.4 
                                                        }]} 
                                                        title="RENT" 
                                                    />
                                                    <Button
                                                        onPress={this.inspect.bind(this)} 
                                                        buttonStyle={[globalStyles.button, { backgroundColor: 'orange', width: width * 0.4 }]} 
                                                        title="INSPECT" 
                                                    />
                                                </>
                                            )
                                        }else{
                                            return(
                                                <Button
                                                    onPress={this.inspect.bind(this)}
                                                    buttonStyle={[globalStyles.button, { backgroundColor: 'orange', width: width * 0.4 }]} 
                                                    title="INSPECT" 
                                                />
                                            )
                                        }
                                    }else{
                                        return (
                                            <View style={{
                                                flexDirection:'row',
                                                justifyContent:'space-between',
                                                marginVertical:15,
                                                width:'100%'
                                            }}>
                                                <TouchableOpacity
                                                    onPress={this.delete.bind(this)}
                                                    style={{
                                                        width:50,
                                                        height:50,
                                                        marginHorizontal:30,
                                                        marginTop:20
                                                    }}
                                                >
                                                    <IconFA 
                                                        name="times"
                                                        size={35}
                                                        color={colors.theme}
                                                    />
                                                </TouchableOpacity>
                                                <TouchableOpacity
                                                    onPress={()=>{
                                                        this.props.navigation.navigate(
                                                            'UpdatesPage',
                                                            {
                                                                data:this.state['room'],
                                                                keys:'rooms'
                                                            }
                                                        )
                                                    }}
                                                    style={{
                                                        width:50,
                                                        height:50,
                                                        marginHorizontal:30,
                                                        marginTop:20
                                                    }}
                                                >
                                                    <IconFA 
                                                        name="pen"
                                                        size={30}
                                                        color={colors.theme}
                                                    />
                                                </TouchableOpacity>
                                            </View>
                                        )
                                    }
                                }
                            }
                        </AuthConsumer>
                    </View>

                </ScrollView>
            </>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        alignItems:'center',
    },
    info: {
        paddingHorizontal:15,
        marginTop:10,
    },
    txt: {
        fontSize: 18
    },
    horizontal: {
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical:5
    },
    images:{
        width:'100%',
        height:height/3
    },
    direction:{
        alignItems:'center',
        justifyContent:'center',
        width:30,
        height:30,
        backgroundColor:'orange',
        borderRadius:50
    }
});
