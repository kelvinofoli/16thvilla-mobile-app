import React, { Component } from 'react';
import { Text, View,ScrollView,StyleSheet,TouchableOpacity,Dimensions } from 'react-native';
import NTextInput from '../components/textInput'
import Header from '../components/header';
import Values from "../constants/values";
import Icon from 'react-native-vector-icons/FontAwesome';
import DatePicker from 'react-native-date-picker';
import moment from "moment";
import { CustomPicker } from 'react-native-custom-picker';
import { Button } from 'react-native-elements';
import globalStyles from '../constants/styles';
import { BASEURL } from '../api/api';
import apiService from '../services/api.service';
import { Alert } from 'react-native';
const {width}=Dimensions.get('screen');




export default class UpdatesPage extends Component {

    state={
        loading:false,
        keys:''
    }

    componentDidMount(){
        const {data,keys}=this.props.route.params;
        this.setState(
            {
                ...data,keys:keys
            }
        )
    }


    update=async()=>{
        try {
            let updates={};
            let response={};
            Values[this.state.keys].forEach(({value})=>{
                updates[value]=this.state[value];
            });
            this.setState(
                {
                    loading:true
                }
            );
            const data={
                id:this.state._id,
                data:updates
            }
            switch (this.state.keys) {
                case 'rents':
                    response=await apiService.updateRent(
                        data
                    );
                    break;
                case 'rooms':
                    response=await apiService.updateRoom(
                        data
                    )
                    break;
                case 'payments':
                    response=await apiService.updatePayment(
                        data
                    )
                    break;
                default:
                    response=await apiService.updateTenant(
                        data
                    );
                    break;
            }
            if(!response.success){
                throw Error(
                    response.message
                )
            };
            this.setState(
                {
                    loading:false
                },()=>{
                    Alert.alert(
                        'Update Successful',
                        'Your request has been completed successfully'
                    )
                }
            )
        } catch (err) {
            this.setState(
                {
                    loading:false
                },()=>{
                    Alert.alert(
                        'Update failed',
                        err.message
                    )
                }
            )
        }
    }


    loadData(data){
        fetch(`${BASEURL}/${data['url']}`,{method:'GET'})
            .then(res=>res.json())
            .then(res=>{
                if(res.success){
                    const _data=res.data.map(d=>{return { label:`${d['type']} ~ ${d['location']}`,value:d['_id'] } });
                    this.setState(
                        {
                            [data['listName']]:_data
                        }
                    )
                }
            }).catch(err=>null);
    }

    render() {
        return (
            <>
            <Header name={`Update ${this.state.keys}`} back={this.props.navigation.goBack} />
            <ScrollView contentContainerStyle={styles.container}>
                {
                    this.state.keys.length>0 && Values[this.state.keys].map((data,i)=>{
                        switch (data['type']) {
                            case 'PICKER-DYNAMIC':
                                if(!Array.isArray(this.state[data['listName']]) || this.state[data['listName']] <=0){
                                    this.loadData(data);
                                }
                                return(
                                    <>
                                        <Text style={styles.heading}>{data['label']} ~ {data['listName']==='rooms' && this.state['room']['type']}</Text>
                                        <View style={styles.dateInput}>
                                            <CustomPicker
                                                options={this.state[data['listName']] || []}
                                                getLabel={item=>item.label}
                                                onValueChange={item=>this.state[data['value']]=item.value}
                                                placeholder={''}
                                                defaultValue={this.state[data['value']]}
                                            />
                                        </View>
                                    </>
                                )
                            case 'INPUT-DATE':
                                if(this.state[data['show']]){
                                    return(
                                        <>
                                            <Text style={styles.heading}>{data['label']}</Text>
                                            <View style={{
                                                    marginVertical:15,
                                                    borderColor:'#6d6d6d',
                                                    borderWidth:1,
                                                    borderRadius:5
                                                }}>
                                                    <TouchableOpacity 
                                                        hitSlop={{top:10,bottom:10,left:10,right:10}}
                                                        onPress={()=>this.setState({[data['show']]:false})}
                                                        style={{
                                                            margin:10,
                                                            width:50
                                                        }}>
                                                        <Icon name="close" size={20}/>
                                                    </TouchableOpacity>
                                                    <DatePicker
                                                        mode={'date'}
                                                        date={ new Date(this.state[data['value']]) || new Date()}
                                                        onDateChange={(v)=>this.state[data['value']]=v}
                                                    />
                                            </View>
                                        </>
                                    )
                                }else{
                                    return(
                                        <>
                                            <Text style={styles.heading}>{data['label']}</Text>
                                            <TouchableOpacity
                                                placeholder="Date"
                                                keyboardType='number-pad'
                                                onPress={()=>this.setState({[data['show']]:true})}
                                                style={styles.dateInput}
                                            >
                                                <Text style={{ fontSize: 18, color: '#000' }}>
                                                    {
                                                        moment(this.state[data['value']]).format(
                                                            'DD-MM-YYYY'
                                                        )
                                                    }
                                                </Text>
                                            </TouchableOpacity>
                                        </>
                                    )
                                }
                            case 'PICKER':
                                return (
                                    <>
                                        <Text style={styles.heading}>{data['label']}</Text>
                                        <View style={styles.dateInput}>
                                            <CustomPicker
                                                options={data['list']}
                                                onValueChange={value=>this.state[data['value']]=value}
                                                placeholder={data['label']}
                                                defaultValue={this.state[data['value']]}
                                            />
                                        </View>
                                    </>
                                )
                            default:
                                return (
                                    <>
                                        <Text style={styles.heading}>{data['label']}</Text>
                                        <NTextInput
                                            onChangeText={v=>this.state[data.value]=v.trim()}
                                            placeholder={data['label']}
                                            defaultValue={this.state[data['value']] && this.state[data['value']].toString()}
                                        />
                                    </>
                                )
                        }
                    })
                }
                <Button
                    onPress={()=>{
                        Alert.alert(
                            'Confirm Action',
                            'Do you want to proceed with your request',
                            [
                                {
                                    text:'Yes,Continue',
                                    onPress:this.update.bind(this)
                                },
                                {
                                    text:'No,Cancel',
                                    onPress:null
                                }
                            ]
                        )
                    }}
                    containerStyle={{
                        marginVertical:20
                    }}
                    loading={this.state.loading}
                    buttonStyle={globalStyles.button}
                    title="UPDATE"
                />
            </ScrollView>
            </>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        width: "100%",
        marginTop: 25,
        padding:15,
        marginVertical:15
    },
    dateInput: {
        borderRadius: 5,
        borderWidth: 2,
        borderColor: '#959595',
        padding: 10,
        width: '100%',
        height: 60,
        justifyContent: 'center',
        marginVertical: 5
    },
    heading:{
        marginVertical:10
    }
});