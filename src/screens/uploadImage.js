import React, { Component } from 'react'
import { View,FlatList,TouchableOpacity,StyleSheet,Image,Dimensions,Alert } from 'react-native';
import Header from '../components/header';
import globalStyles from '../constants/styles';
const { height,width } = Dimensions.get('window');
import { Button, Text } from 'react-native-elements';
import {launchImageLibrary} from 'react-native-image-picker';
import Icon from 'react-native-vector-icons/FontAwesome';
import apiService from '../services/api.service';




export default class uploadImage extends Component {

    state={
        data:{},
        images:[],
        loading:false
    }

    componentDidMount(){
        this.setState(
            { 
                data:{
                    ...this.props.route.params
                }
            }
        )
    }

    sendRequest=async()=>{
        try {
            let{images,data}=this.state;
            if(images.length<=0){
                return;
            }
            this.setState(
                {
                    loading:true
                }
            )
            const response=await apiService.addRoom(
                {
                    ...data,
                    images:images
                }
            )
            if(!response.success){
                throw Error(
                    response.message
                )
            }
            this.setState(
                {
                    loading:false
                },()=>{
                    Alert.alert(
                        'ADD PROPERTY',
                        'Property added successfully'
                    );
                    this.props.navigation.navigate(
                        'propertiesPage'
                    );
                }
            )
        } catch (err) {
            this.setState(
                {
                    loading:false
                },()=>Alert.alert(
                    'Oops',
                    err.message
                )
            )
        }
    }


    getImages=()=>launchImageLibrary(
        {
            mediaType:'photo',
            includeBase64:true
        },({
            base64,
            type
        })=>{
            let{images}=this.state;
            images.push(
                {
                    data:`data:${type};base64,${base64}`,
                    type:type.split('/')[1]
                }
            )
            this.setState(
                {
                    images:images
                }
            )
        })

    render() {
        return (
            <View>
                <Header 
                    name="Add Pictures"
                    back={this.props.navigation.goBack} 
                />
                <FlatList 
                    contentContainerStyle={styles.container}
                    data={this.state.images}
                    numColumns={2}
                    keyExtractor={el=>Math.random()}
                    ListFooterComponent={()=>(
                        <TouchableOpacity 
                            onPress={this.getImages.bind(this)}
                        style={[styles.item,{
                            borderColor:'#ddd',
                            borderWidth:1,
                            borderStyle:'dotted',
                            borderRadius:5
                        }]}>
                            <Icon 
                                name="camera"
                                size={30}
                                color="#ddd"
                            />
                        </TouchableOpacity>
                    )}
                    renderItem={({item})=>
                        <View style={styles.item}>
                            <Image
                                style={{
                                    width:'100%',
                                    height:'100%'
                                }}
                                source={{uri:item.data}} 
                            />
                        </View>
                    }
                />
                <Button
                    onPress={this.sendRequest.bind(this)}
                    loading={this.state.loading}
                    buttonStyle={globalStyles.button}
                    title="DONE"
                />
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        marginTop: 10,
    },
    horizontal: {
        flexDirection: 'row',
        alignItems: 'center',
        // marginBottom: 23,
    },
    item: {
        width:width*0.45,
        height: 150,
        borderRadius: 2,
        margin: 10,
        marginBottom: 6,
        justifyContent:'center',
        alignItems:'center'
    }
});