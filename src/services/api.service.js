import { BASEURL } from "../api/api";

const sendOtp=(data)=>fetch(
    `${BASEURL}/otp/sendotp`,
    {
        method:'POST',
        body:JSON.stringify(data),
        headers:{
            'Content-type':'Application/json'
        }
    }
).then(res=>res.json()).catch(err=>err);

const getRooms=()=>fetch(
    `${BASEURL}/rooms/get`,
    {
        method:'GET',
        headers:{
            'Content-type':'Application/json'
        }
    }
).then(res=>res.json()).catch(err=>err);


const resetpassword=(data)=>fetch(
    `${BASEURL}/tenants/resetpassword`,
    {
        method:'POST',
        headers:{
            'Content-type':'Application/json'
        },
        body:JSON.stringify(data)
    }
).then(res=>res.json()).catch(err=>err);


const getAllRooms=()=>fetch(
    `${BASEURL}/rooms/all/get`,
    {
        method:'GET',
        headers:{
            'Content-type':'Application/json'
        }
    }
).then(res=>res.json()).catch(err=>err);



const getTenantRents=(id)=>fetch(
    `${BASEURL}/rents/tenant/get/${id}`,
    {
        method:'GET',
        headers:{
            'Content-type':'Application/json'
        }
    }
).then(res=>res.json()).catch(err=>err);


const updateRent=(data)=>fetch(
    `${BASEURL}/rents/update`,
    {
        method:'PUT',
        headers:{
            'Content-type':'Application/json'
        },
        body:JSON.stringify(data)
    }
).then(res=>res.json()).catch(err=>err);


const getInspections=(id)=>fetch(
    `${BASEURL}/inspection/get`,
    {
        method:'GET',
        headers:{
            'Content-type':'Application/json'
        }
    }
).then(res=>res.json()).catch(err=>err);

const getUtils=()=>fetch(
    `${BASEURL}/utilities/get`,
    {
        method:'GET',
        headers:{
            'Content-type':'Application/json'
        }
    }
).then(res=>res.json()).catch(err=>err);


const getAllRents=(id)=>fetch(
    `${BASEURL}/rents/get`,
    {
        method:'GET',
        headers:{
            'Content-type':'Application/json'
        }
    }
).then(res=>res.json()).catch(err=>err);

const rentLastPaid=(id)=>fetch(
    `${BASEURL}/payments/rent/lastpaid/${id}`,
    {
        method:'GET',
        headers:{
            'Content-type':'Application/json'
        }
    }
).then(res=>res.json()).catch(err=>err);

const updatePayment=(data)=>fetch(
    `${BASEURL}/payments/update`,
    {
        method:'PUT',
        headers:{
            'Content-type':'Application/json'
        },
        body:JSON.stringify(data)
    }
).then(res=>res.json()).catch(err=>err);


const getTenantPayments=(id)=>fetch(
    `${BASEURL}/payments/tenant/get/${id}`,
    {
        method:'GET',
        headers:{
            'Content-type':'Application/json'
        }
    }
).then(res=>res.json()).catch(err=>err);


const getAllPayments=(id)=>fetch(
    `${BASEURL}/payments/get`,
    {
        method:'GET',
        headers:{
            'Content-type':'Application/json'
        }
    }
).then(res=>res.json()).catch(err=>err);


const getTenantReports=(id)=>fetch(
    `${BASEURL}/reports/get/${id}`,
    {
        method:'GET',
        headers:{
            'Content-type':'Application/json'
        }
    }
).then(res=>res.json()).catch(err=>err);


const getReports=(id)=>fetch(
    `${BASEURL}/reports/get`,
    {
        method:'GET',
        headers:{
            'Content-type':'Application/json'
        }
    }
).then(res=>res.json()).catch(err=>err);


const getPendingRents=()=>fetch(
    `${BASEURL}/rents/pending/get`,
    {
        method:'GET',
        headers:{
            'Content-type':'Application/json'
        }
    }
).then(res=>res.json()).catch(err=>err);


const loadSettings=()=>fetch(
    `${BASEURL}/settings/get`,
    {
        method:'GET',
        headers:{
            'Content-type':'Application/json'
        }
    }
).then(res=>res.json()).catch(err=>err);


const getTenants=()=>fetch(
    `${BASEURL}/tenants/get`,
    {
        method:'GET',
        headers:{
            'Content-type':'Application/json'
        }
    }
).then(res=>res.json()).catch(err=>err);


const newInspection=(data)=>fetch(
    `${BASEURL}/inspection/new`,
    {
        method:'POST',
        headers:{
            'Content-type':'Application/json'
        },
        body:JSON.stringify(data)
    }
).then(res=>res.json()).catch(err=>err);


const approveRent=(data)=>fetch(
    `${BASEURL}/rents/approve`,
    {
        method:'POST',
        headers:{
            'Content-type':'Application/json'
        },
        body:JSON.stringify(data)
    }
).then(res=>res.json()).catch(err=>err);


const newReport=(data)=>fetch(
    `${BASEURL}/reports/new`,
    {
        method:'POST',
        headers:{
            'Content-type':'Application/json'
        },
        body:JSON.stringify(data)
    }
).then(res=>res.json()).catch(err=>err);


const newRent=(data)=>fetch(
    `${BASEURL}/rents/new`,
    {
        method:'POST',
        headers:{
            'Content-type':'Application/json'
        },
        body:JSON.stringify(data)
    }
).then(res=>res.json()).catch(err=>err);


const newAdminRent=(data)=>fetch(
    `${BASEURL}/rents/admin/new`,
    {
        method:'POST',
        headers:{
            'Content-type':'Application/json'
        },
        body:JSON.stringify(data)
    }
).then(res=>res.json()).catch(err=>err);


const newTenant=(data)=>fetch(
    `${BASEURL}/tenants/new`,
    {
        method:'POST',
        headers:{
            'Content-type':'Application/json'
        },
        body:JSON.stringify(data)
    }
).then(res=>res.json()).catch(err=>err);


const payRent=(data)=>fetch(
    `${BASEURL}/rents/pay`,
    {
        method:'POST',
        headers:{
            'Content-type':'Application/json'
        },
        body:JSON.stringify(data)
    }
).then(res=>res.json()).catch(err=>err);



const renewRent=(data)=>fetch(
    `${BASEURL}/rents/renew`,
    {
        method:'POST',
        headers:{
            'Content-type':'Application/json'
        },
        body:JSON.stringify(data)
    }
).then(res=>res.json()).catch(err=>err);


const paySecurityDeposit=(data)=>fetch(
    `${BASEURL}/rents/security/deposit`,
    {
        method:'POST',
        headers:{
            'Content-type':'Application/json'
        },
        body:JSON.stringify(data)
    }
).then(res=>res.json()).catch(err=>err);

const payUtility=(data)=>fetch(
    `${BASEURL}/utilities/pay`,
    {
        method:'POST',
        headers:{
            'Content-type':'Application/json'
        },
        body:JSON.stringify(data)
    }
).then(res=>res.json()).catch(err=>err);


const addRoom=(data)=>fetch(
    `${BASEURL}/rooms/new`,
    {
        method:'POST',
        headers:{
            'Content-type':'Application/json'
        },
        body:JSON.stringify(data)
    }
).then(res=>res.json()).catch(err=>err);



const updateRoom=(data)=>fetch(
    `${BASEURL}/rooms/update`,
    {
        method:'PUT',
        headers:{
            'Content-type':'Application/json'
        },
        body:JSON.stringify(data)
    }
).then(res=>res.json()).catch(err=>err);


const updateTenant=(data)=>fetch(
    `${BASEURL}/tenants/update`,
    {
        method:'PUT',
        headers:{
            'Content-type':'Application/json'
        },
        body:JSON.stringify(data)
    }
).then(res=>res.json()).catch(err=>err);


const deleteRoom=(id)=>fetch(
    `${BASEURL}/rooms/delete/${id}`,
    {
        method:'DELETE',
        headers:{
            'Content-type':'Application/json'
        }
    }
).then(res=>res.json()).catch(err=>null);


const revokeTenant=(data)=>fetch(
    `${BASEURL}/rents/revoke`,
    {
        method:'PUT',
        headers:{
            'Content-type':'Application/json'
        },
        body:JSON.stringify(data)
    }
).then(res=>res.json()).catch(err=>err);



export default {
    sendOtp,
    getRooms,
    newInspection,
    newRent,
    newTenant,
    getTenantRents,
    payRent,
    getTenantPayments,
    newReport,
    getTenantReports,
    getReports,
    addRoom,
    getPendingRents,
    approveRent,
    getTenants,
    getAllRents,
    getAllPayments,
    updateTenant,
    revokeTenant,
    getInspections,
    getUtils,
    payUtility,
    deleteRoom,
    updateRent,
    rentLastPaid,
    newAdminRent,
    updateRoom,
    paySecurityDeposit,
    updatePayment,
    getAllRooms,
    renewRent,
    loadSettings,
    resetpassword
}