import { BASEURL } from "../api/api";
import AsyncStorage from '@react-native-async-storage/async-storage';

const login = (data) => fetch(
    `${BASEURL}/tenants/login`,
    {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-type': 'Application/json'
        }
    }
).then(res=>res.json()).catch(err=>err);


const landlordLogin=(data)=>fetch(
    `${BASEURL}/landlords/login`,
    {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-type': 'Application/json'
        }
    }
).then(res=>res.json()).catch(err=>null);


const logout=(callback)=>{
    AsyncStorage.removeItem('CRT-user').then(res=>{
        callback();
    }).catch(err=>console.log(err));
}


const signup = (data) => fetch(
    `${BASEURL}/tenants/new`,
    {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-type': 'Application/json'
        }
    }
).then(res=>res.json()).catch(err=>err);


export default { 
    login,
    signup,
    logout,
    landlordLogin 
}