import React, { Component, createContext } from 'react';
export const AuthProvider = createContext();
export const AuthConsumer = AuthProvider.Consumer;
import fetchIntercept from "fetch-intercept";



export default class AuthState extends Component {

    state = {
        user:null,
        token:null,
        showguide:null,
        role:''
    }


    componentDidMount() {};



    dispath(data, callback=null) {
        Object.keys(data).forEach(key => {
            this.setState(
                { [key]: data[key] }
            )
        });
        if (callback) callback();
    }

    setHeaders=(token)=>{
        this.unregister=fetchIntercept.register({
            request: (url,config)=>{
                config.headers = {
                    'Content-type':'Application/json',
                    'Authorization':'Bearer ' + token
                }
                return [url,config];
            },
            requestError:(err)=>{
                return Promise.reject(err);
            },
            response:(response)=>{
                switch (response.status) {
                    case 401:
                        throw Error(
                            'Session expired kindly login again to continue'
                        )
                    case 400:
                        throw Error(
                            'Dear customer please check and fill in all the required details for the input fields. We cannot proceed with your request.'
                        )
                    default:
                        if(response.status!==200){
                            throw Error(
                                'Your internet connection is weak,we are not able to process your request'
                            )
                        }
                        break;
                }
                return response;
            },
            responseError:(err)=>{
                return Promise.reject(
                    err
                );
            }
        })

    }

    render() {
        return (
            <AuthProvider.Provider
                value={{
                    state: this.state,
                    dispath: this.dispath.bind(this),
                    setHeaders: this.setHeaders.bind(this)
                }}
            >
                {this.props.children}
            </AuthProvider.Provider>
        )
    }
}
