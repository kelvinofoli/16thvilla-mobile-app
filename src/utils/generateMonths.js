
generateMonths=(start,end)=>{
    let i=start;
    let durations=[];
    while(i<=end){
        durations.push(
            { 
                label:`${i} months`, 
                value:i 
            }
        )
        i++;
    }
    return durations;
}

export default generateMonths;